//
// Created by penci on 10/06/2020.
//

#ifndef GPC_TEMP_TYPENAME_HPP
#define GPC_TEMP_TYPENAME_HPP

#include "Core/type.hpp"
#ifdef __GNUG__
#include <cstdlib>
#include <memory>
#include <cxxabi.h>

namespace gpc {
	namespace type_name {
		std::string demangle(const char* name) {

			int status = -4; // some arbitrary value to eliminate the compiler warning

			// enable c++11 by passing the flag -std=c++11 to g++
			std::unique_ptr<char, void(*)(void*)> res {
					abi::__cxa_demangle(name, NULL, NULL, &status),
					std::free
			};

			return (status==0) ? res.get() : name ;
		}

#else

// does nothing if not g++
std::string demangle(const char* name) {
    return name;
}

#endif
	}
}

#endif //GPC_TEMP_TYPENAME_HPP

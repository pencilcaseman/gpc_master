//
// Created by penci on 22/03/2020.
//

#ifndef GPC_TEMP_TYPES_HPP
#define GPC_TEMP_TYPES_HPP

#ifdef __GXX_EXPERIMENTAL_CXX0X__
#include <experimental/type_traits>
#else
#include <type_traits>
#endif
#include <sstream>

namespace gpc {
    namespace conversion {
        namespace type {
            /**
             * Convert a character to an integer value
             *
             * @param val Character to convert
             * @return Integer representation of character
             */
            short charToInt(char val) {
                switch (val) {
                    case '0':
                        return 0;
                    case '1':
                        return 1;
                    case '2':
                        return 2;
                    case '3':
                        return 3;
                    case '4':
                        return 4;
                    case '5':
                        return 5;
                    case '6':
                        return 6;
                    case '7':
                        return 7;
                    case '8':
                        return 8;
                    case '9':
                        return 9;
                    default:
                        return -1;
                }
            }

            /**
             * Convert an integer to a character
             *
             * CAUTION: Only works for single digits
             *
             * @param val Digit value to convert
             * @return Character representation of digit
             */
            char intToChar(int val) {
                switch (val) {
                    case 0:
                        return '0';
                    case 1:
                        return '1';
                    case 2:
                        return '2';
                    case 3:
                        return '3';
                    case 4:
                        return '4';
                    case 5:
                        return '5';
                    case 6:
                        return '6';
                    case 7:
                        return '7';
                    case 8:
                        return '8';
                    case 9:
                        return '9';
                    default:
                        return '0';
                }
            }

            // To String conversions
            // 1- detecting if std::to_string is valid on T
            template<typename T>
            using std_to_string_expression = decltype(std::to_string(std::declval<T>()));
            template<typename T>
            constexpr bool has_std_to_string = std::experimental::is_detected<std_to_string_expression, T>();

            // 2- detecting if to_string is valid on T
            template<typename T>
            using to_string_expression = decltype(to_string(std::declval<T>()));
            template<typename T>
            constexpr bool has_to_string = std::experimental::is_detected<to_string_expression, T>();

            // 3- detecting if T can be sent to an ostringstream
            template<typename T>
            using ostringstream_expression = decltype(std::declval<std::ostringstream &>() << std::declval<T>());
            template<typename T>
            constexpr bool has_ostringstream = std::experimental::is_detected<ostringstream_expression, T>();

            /**
             * Convert a datatype with an std::to_string method to a string
             *
             * @tparam T Value to convert to a string
             * @param t Typename for the value
             * @return String representation for the value
             */
            // 1-  std::to_string is valid on T
            template<typename T, typename std::enable_if<has_std_to_string<T>, int>::type = 0>
            std::string toString(T const &t) {
                std::ostringstream res;
                res << std::fixed << t;
                return res.str();
            }

            /**
             * Convert a datatype with a to_string method to a string
             *
             * @tparam T Value to convert to a string
             * @param t Typename for the value
             * @return String representation for the value
             */
            // 2-  std::to_string is not valid on T, but to_string is
            template<typename T, typename std::enable_if<!has_std_to_string<T> && has_to_string<T>, int>::type = 0>
            std::string toString(T const &t) {
                return to_string(t);
            }

            /**
             * Convert a datatype with a string stream method to a string
             *
             * @tparam T Value to convert to a string
             * @param t Typename for the value
             * @return String representation for the value
             */
            // 3-  neither std::string nor to_string work on T, let's stream it then
            template<typename T, typename std::enable_if<
                    !has_std_to_string<T> && !has_to_string<T> && has_ostringstream<T>, int>::type = 0>
            std::string toString(T const &t) {
                std::ostringstream oss;
                oss << t;
                return oss.str();
            }
        }
    }
}

#endif //GPC_TEMP_TYPES_HPP

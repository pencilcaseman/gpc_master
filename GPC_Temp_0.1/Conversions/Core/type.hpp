//
// Created by penci on 10/06/2020.
//

#ifndef GPC_TEMP_TYPE_HPP
#define GPC_TEMP_TYPE_HPP

#include <string>
#include <typeinfo>

namespace gpc {
	namespace type_name {
		std::string demangle(const char *name);

		template<class T>
		std::string typeDemangle(const T &t) {
			return demangle(typeid(t).name());
		}
	}
}

#endif //GPC_TEMP_TYPE_HPP

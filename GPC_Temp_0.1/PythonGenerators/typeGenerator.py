def getTypes(typeNames):
    for dataType in typeNames:
        print('std::string TypeName({}) {} return "{}"; {};'.format(dataType, "{", dataType, "}"))

typeNames1 = [
    "short",
    "unsigned short",
    "int",
    "unsigned int",
    "long",
    "unsigned long",
    "long long",
    "unsigned long long",
    "double",
    "long double",
    "float",
    "bool"
    ]

typeNames2 = []

for val in typeNames1:
    typeNames2.append(val)
    for i in range(1, 32):
        typeNames2.append(val + " " + ("*" * i))


def getArrayMacros(n):
    for i in range(n):
        string = "#define Array{}D(type) ".format(i + 1)
        for j in range(i):
            string += "gpc::array::Array<"
        string += "type"
        for j in range(i):
            string += ">"
        print(string)
               

# getTypes(typeNames2)
getArrayMacros(32)

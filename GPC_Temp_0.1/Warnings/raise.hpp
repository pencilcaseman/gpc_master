//
// Created by penci on 22/03/2020.
//

#ifndef GPC_RAISE_HPP
#define GPC_RAISE_HPP

#include "../IO/colors.hpp"
#include <exception>

namespace gpc {
    namespace warning {
        /**
         * Raise an error given a type and a message
         * The line number, function and file are passed too
         */

#ifdef __LINE__
#   ifdef __PRETTY_FUNCTION__
#       ifdef __FILE__
#           define raiseError(type, message) _raiseError(type, message, __LINE__, __PRETTY_FUNCTION__, __FILE__)
#       else
            #define raiseError(type, message) _raiseError(type, message, __LINE__, __PRETTY_FUNCTION__)
#       endif
#   else
#       define raiseError(type, message) _raiseError(type, message, __LINE__)
#   endif
#else
#   define raiseError(type, message) _raiseError(type, message)
#endif

        /**
         * Raise an error and exit the program
         *
         * @param type The type of the error
         * @param message The error message
         * @param line The line number for the error
         * @param function The function for the error
         * @param file The file for the error
         */
        void _raiseError(const std::string &type = "Error", const std::string &message = "Message",
                         unsigned long long line = 0,
                         const std::string &function = "Function", const std::string &file = "File") {
            std::cout << "\n\n";
            std::cout << gpc::color::Color().RED << type << " in line " << line << std::endl;
            std::cout << gpc::color::Color().PURPLE << "Error received in function \"" << function << "\" in file \""
                      << file << "\""
                      << std::endl;
            std::cout << gpc::color::Color().GREEN << message << gpc::color::Color().RESET;

            exit(1);
        }
    }
}

#endif //GPC_BUILTIN_HPP

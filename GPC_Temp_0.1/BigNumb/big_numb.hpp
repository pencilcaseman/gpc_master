//
// Created by penci on 02/01/2020.
//

#ifndef CLIB_BIGNUMB_H
#define CLIB_BIGNUMB_H

#include <cstring>
#include <cstdbool>
#include <cmath>
#include <iostream>
#include <string>
#include <algorithm>
#include "../Conversions/types.hpp"
#include "../Warnings/raise.hpp"
#include "../Math/math.hpp"
#include "../Basic/basic.hpp"

namespace gpc { namespace big_numb {
    /**
     * A class to define some variables for the BigNumb type
     */
    class Context {
    public:
        /**
         * A set of rounding modes for the BigNumb class
         */
        enum RoundingTypes {
            // Round values towards -infinity
            ROUND_DOWN = 0,
            // Round values towards +infinity
            ROUND_UP = 1,
            // Round values to 0
            ROUND_TO_ZERO = 2,
            // Round values away from zero
            ROUND_FROM_ZERO = 3,
            // .5 always rounds up
            ROUND_HALF_UP = 4,
            // .5 always rounds down
            ROUND_HALF_DOWN = 5,
            // .5 rounds to 0
            ROUND_HALF_TO_ZERO = 6,
            // .5 rounds from 0
            ROUND_HALF_FROM_ZERO = 7,
            // Round to nearest even number
            ROUND_HALF_EVEN = 8,
            // Round to nearest odd number
            ROUND_HALF_ODD = 9,
            // Stochastic rounding (1.6 -> 1 [0.4 (2 - 1.6)] -> 2 [0.6 (1.6 - 1)])
            STOCHASTIC = 10
        };

        // Precision for arithmetic (currently does nothing)
        unsigned long long prec;
        enum RoundingTypes rounding;

        // Default context
        Context() {
            prec = 10;
            rounding = RoundingTypes::ROUND_TO_ZERO;
        }

        // Create a custom context
        Context(unsigned long long precision, enum RoundingTypes roundingMode) {
            prec = gpc::math::max(precision, 1llu);
            rounding = roundingMode;
        }
    };

    // Context object
    Context context = Context();

    /**
     * BigNumb datatype to store indefinitely large numbers
     *
     * Currently is quite slow and does not support division
     */
    class BigNumb {
#   define INT int16_t
    private:
        /**
         * Initialize a BigNumb object from given input string
         *
         * @param string The string input to create the BigNumb object from
         * @param strip Remove leading and trailing zeros
         * @return Initialized BigNumb object
         */
        static BigNumb _initialize_(const std::string& string, bool strip = true) {
            // Strip leading and trailing zeros
            std::string val = strip ? gpc::basic::ltrim(string, "0") : string;
            BigNumb res;


            unsigned long long len = val.length();
            unsigned long long digitsBefore, digitsAfter;
            digitsBefore = digitsAfter = 0;
            INT *beforeDecimal;
            INT *afterDecimal;

            for (unsigned long long i = 0; i < len; i++) {
                if (val[i] == '-') {
                    if (i == 0) {
                        res.isNegative = true;
                    } else {
                        gpc::warning::raiseError("Invalid Character Error", "Invalid character '-'");
                    }
                } else if (val[i] == '.') {
                    if (digitsBefore == 0 && digitsAfter == 0) {
                        digitsBefore = i - (res.isNegative ? 1 : 0);
                        digitsAfter = len - digitsBefore - 1 - (res.isNegative ? 1 : 0);
                    } else {
                        gpc::warning::raiseError("Invalid Character Error", "Two '.'s were found in number string");
                    }
                }
            }

            if (digitsBefore == 0 && digitsAfter == 0) {
                digitsBefore = len - (res.isNegative ? 1 : 0);
            }

            // No decimal point was found
            res.integerLength = digitsBefore; // max(digitsBefore, 1u);
            res.decimalLength = digitsAfter; // max(digitsAfter, 1u);

            if (digitsBefore > 0) {
                beforeDecimal = (INT *) malloc(sizeof(INT) * digitsBefore);
                unsigned long long index = digitsBefore - 1;
                for (unsigned long long i = res.isNegative ? 1 : 0; i < digitsBefore + (res.isNegative ? 1 : 0); i++) {
                    beforeDecimal[index--] = (INT) gpc::conversion::type::charToInt(val[i]);
                }
            } else {
                beforeDecimal = (INT *) malloc(sizeof(INT));
                beforeDecimal[0] = 0;
            }

            if (digitsAfter > 0) {
                afterDecimal = (INT *) malloc(sizeof(INT) * digitsAfter);
                unsigned long long index = 0;
                for (unsigned long long i = digitsBefore + (res.isNegative ? 1 : 0) + 1; i < len; i++) {
                    afterDecimal[index++] = (INT) gpc::conversion::type::charToInt(val[i]);
                }
            } else {
                afterDecimal = (INT *) malloc(sizeof(INT));
                afterDecimal[0] = 0;
            }

            res.integerSection = beforeDecimal;
            res.decimalSection = afterDecimal;

            return res;
        }
    public:
        // Lengths of the number
        unsigned long long integerLength;
        unsigned long long decimalLength;
        INT *integerSection;
        INT *decimalSection;
        bool isNegative;

        // #################################################################################################################
        // #################################################################################################################
        // #################################################################################################################
        //      ░█████╗░░█████╗░███╗░░██╗░██████╗████████╗██████╗░██╗░░░██╗░█████╗░████████╗░█████╗░██████╗░░██████╗
        //      ██╔══██╗██╔══██╗████╗░██║██╔════╝╚══██╔══╝██╔══██╗██║░░░██║██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██╔════╝
        //      ██║░░╚═╝██║░░██║██╔██╗██║╚█████╗░░░░██║░░░██████╔╝██║░░░██║██║░░╚═╝░░░██║░░░██║░░██║██████╔╝╚█████╗░
        //      ██║░░██╗██║░░██║██║╚████║░╚═══██╗░░░██║░░░██╔══██╗██║░░░██║██║░░██╗░░░██║░░░██║░░██║██╔══██╗░╚═══██╗
        //      ╚█████╔╝╚█████╔╝██║░╚███║██████╔╝░░░██║░░░██║░░██║╚██████╔╝╚█████╔╝░░░██║░░░╚█████╔╝██║░░██║██████╔╝
        //      ░╚════╝░░╚════╝░╚═╝░░╚══╝╚═════╝░░░░╚═╝░░░╚═╝░░╚═╝░╚═════╝░░╚════╝░░░░╚═╝░░░░╚════╝░╚═╝░░╚═╝╚═════╝░
        // #################################################################################################################
        // #################################################################################################################
        // #################################################################################################################

        /**
         * Default value for a BigNumb object
         */
        BigNumb() {
            integerLength = 1;
            decimalLength = 1;
            integerSection = (INT *) calloc(sizeof(INT), 1);
            decimalSection = (INT *) calloc(sizeof(INT), 1);
            integerSection[0] = 0;
            decimalSection[0] = 0;
            isNegative = false;
        }

        /*
        ~BigNumb() {
        	free(integerSection);
        	free(decimalSection);
        }
         */

        BigNumb(const std::string& val) {
            *this = _initialize_(val);
        }

        BigNumb(long long val) {
            *this = _initialize_(std::to_string(val));
        }

        BigNumb(unsigned long long val) {
            *this = _initialize_(std::to_string(val));
        }

        BigNumb(long val) {
            *this = _initialize_(std::to_string(val));
        }

        BigNumb(unsigned long val) {
            *this = _initialize_(std::to_string(val));
        }

        BigNumb(int val) {
            *this = _initialize_(std::to_string(val));
        }

        BigNumb(unsigned int val) {
            *this = _initialize_(std::to_string(val));
        }

        BigNumb(short val) {
            *this = _initialize_(std::to_string(val));
        }

        BigNumb(unsigned short val) {
            *this = _initialize_(std::to_string(val));
        }

        BigNumb(long double val) {
            *this = _initialize_(std::to_string(val));
        }

        BigNumb(double val) {
            *this = _initialize_(std::to_string(val));
        }

        BigNumb(float val) {
            *this = _initialize_(std::to_string(val));
        }

        BigNumb(unsigned long long digitsBeforeDecimal, unsigned long long digitsAfterDecimal) {
            char *str = (char *) malloc(sizeof(char) * (digitsBeforeDecimal + digitsAfterDecimal + 2));

            for (unsigned long long i = 0; i < digitsBeforeDecimal; i++) {
                str[i] = '0';
            }
            str[digitsBeforeDecimal] = '.';
            for (unsigned long long i = digitsBeforeDecimal + 1;
                 i < digitsBeforeDecimal + digitsAfterDecimal + 1; i++) {
                str[i] = '0';
            }
            str[digitsBeforeDecimal + digitsAfterDecimal + 2] = '\0';

            *this = _initialize_(str, false);
        }

        // #################################################################################################################
        // #################################################################################################################
        // #################################################################################################################
        //  ██████╗░░█████╗░░██████╗██╗░█████╗░  ░█████╗░██████╗░███████╗██████╗░░█████╗░████████╗░█████╗░██████╗░░██████╗
        //  ██╔══██╗██╔══██╗██╔════╝██║██╔══██╗  ██╔══██╗██╔══██╗██╔════╝██╔══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██╔════╝
        //  ██████╦╝███████║╚█████╗░██║██║░░╚═╝  ██║░░██║██████╔╝█████╗░░██████╔╝███████║░░░██║░░░██║░░██║██████╔╝╚█████╗░
        //  ██╔══██╗██╔══██║░╚═══██╗██║██║░░██╗  ██║░░██║██╔═══╝░██╔══╝░░██╔══██╗██╔══██║░░░██║░░░██║░░██║██╔══██╗░╚═══██╗
        //  ██████╦╝██║░░██║██████╔╝██║╚█████╔╝  ╚█████╔╝██║░░░░░███████╗██║░░██║██║░░██║░░░██║░░░╚█████╔╝██║░░██║██████╔╝
        //  ╚═════╝░╚═╝░░╚═╝╚═════╝░╚═╝░╚════╝░  ░╚════╝░╚═╝░░░░░╚══════╝╚═╝░░╚═╝╚═╝░░╚═╝░░░╚═╝░░░░╚════╝░╚═╝░░╚═╝╚═════╝░
        // #################################################################################################################
        // #################################################################################################################
        // #################################################################################################################

        [[nodiscard]] BigNumb _add(BigNumb other) const {
            auto res = BigNumb(gpc::math::max(integerLength, other.integerLength) + 1,
                               gpc::math::max(decimalLength, other.decimalLength));

            INT carry = 0;
            for (unsigned long long i = res.decimalLength; i > 0; i--) {
                // Use i - 1
                INT aVal, bVal;
                aVal = bVal = 0;

                if (i - 1 < decimalLength) aVal = decimalSection[i - 1];
                if (i - 1 < other.decimalLength) bVal = other.decimalSection[i - 1];

                uint16_t sum = aVal + bVal + carry;
                res.decimalSection[i - 1] = sum % 10;
                carry = sum / 10;
            }

            for (unsigned long long i = 0; i < res.integerLength; i++) {
                INT aVal, bVal;
                aVal = bVal = 0;

                if (i < integerLength) aVal = integerSection[i];
                if (i < other.integerLength) bVal = other.integerSection[i];

                uint16_t sum = aVal + bVal + carry;
                res.integerSection[i] = sum % 10;
                carry = sum / 10;
            }

            return res;
        }

        [[nodiscard]] BigNumb _sub(BigNumb other) {
            auto res = copy(gpc::math::max(integerLength, other.integerLength), gpc::math::max(decimalLength,
                                                                                               other.decimalLength)); // BigNumb(max(integerLength, other.integerLength) + 1, max(decimalLength, other.decimalLength));

            for (unsigned long long i = res.decimalLength; i > 0; i--) {
                // Use i - 1
                INT val = 0;
                if (i - 1 < other.decimalLength) {
                    val = other.decimalSection[i - 1];
                }

                res.decimalSection[i - 1] -= val;

                bool cont = true;
                unsigned long long index = i - 1;
                while (cont) {
                    if (res.decimalSection[index] < 0) {
                        res.decimalSection[index] += 10;

                        if (index > 0) {
                            res.decimalSection[index - 1]--;
                        } else {
                            res.integerSection[0] -= 1;
                        }

                        index--;
                    } else {
                        cont = false;
                    }
                }
            }

            for (unsigned long long i = integerLength; i > 0; i--) {
                // Use i - 1
                INT val = 0;
                if (i - 1 < other.integerLength) {
                    val = other.integerSection[i - 1];
                }

                res.integerSection[i - 1] -= val;

                bool cont = true;
                unsigned long long index = i - 1;
                while (cont) {
                    if (res.integerSection[index] < 0) {
                        res.integerSection[index] += 10;
                        res.integerSection[index + 1]--;
                        index++;
                    } else {
                        cont = false;
                    }
                }
            }

            return res;
        }

        [[nodiscard]] BigNumb _mul(BigNumb other) const {
            unsigned long long aLen = integerLength + decimalLength;
            unsigned long long bLen = other.integerLength + other.decimalLength;
            unsigned long long resLen = gpc::math::max(aLen, bLen) * 2;

            auto *a = (int16_t *) malloc(sizeof(int16_t) * aLen);
            auto *b = (int16_t *) malloc(sizeof(int16_t) * bLen);
            auto *res = (int16_t *) calloc(sizeof(int16_t), resLen);

            for (int i = 0; i < aLen; i++) {
                if (i < integerLength) {
                    a[i] = integerSection[i];
                } else {
                    a[i] = decimalSection[i - integerLength];
                }
            }

            for (int i = 0; i < bLen; i++) {
                if (i < other.integerLength) {
                    b[i] = other.integerSection[i];
                } else {
                    b[i] = other.decimalSection[i - other.integerLength];
                }
            }

            for (unsigned long long i = aLen; i > 0; i--) {
                for (unsigned long long j = bLen; j > 0; j--) {
                    int16_t tmp = a[i - 1] * b[j - 1];

                    res[(resLen - 1) - (aLen - i - 1) - (bLen - j - 1) - 2] += tmp % 10;
                    res[(resLen - 1) - (aLen - i - 1) - (bLen - j - 1) - 3] += floor((double) tmp / 10);

                    for (unsigned long long k = resLen; k > 0; k--) {
                        tmp = res[k - 1];

                        if (tmp > 9) {
                            res[k - 1] = tmp % 10;
                            res[k - 2] += floor((double) tmp / 10);
                        }
                    }
                }
            }

            unsigned long long decLen = (decimalLength + other.decimalLength);
            unsigned long long intLen = resLen - decLen;

            BigNumb resNumb = BigNumb(intLen, decLen);

            unsigned long long j = 0;
            for (unsigned long long i = resNumb.integerLength; i > 0; i--) {
                // Use i - 1
                resNumb.integerSection[i - 1] = res[j];
                j++;
            }

            for (unsigned long long i = 0; i < resNumb.decimalLength; i++) {
                resNumb.decimalSection[i] = res[j];
                j++;
            }

            return +resNumb;
        }

        [[nodiscard]] BigNumb _div(BigNumb other) {
#       pragma "Not Implemented"
        }

        [[nodiscard]] BigNumb operator+(BigNumb other) const {
            return _add(other);
        }

        [[nodiscard]] BigNumb operator-(BigNumb other) {
            return _sub(other);
        }

        [[nodiscard]] BigNumb operator*(BigNumb other) const {
            return _mul(other);
        }

        [[nodiscard]] BigNumb operator/(BigNumb other) {
            return _div(other);
        }

        // #################################################################################################################
        // #################################################################################################################
        // #################################################################################################################
        //            ░█████╗░░█████╗░███╗░░░███╗██████╗░░█████╗░██████╗░██╗░██████╗░█████╗░███╗░░██╗░██████╗
        //            ██╔══██╗██╔══██╗████╗░████║██╔══██╗██╔══██╗██╔══██╗██║██╔════╝██╔══██╗████╗░██║██╔════╝
        //            ██║░░╚═╝██║░░██║██╔████╔██║██████╔╝███████║██████╔╝██║╚█████╗░██║░░██║██╔██╗██║╚█████╗░
        //            ██║░░██╗██║░░██║██║╚██╔╝██║██╔═══╝░██╔══██║██╔══██╗██║░╚═══██╗██║░░██║██║╚████║░╚═══██╗
        //            ╚█████╔╝╚█████╔╝██║░╚═╝░██║██║░░░░░██║░░██║██║░░██║██║██████╔╝╚█████╔╝██║░╚███║██████╔╝
        //            ░╚════╝░░╚════╝░╚═╝░░░░░╚═╝╚═╝░░░░░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝╚═════╝░░╚════╝░╚═╝░░╚══╝╚═════╝░
        // #################################################################################################################
        // #################################################################################################################
        // #################################################################################################################

        bool lt__(BigNumb other, bool lte = false) {
            auto a = operator+();
            auto b = other.operator+();

            if (a.integerLength < b.integerLength) {
                return true;
            } else if (a.integerLength > b.integerLength) {
                return false;
            } else {
                bool cont = true;
                unsigned long long index = a.integerLength;
                while (index > 0) {
                    if (a.integerSection[index - 1] < b.integerSection[index - 1]) {
                        return true;
                    } else if (a.integerSection[index - 1] > b.integerSection[index - 1]) {
                        return false;
                    } else {
                        index--;
                    }
                }
            }

            bool cont = true;
            unsigned long long index = gpc::math::max(a.decimalLength, b.decimalLength);
            while (index > 0) {
                unsigned long long aVal = 0, bVal = 0;

                if (index - 1 < a.decimalLength) aVal = a.decimalSection[index - 1];
                if (index - 1 < b.decimalLength) bVal = b.decimalSection[index - 1];

                if (aVal < bVal) {
                    return true;
                } else if (a.integerSection[index - 1] > b.integerSection[index - 1]) {
                    return false;
                } else {
                    index--;
                }
            }

            return lte;
        }

        bool gt__(BigNumb other, bool gte = false) {
            auto a = operator+();
            auto b = other.operator+();

            if (a.integerLength > b.integerLength) {
                return true;
            } else if (a.integerLength < b.integerLength) {
                return false;
            } else {
                bool cont = true;
                unsigned long long index = a.integerLength;
                while (index > 0) {
                    if (a.integerSection[index - 1] > b.integerSection[index - 1]) {
                        return true;
                    } else if (a.integerSection[index - 1] < b.integerSection[index - 1]) {
                        return false;
                    } else {
                        index--;
                    }
                }
            }

            bool cont = true;
            unsigned long long index = gpc::math::max(a.decimalLength, b.decimalLength);
            while (index > 0) {
                unsigned long long aVal = 0, bVal = 0;

                if (index - 1 < a.decimalLength) aVal = a.decimalSection[index - 1];
                if (index - 1 < b.decimalLength) bVal = b.decimalSection[index - 1];

                if (aVal > bVal) {
                    return true;
                } else if (a.integerSection[index - 1] < b.integerSection[index - 1]) {
                    return false;
                } else {
                    index--;
                }
            }

            return gte;
        }

        bool eq__(BigNumb other) {
            auto a = operator+();
            auto b = operator+();

            if (a.integerLength != b.integerLength) {
                return false;
            }

            if (a.decimalLength != b.decimalLength) {
                return false;
            }

            for (unsigned long long i = 0; i < integerLength; i++) {
                if (a.integerSection[i] != b.integerSection[i]) {
                    return false;
                }
            }

            for (unsigned long long i = 0; i < decimalLength; i++) {
                if (a.decimalSection[i] != b.decimalSection[i]) {
                    return false;
                }
            }

            return true;
        }

        bool operator<(BigNumb other) {
            return lt__(other);
        }

        bool operator>(BigNumb other) {
            return gt__(other);
        }

        bool operator<=(BigNumb other) {
            return lt__(other, true);
        }

        bool operator>=(BigNumb other) {
            return lt__(other, true);
        }

        bool operator==(BigNumb other) {
            return eq__(other);
        }

        // #################################################################################################################
        // #################################################################################################################
        // #################################################################################################################
        //                      ███████╗██╗░░░██╗███╗░░██╗░█████╗░████████╗██╗░█████╗░███╗░░██╗░██████╗
        //                      ██╔════╝██║░░░██║████╗░██║██╔══██╗╚══██╔══╝██║██╔══██╗████╗░██║██╔════╝
        //                      █████╗░░██║░░░██║██╔██╗██║██║░░╚═╝░░░██║░░░██║██║░░██║██╔██╗██║╚█████╗░
        //                      ██╔══╝░░██║░░░██║██║╚████║██║░░██╗░░░██║░░░██║██║░░██║██║╚████║░╚═══██╗
        //                      ██║░░░░░╚██████╔╝██║░╚███║╚█████╔╝░░░██║░░░██║╚█████╔╝██║░╚███║██████╔╝
        //                      ╚═╝░░░░░░╚═════╝░╚═╝░░╚══╝░╚════╝░░░░╚═╝░░░╚═╝░╚════╝░╚═╝░░╚══╝╚═════╝░
        // #################################################################################################################
        // #################################################################################################################
        // #################################################################################################################

        BigNumb copy(unsigned long long intLen = 0, unsigned long long decLen = 0) {
            BigNumb res;
            res.integerLength = (intLen == 0 ? integerLength : intLen);
            res.decimalLength = (decLen == 0 ? decimalLength : decLen);
            res.integerSection = (INT *) malloc(sizeof(INT) * res.integerLength);
            res.decimalSection = (INT *) malloc(sizeof(INT) * res.decimalLength);

            if (intLen == 0) {
                memcpy((void *) res.integerSection, (void *) integerSection, sizeof(INT) * integerLength);
            } else {
                for (unsigned long long i = 0; i < res.integerLength; i++) {
                    res.integerSection[i] = i < integerLength ? integerSection[i] : 0;
                }
            }

            if (decLen == 0) {
                memcpy((void *) res.decimalSection, (void *) decimalSection, sizeof(INT) * decimalLength);
            } else {
                for (unsigned long long i = 0; i < res.decimalLength; i++) {
                    res.decimalSection[i] = i < decimalLength ? decimalSection[i] : 0;
                }
            }

            res.isNegative = isNegative;

            return res;
        }

        BigNumb round() {
            BigNumb res = copy();

            // Check the current context's rounding option
            if (context.rounding == Context::ROUND_TO_ZERO) {
                if (!isNegative) {
                    // Remove the decimal part of the number
                    res.decimalSection = (INT *) malloc(sizeof(INT));
                    res.decimalSection[0] = 0;
                    res.decimalLength = 0;
                } else {
                    // If decimal part != 0 then add 1
                    bool isZero = true;
                    for (unsigned long long i = 0; i < decimalLength; i++) {
                        if (decimalSection[i] != 0) {
                            isZero = false;
                            break;
                        }
                    }

                    if (!isZero) {
                        // Remove the decimal part of the number
                        res.decimalSection = (INT *) malloc(sizeof(INT));
                        res.decimalSection[0] = 0;
                        res.decimalLength = 0;

                        res = res + BigNumb(1);
                    }
                }
            }

            return res;
        }

        // Remove leading and trailing zeros
        BigNumb operator+() const {
            BigNumb res;

            unsigned long long leading, trailing;
            leading = trailing = 0;

            bool cont = true;
            while (cont && leading < integerLength) {
                if (integerSection[integerLength - leading - 1] == 0) {
                    leading++;
                } else {
                    cont = false;
                }
            }

            cont = true;
            while (cont && trailing < decimalLength) {
                if (decimalSection[decimalLength - trailing - 1] == 0) {
                    trailing++;
                } else {
                    cont = false;
                }
            }

            if (integerLength - leading == 0) {
                leading--;
            }

            if (decimalLength - trailing == 0) {
                trailing--;
            }

            INT *intFixed = (INT *) malloc(sizeof(INT) * (integerLength - leading));
            INT *decFixed = (INT *) malloc(sizeof(INT) * (decimalLength - trailing));

            for (unsigned long long i = 0; i < (integerLength - leading); i++) {
                intFixed[i] = integerSection[i];
            }

            for (unsigned long long i = 0; i < (decimalLength - trailing); i++) {
                decFixed[i] = decimalSection[i];
            }

            res.integerSection = intFixed;
            res.decimalSection = decFixed;
            res.integerLength = integerLength - leading;
            res.decimalLength = decimalLength - trailing;

            return res;
        }

        // Remove leading and trailing zeros
        BigNumb operator-() {
            BigNumb res = copy();
            res.isNegative = !res.isNegative;
            return res;
        }

        // #################################################################################################################
        // #################################################################################################################
        // #################################################################################################################
        //              ░█████╗░░█████╗░███╗░░██╗██╗░░░██╗███████╗██████╗░░██████╗██╗░█████╗░███╗░░██╗░██████╗
        //              ██╔══██╗██╔══██╗████╗░██║██║░░░██║██╔════╝██╔══██╗██╔════╝██║██╔══██╗████╗░██║██╔════╝
        //              ██║░░╚═╝██║░░██║██╔██╗██║╚██╗░██╔╝█████╗░░██████╔╝╚█████╗░██║██║░░██║██╔██╗██║╚█████╗░
        //              ██║░░██╗██║░░██║██║╚████║░╚████╔╝░██╔══╝░░██╔══██╗░╚═══██╗██║██║░░██║██║╚████║░╚═══██╗
        //              ╚█████╔╝╚█████╔╝██║░╚███║░░╚██╔╝░░███████╗██║░░██║██████╔╝██║╚█████╔╝██║░╚███║██████╔╝
        //              ░╚════╝░░╚════╝░╚═╝░░╚══╝░░░╚═╝░░░╚══════╝╚═╝░░╚═╝╚═════╝░╚═╝░╚════╝░╚═╝░░╚══╝╚═════╝░
        // #################################################################################################################
        // #################################################################################################################
        // #################################################################################################################

        explicit operator char *() const {
            BigNumb fixed = operator+();

            unsigned long long length = fixed.integerLength;
            if (fixed.decimalLength > 0 || fixed.decimalSection[0] > 0) length += fixed.decimalLength + 1;
            if (fixed.isNegative) length += 1;
            unsigned long long resIndex = 0;

            char *res = (char *) malloc(sizeof(char) * (length));

            if (fixed.isNegative) {
                res[resIndex++] = '-';
            }

            unsigned long long start = fixed.isNegative ? 1 : 0;
            unsigned long long index = fixed.integerLength - 1;
            for (unsigned long long i = start; i < fixed.integerLength + start; i++) {
                res[resIndex++] = gpc::conversion::type::intToChar(fixed.integerSection[index--]);
            }

            res[resIndex++] = '.';

            if (fixed.decimalLength > 1 || fixed.decimalSection[0] > 0) {
                for (unsigned long long i = 0; i < fixed.decimalLength; i++) {
                    res[resIndex++] = gpc::conversion::type::intToChar(fixed.decimalSection[i]);
                }
            }

            res[resIndex] = '\0';
            return res;
        }

        std::string toString() const {
            return std::string(operator char *());
        }

#   undef INT
    };

    std::string to_string(BigNumb val) {
        return std::string((char *) val);
    }

    std::ostream &operator<<(std::ostream &os, BigNumb val) {
        return os << (char *) val;
    }
} }

/*
    class BigNumb {
    public:
        unsigned long long digitsBeforeDecimal;
        unsigned long long digitsAfterDecimal;

        int16_t *digitsBefore;
        int16_t *digitsAfter;
        bool positive;

        BigNumb() {
            digitsBeforeDecimal = 1;
            digitsAfterDecimal = 1;
            digitsBefore = (int16_t *) calloc(sizeof(int16_t), 1);
            digitsAfter = (int16_t *) calloc(sizeof(int16_t), 1);
            positive = true;
        }

        explicit BigNumb(std::string string) {
            unsigned long long charsBefDec = 0;
            unsigned long long charsAftDec = 0;
            bool loadInt = true;

            // Do some preliminary counting
            for (int i = 0; i < string.length(); i++) {
                if (string[i] == '.') loadInt = false;
                else if (string[i] == '-') positive = false;
                else {
                    // Check for an integer:
                    if (
                            string[i] == '1' || string[i] == '2' ||
                            string[i] == '3' || string[i] == '4' ||
                            string[i] == '5' || string[i] == '6' ||
                            string[i] == '7' || string[i] == '8' ||
                            string[i] == '9' || string[i] == '0'
                            ) {
                        if (loadInt) charsBefDec++;
                        else charsAftDec++;

                        std::cout << "FOUND A NUMBER! " << string[i] << "\n";
                    } else {
                        printf("Invalid character in string conversion (%s) to BigNumb: %c\n", string.c_str(), string[i]);
                        exit(EXIT_FAILURE);
                    }
                }
            }

            digitsBefore = (int16_t *) malloc(sizeof(int16_t) * charsBefDec);

            if (!digitsBefore) {
                printf("Unable to allocate memory\n");
                exit(EXIT_FAILURE);
            }

            if (charsAftDec > 0) {
                digitsAfter = (int16_t *) malloc(sizeof(int16_t) * charsAftDec);

                if (!digitsAfter) {
                    printf("Unable to allocate memory\n");
                    exit(EXIT_FAILURE);
                }
            } else {
                digitsAfter = (int16_t *) malloc(sizeof(int16_t) * 1);

                if (!digitsAfter) {
                    printf("Unable to allocate memory\n");
                    exit(EXIT_FAILURE);
                }

                digitsAfter[0] = 0;
            }

            // unsigned long long start = 0;
            unsigned long long start = !positive;
            // if (!positive) start = 1;

            for (unsigned long long i = start; i < charsBefDec + start; i++) {
                digitsBefore[i - start] = charToInt(string[i]);
                std::cout << "Digit before: " << digitsBefore[i - start] << "\n";
            }

            for (unsigned long long i = 0; i < charsAftDec; i++) {
                digitsAfter[i] = charToInt(string[i + 1 + charsBefDec + start]);
            }

            digitsBeforeDecimal = charsBefDec;
            if (charsAftDec > 0) digitsAfterDecimal = charsAftDec;
            else digitsAfterDecimal = 1;
        }

        static BigNumb empty(unsigned long long digitsBefore, unsigned long long digitsAfter) {
            auto res = BigNumb("0.0");

            res.digitsBeforeDecimal = digitsBefore;
            res.digitsAfterDecimal = digitsAfter;

            res.digitsBefore = (int16_t *) calloc(sizeof(int16_t), digitsBefore);
            res.digitsAfter = (int16_t *) calloc(sizeof(int16_t), digitsAfter);

            if (!res.digitsBefore || !res.digitsAfter) {
                printf("Unable to allocate memory\n");
                exit(EXIT_FAILURE);
            }

            return res;
        }

        [[nodiscard]] BigNumb _removeZeros() const {
            unsigned long long leading, trailing;
            leading = trailing = 0;

            for (unsigned long long i = 0; i < digitsBeforeDecimal - 1; i++) {
                if (digitsBefore[i] == '0') leading++;
                else break;
            }

            for (unsigned long long i = digitsAfterDecimal; i > 0; i--) {
                if (digitsAfter[i - 1] == 0) trailing++;
                else break;
            }

            BigNumb res = empty(digitsBeforeDecimal - leading, digitsAfterDecimal - trailing);
            for (int i = 0; i < res.digitsBeforeDecimal; i++)
                res.digitsBefore[i] = digitsBefore[i + leading];

            for (int i = 0; i < res.digitsAfterDecimal; i++)
                res.digitsAfter[i] = digitsAfter[i];

            return res;
        }

        explicit operator char *() const {
            BigNumb numb = _removeZeros();
            char *res = (char *) malloc(sizeof(char) * (digitsBeforeDecimal + digitsAfterDecimal + 2));
            unsigned long long index = 0;

            if (!numb.positive) res[index++] = '-';

            for (int i = 0; i < numb.digitsBeforeDecimal; i++)
                res[index++] = numb.digitsBefore[i];

            if (numb.digitsAfterDecimal > 0 || numb.digitsAfter[0] != 0) {
                res[index++] = '.';

                for (int i = 0; i < numb.digitsAfterDecimal; i++)
                    res[index++] = numb.digitsAfter[i];
            }

            return res;
        }
    };

    BigNumb copy(BigNumb a) {
        BigNumb res = fromString("0.0");

        res.digitsBefore = (int16_t *) realloc(res.digitsBefore, sizeof(int16_t) * a.digitsBeforeDecimal);
        res.digitsAfter = (int16_t *) realloc(res.digitsAfter, sizeof(int16_t) * a.digitsAfterDecimal);

        if (!res.digitsBefore || !res.digitsAfter) {
            printf("Unable to reallocate memory\n");
            exit(EXIT_FAILURE);
        }

        res.digitsBeforeDecimal = a.digitsBeforeDecimal;
        res.digitsAfterDecimal = a.digitsAfterDecimal;
        res.positive = a.positive;

        for (unsigned long long i = 0; i < res.digitsBeforeDecimal; i++) {
            res.digitsBefore[i] = a.digitsBefore[i];
        }

        for (unsigned long long i = 0; i < res.digitsAfterDecimal; i++) {
            res.digitsAfter[i] = a.digitsAfter[i];
        }

        return res;
    }

    BigNumb negative(BigNumb val) {
        BigNumb res = copy(val);
        res.positive = !res.positive;

        return res;
    }

    bool greaterThan(BigNumb a, BigNumb b) {
        if (a.positive && !b.positive) {
            return true;
        } else if (!a.positive && b.positive) {
            return false;
        } else if (!a.positive && !b.positive) {
            return greaterThan(negative(b), negative(a));
        } else if (a.digitsBeforeDecimal < b.digitsBeforeDecimal) {
            return false;
        } else if (a.digitsBeforeDecimal > b.digitsBeforeDecimal) {
            return true;
        }

        // Both numbers have the same length integer part
        for (int i = 0; i < a.digitsBeforeDecimal; i++) {
            if (a.digitsBefore[i] > b.digitsBefore[i]) {
                return true;
            } else if (a.digitsBefore[i] < b.digitsBefore[i]) {
                return false;
            }
        }

        // Same integer part
        for (int i = 0; i < longMin(a.digitsAfterDecimal, b.digitsBeforeDecimal); i++) {
            if (a.digitsAfter[i] > b.digitsAfter[i]) {
                return true;
            } else if (a.digitsAfter[i] < b.digitsAfter[i]) {
                return false;
            }
        }

        return false;
    }

    bool lessThan(BigNumb a, BigNumb b) {
        if (a.positive && !b.positive) {
            return false;
        } else if (!a.positive && b.positive) {
            return true;
        } else if (!a.positive && !b.positive) {
            return lessThan(negative(b), negative(a));
        } else if (a.digitsBeforeDecimal < b.digitsBeforeDecimal) {
            return true;
        } else if (a.digitsBeforeDecimal > b.digitsBeforeDecimal) {
            return false;
        }

        // Both numbers have the same length integer part
        for (int i = 0; i < a.digitsBeforeDecimal; i++) {
            if (a.digitsBefore[i] > b.digitsBefore[i]) {
                return false;
            } else if (a.digitsBefore[i] < b.digitsBefore[i]) {
                return true;
            }
        }

        // Same integer part
        for (int i = 0; i < longMin(a.digitsAfterDecimal, b.digitsBeforeDecimal); i++) {
            if (a.digitsAfter[i] > b.digitsAfter[i]) {
                return false;
            } else if (a.digitsAfter[i] < b.digitsAfter[i]) {
                return true;
            }
        }

        return false;
    }

    bool equalTo(BigNumb a, BigNumb b) {
        if (a.positive && !b.positive) {
            return false;
        } else if (!a.positive && b.positive) {
            return false;
        } else if (a.digitsBeforeDecimal < b.digitsBeforeDecimal) {
            return false;
        } else if (a.digitsBeforeDecimal > b.digitsBeforeDecimal) {
            return false;
        }

        // Both numbers have the same length integer part
        for (int i = 0; i < a.digitsBeforeDecimal; i++) {
            if (a.digitsBefore[i] != b.digitsBefore[i]) {
                return false;
            }
        }

        // Same integer part
        for (int i = 0; i < longMin(a.digitsAfterDecimal, b.digitsBeforeDecimal); i++) {
            if (a.digitsAfter[i] != b.digitsAfter[i]) {
                return false;
            }
        }

        return true;
    }

    BigNumb __add(BigNumb a, BigNumb b) {
        BigNumb res = empty(longgpc::math::max(a.digitsBeforeDecimal, b.digitsBeforeDecimal) + 1,
                           longgpc::math::max(a.digitsAfterDecimal, b.digitsAfterDecimal));

        for (unsigned long long i = res.digitsAfterDecimal; i > 0; i--) {
            if (i - 1 < a.digitsAfterDecimal) {
                res.digitsAfter[i - 1] += a.digitsAfter[i - 1];
            }

            if (i - 1 < b.digitsAfterDecimal) {
                res.digitsAfter[i - 1] += b.digitsAfter[i - 1];
            }

            if (i - 1 > 0) {
                if (res.digitsAfter[i - 1] > 9) {
                    res.digitsAfter[i - 1] -= 10;
                    res.digitsAfter[i - 2] += 1;
                }
            } else {
                if (res.digitsAfter[i - 1] > 9) {
                    res.digitsAfter[i - 1] -= 10;
                    res.digitsBefore[res.digitsBeforeDecimal - 1] += 1;
                }
            }
        }

        unsigned long long resIndex = res.digitsBeforeDecimal - 1;
        unsigned long long aIndex = a.digitsBeforeDecimal - 1;
        unsigned long long bIndex = b.digitsBeforeDecimal - 1;

        for (unsigned long long i = res.digitsBeforeDecimal; i > 0; i--) {
            if (aIndex < a.digitsBeforeDecimal) {
                res.digitsBefore[resIndex] += a.digitsBefore[aIndex];
            }

            if (bIndex < b.digitsBeforeDecimal) {
                res.digitsBefore[resIndex] += b.digitsBefore[bIndex];
            }

            if (res.digitsBefore[resIndex] > 9) {
                res.digitsBefore[resIndex] -= 10;
                res.digitsBefore[resIndex - 1] += 1;
            }

            resIndex--;
            aIndex--;
            bIndex--;
        }

        return _removeZeros(res);
    }

    BigNumb __sub(BigNumb a, BigNumb b) {
        if (lessThan(a, b)) {
            return negative(__sub(b, a));
        }

        BigNumb res = empty(longgpc::math::max(a.digitsBeforeDecimal, b.digitsBeforeDecimal),
                           longgpc::math::max(a.digitsAfterDecimal, b.digitsAfterDecimal));

        for (unsigned long long i = 0; i < a.digitsAfterDecimal; i++) {
            res.digitsAfter[i] = a.digitsAfter[i];
        }

        for (unsigned long long i = 0; i < a.digitsBeforeDecimal; i++) {
            res.digitsBefore[i] = a.digitsBefore[i];
        }

        for (unsigned long long i = res.digitsAfterDecimal; i > 0; i--) {
            int16_t valB = 0;

            if (i - 1 < b.digitsAfterDecimal) {
                valB = b.digitsAfter[i - 1];
            }

            res.digitsAfter[i - 1] -= valB;

            for (unsigned long long j = i; j > 0; j--) {
                if (res.digitsAfter[j - 1] < 0) {
                    if (j - 2 >= 0) {
                        res.digitsAfter[j - 1] += 10;
                        res.digitsAfter[j - 2] -= 1;
                    } else {
                        res.digitsAfter[j - 1] += 10;
                        res.digitsBefore[res.digitsBeforeDecimal - 1] -= 1;
                    }
                }
            }
        }

        unsigned long long bIndex = b.digitsBeforeDecimal - 1;
        for (unsigned long long i = res.digitsBeforeDecimal; i > 0; i--) {
            int16_t valB = 0;

            if (bIndex >= 0) {
                valB = b.digitsBefore[bIndex];
                bIndex--;
            }

            res.digitsBefore[i - 1] -= valB;

            for (unsigned long long j = i; j > 0; j--) {
                if (res.digitsBefore[j - 1] < 0) {
                    res.digitsBefore[j - 1] += 10;
                    res.digitsBefore[j - 2] -= 1;
                }
            }
        }

        return _removeZeros(res);
    }

    BigNumb __mul(BigNumb aVal, BigNumb bVal) {
        unsigned long long aLen = aVal.digitsBeforeDecimal + aVal.digitsAfterDecimal;
        unsigned long long bLen = bVal.digitsBeforeDecimal + bVal.digitsAfterDecimal;
        unsigned long long resLen = longgpc::math::max(aVal.digitsBeforeDecimal + aVal.digitsAfterDecimal,
                                            bVal.digitsBeforeDecimal + bVal.digitsAfterDecimal) * 2;

        int16_t *a = (int16_t *) malloc(sizeof(int16_t) * aLen);
        int16_t *b = (int16_t *) malloc(sizeof(int16_t) * bLen);
        int16_t *res = (int16_t *) malloc(sizeof(int16_t) * resLen);

        for (int i = 0; i < aVal.digitsBeforeDecimal + aVal.digitsAfterDecimal; i++) {
            int16_t val = 0;
            if (i < aVal.digitsBeforeDecimal) {
                val = aVal.digitsBefore[i];
            } else {
                val = aVal.digitsAfter[i - aVal.digitsBeforeDecimal];
            }

            a[i] = val;
        }

        for (int i = 0; i < bVal.digitsBeforeDecimal + bVal.digitsAfterDecimal; i++) {
            int16_t val = 0;
            if (i < bVal.digitsBeforeDecimal) {
                val = bVal.digitsBefore[i];
            } else {
                val = bVal.digitsAfter[i - bVal.digitsBeforeDecimal];
            }

            b[i] = val;
        }

        for (int i = 0; i < bVal.digitsBeforeDecimal + bVal.digitsAfterDecimal; i++) {
            res[i] = 0;
        }

        for (unsigned long long i = aLen; i > 0; i--) {
            for (unsigned long long j = bLen; j > 0; j--) {
                int16_t tmp = a[i - 1] * b[j - 1];

                res[(resLen - 1) - (aLen - i - 1) - (bLen - j - 1) - 2] += tmp % 10;

                res[(resLen - 1) - (aLen - i - 1) - (bLen - j - 1) - 3] += floor((double) tmp / 10);

                for (unsigned long long k = resLen; k > 0; k--) {
                    tmp = res[k - 1];

                    if (tmp > 9) {
                        res[k - 1] = tmp % 10;
                        res[k - 2] += floor((double) tmp / 10);
                    }
                }
            }
        }

        unsigned long long decLen = (aVal.digitsAfterDecimal + bVal.digitsAfterDecimal);
        unsigned long long intLen = resLen - decLen;

        unsigned long long leadingZeros = 0;
        bool cont = true;
        while (cont && leadingZeros < intLen) {
            if (res[leadingZeros] == 0) {
                leadingZeros++;
            } else {
                cont = false;
            }
        }

        unsigned long long trailingZeros = 0;
        cont = true;
        while (cont) {
            if (res[resLen - trailingZeros - 1] == 0 && trailingZeros < aVal.digitsAfterDecimal + bVal.digitsAfterDecimal) {
                trailingZeros++;
            } else {
                cont = false;
            }
        }

        BigNumb resNumb = empty(longgpc::math::max(intLen - leadingZeros, 1), longgpc::math::max(decLen - trailingZeros, 1));

        unsigned long long j = leadingZeros;
        if (leadingZeros != intLen) {
            for (unsigned long long i = 0; i < resNumb.digitsBeforeDecimal; i++) {
                resNumb.digitsBefore[i] = res[j];
                j++;
            }
        } else {
            resNumb.digitsBefore[0] = 0;
        }

        for (unsigned long long i = 0; i < resNumb.digitsAfterDecimal; i++) {
            resNumb.digitsAfter[i] = res[j];
            j++;
        }

        return _removeZeros(resNumb);
    }

    BigNumb __div(BigNumb a, BigNumb b) {
        BigNumb mult = fromString("0");

        long long zeros = (long long) (a.digitsBeforeDecimal + a.digitsAfterDecimal);
        bool intPart = true;

        char *checkerString = (char *) malloc(sizeof(char) * CLibSetup.prec - 7);
        unsigned long long len = CLibSetup.prec - 7;
        for (int i = 0; i < len; i++) {
            if (i == 1) {
                checkerString[i] = '.';
            } else if (i == len - 1) {
                checkerString[i] = '1';
            } else {
                checkerString[i] = '0';
            }
        }
        BigNumb checker = fromString(checkerString);

        while (lessThan(mul(b, mult), a)) {
            printf("Got a less than!\n");
            print(mul(b, mult));
            print(mult);
            printf("%i\n", lessThan(mul(b, mult), a));

            if (intPart) {
                char *zeroStringString = (char *) malloc(sizeof(char) * (zeros + 1));
                for (int i = 0; i < zeros + 1; i++) {
                    if (i == 0) {
                        zeroStringString[i] = '1';
                    } else {
                        zeroStringString[i] = '0';
                    }
                }

                BigNumb zeroString = fromString(zeroStringString);

                if (lessThan(mul(add(mult, zeroString), b), a) || equalTo(mul(add(mult, zeroString), b), a)) {
                    mult = add(mult, zeroString);

                    BigNumb delta = sub(a, mul(b, mult));
                    if (delta.positive) {
                        if (lessThan(delta, checker) || equalTo(delta, checker)) {
                            return mult;
                        }
                    } else {
                        if (lessThan(negative(delta), checker) || equalTo(negative(delta), checker)) {
                            return mult;
                        }
                    }
                } else {
                    zeros--;
                }

                if (zeros < 0) {
                    intPart = false;
                }
            } else {
                char *zeroStringString = (char *) malloc(sizeof(char) * (zeros + 4));
                for (unsigned long long i = 0; i < zeros + 3; i++) {
                    zeroStringString[i] = '0';
                }

                zeroStringString[1] = '.';
                zeroStringString[zeros + 2] = '1';
                BigNumb zeroString = fromString(zeroStringString);

                free(zeroStringString);

                if (lessThan(mul(add(mult, zeroString), b), a) || equalTo(mul(add(mult, zeroString), b), a)) {
                    mult = add(mult, zeroString);
                } else {
                    zeros++;
                }

                BigNumb delta = sub(a, mul(b, mult));
                if (delta.positive) {
                    if (lessThan(delta, checker) || equalTo(delta, checker)) {
                        printf("Returning here 1\n");
                        return mult;
                    }
                } else {
                    if (lessThan(negative(delta), checker) || equalTo(negative(delta), checker)) {
                        printf("Returning here 2\n");
                        return mult;
                    }
                }

                /
                if (zeros > CLibSetup.prec) {
                    printf("Returning here 3\n");
                    return mult;
                }
                //
            }
        }

        printf("Got a greater than!\n");
        print(mul(b, mult));
        print(mult);
        printf("%i\n", lessThan(mul(b, mult), a));

        printf("Returning here 4\n");
        return mult;
    }

    BigNumb add(BigNumb a, BigNumb b) {
        BigNumb res;

        if (a.positive && b.positive) {
            res = __add(a, b);
        } else if (a.positive && !b.positive) {
            if (greaterThan(negative(b), a)) {
                res = __sub(negative(b), a);
                res.positive = !res.positive;
            } else if (lessThan(negative(b), a)) {
                res = __sub(a, negative(b));
            } else {
                res = fromString("0");
            }
        } else if (!a.positive && b.positive) {
            if (greaterThan(negative(a), b)) {
                res = __sub(negative(a), b);
                res.positive = !res.positive;
            } else if (lessThan(negative(a), b)) {
                res = __sub(b, negative(a));
            } else {
                res = fromString("0");
            }
        } else if (!a.positive && !b.positive) {
            res = __add(a, b);
            res.positive = !res.positive;
        } else {
            printf("Invalid values for add(a, b)");
            exit(EXIT_FAILURE);
        }

        return res;
    }

    BigNumb sub(BigNumb a, BigNumb b) {
        BigNumb res;

        if (a.positive && b.positive) {
            res = __sub(a, b);
        } else if (a.positive && !b.positive) {
            res = __add(a, negative(b));
        } else if (!a.positive && b.positive) {
            res = __add(negative(a), b);
            res.positive = !res.positive;
        } else if (!a.positive && !b.positive) {
            res = __sub(negative(a), negative(b));
            res.positive = !res.positive;
        } else {
            printf("Invalid values for add(a, b)");
            exit(EXIT_FAILURE);
        }

        return res;
    }

    BigNumb mul(BigNumb a, BigNumb b) {
        BigNumb res = _mul(a, b);
        res.positive = (a.positive ^ b.positive) == 1 ? 0 : 1;

        return res;
    }
    */

#endif //CLIB_BIGNUMB_H
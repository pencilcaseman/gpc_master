//
// Created by Toby Davis on 13/04/2020.
//

#ifndef GPC_TEMPBit_HPP
#define GPC_TEMPBit_HPP

#include <cstdlib>

namespace gpc {
    namespace bit {
        /*
         * Class to store a single binary value. Used to store and allow index operators
         * for the dynamic bitset class
         */
        class Bit {
        private:
            // The number value
            uint32_t *binVal;
            // nth Bit
            unsigned long long n;
            // last set bits (for code optimizations)
            unsigned long long *lastSet;
            unsigned long long binIndex;

        public:
            /// Create a single Bit (Used internally -- not designed for mainstream use)
            /// \param val Pointer to <unsigned long long> value containing the desired Bit
            /// \param index nth Bit of value to access
            /// \param _firstSet First set Bit of <DynamicBitset> value containing the accessed Bit
            /// \param _binIndex Last set Bit of <DynamicBitset> value containing the accessed Bit
            Bit(uint32_t *val, unsigned long long index, unsigned long long *_lastSet,
                unsigned long long _binIndex) {
                binVal = val;
                n = index;
                lastSet = _lastSet;
                binIndex = _binIndex;
            }

            /// Assignment operator
            /// \param val Value to set Bit to. If greater than 0 Bit is set to 1, otherwise 0
            /// \return Bit type
            Bit &operator=(uint32_t val) {
                if (val > 0) {
                    *binVal |= (uint32_t) ((uint32_t) 1 << n);
                } else {
                    *binVal &= (uint32_t)
                            ~((uint32_t) 1 << n);
                }
                return *this;
            }

            /// Cast Bit to a <char>
            /// \return '1' or '0'
            explicit operator char() {
                return (*binVal & (uint32_t) ((uint32_t) 1 << n)) > 0 ? '1' : '0';
            }

            /// Cast Bit to an <int>
            /// \return 1 or 0
            explicit operator int() {
                return (*binVal & (uint32_t) ((uint32_t) 1 << n)) > 0 ? 1 : 0;

            }
        };
    }
}

#endif

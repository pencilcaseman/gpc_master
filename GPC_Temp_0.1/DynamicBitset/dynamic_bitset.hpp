//
// Created by penci on 22/03/2020.
//

#ifndef GPC_TEMP_DYNAMICBITSET_HPP
#define GPC_TEMP_DYNAMICBITSET_HPP

#include <iostream>
#include <cstdlib>
#include <cstring>
#include "bit.hpp"

namespace gpc {
    namespace dynamic_bitset {
        /**
         * A class to create arrays of single bits, access individual bits and
         * do some operations on those bits
         */
        class DynamicBitset {
        private:
            // Number of <uint32_t> values
            unsigned long long binLen;
            // Array of <uint32_t> values
            uint32_t *bin;
            // Number of bits
            unsigned long long bits;
            // Last (most significant) set bit
            unsigned long long lastSetBit;

            /**
             * Set the <lastSetBit> to the last non-zero value
             * in the <bin> array
             */
            // Find the first and last values with at least one 'on' Bit
            void setLastBit() {
                // Loop all stored values
                for (unsigned long long i = 0; i < binLen; i++) {
                    if (bin[i] > 0) lastSetBit = i;
                }
            }

            /**
             * Convert a number of bits to an index in a <uint32_t *> array
             * @param b Number of bits
             * @return Equivalent index in array
             */
            [[nodiscard]] static unsigned long long btou(unsigned long long b) {
                return b / ((sizeof(uint32_t) * 8) + 1);
            }

            /**
             * Convert an index in a <uint32_t> array to a number of bits
             * @param b Index
             * @return Equivalent number of bits
             */
            [[nodiscard]] static unsigned long long utob(unsigned long long u) {
                return (u + 1) * (sizeof(uint32_t) * 8);
            }

            /**
             * Shift all bits one to the left
             *
             * This is an optimised method for bit shifting for
             * the purpose of increasing speeds in certain
             * operations where only 1 shift is needed
             *
             * @return Shifted DynamicBitset object
             */
            [[nodiscard]] DynamicBitset _lShift1_() const {
                DynamicBitset res = copy(bits + 1);

                uint32_t addToEnd = 0;
                for (unsigned long long i = 0; i < binLen + 1; i++) {
                    res.bin[i] = bin[i] << 1u;
                    res.bin[i] |= addToEnd;

                    addToEnd = bin[i] >> (sizeof(uint32_t) * (uint32_t) 8 -
                                          1); // (bin[i] & ((uint32_t) 1 << (uint32_t) (sizeof(uint32_t) * 8 - 1))) > 0 ? 1 : 0;
                }

                return res;
            }

        public:
            /**
             * Create an empty DynamicBitset object.
             * Value is initialized to zero and 32 bits are allocated
             */
            explicit DynamicBitset() {
                binLen = 1;
                bin = (uint32_t *) calloc(1, sizeof(uint32_t));
                bits = sizeof(uint32_t) * 8;
                lastSetBit = 0;
            }

            /*
            ~DynamicBitset() {
            	free(bin);
            }
             */

            /**
             * Create a DynamicBitset object from an integer value
             * The binary value is initialized to store that value
             *
             * @param val The value to initialize the object with
             */
            explicit DynamicBitset(int val) {
                binLen = 1;
                bin = (uint32_t *) malloc(sizeof(uint32_t));
                bits = sizeof(uint32_t) * 8;
                bin[0] = val;
                lastSetBit = 0;
            }

            /**
             * Create a DynamicBitset object from an unsigned integer value
             * The binary value is initialized to store that value
             *
             * @param val The value to initialize the object with
             */
            explicit DynamicBitset(unsigned int val) {
                binLen = 1;
                bin = (uint32_t *) malloc(sizeof(uint32_t));
                bits = sizeof(uint32_t) * 8;
                bin[0] = val;
                lastSetBit = 0;
            }

            /**
             * Create a DynamicBitset object from an unsigned long value
             * The binary value is initialized to store that value
             *
             * @param val The value to initialize the object with
             */
            explicit DynamicBitset(unsigned long val) {
                binLen = 1;
                bin = (uint32_t *) malloc(sizeof(uint32_t));
                bits = sizeof(uint32_t) * 8;
                bin[0] = val;
                lastSetBit = 0;
            }

            /**
             * Create a DynamicBitset object from an unsigned long long value
             * The binary value is initialized to store that value
             *
             * @param val The value to initialize the object with
             */
            explicit DynamicBitset(unsigned long long val) {
                binLen = 1;
                bin = (uint32_t *) malloc(sizeof(uint32_t));
                bits = sizeof(uint32_t) * 8;
                bin[0] = (uint32_t) val;
                bin[1] = (uint32_t) (val >> (sizeof(int32_t) * 8));
                lastSetBit = 0;
            }

            /**
             * Create a DynamicBitset object given a value and a number
             * of bits to allocate. The binary value is initialized to
             * store that value and all remaining bits are set to zero
             *
             * @param val The value to initialize the object with
             */
            explicit DynamicBitset(unsigned long long len, uint32_t val) {
                binLen = (len - 1) / ((sizeof(uint32_t) * 8)) + 1;
                bin = (uint32_t *) calloc((size_t) binLen, sizeof(uint32_t) * 8);
                bits = len;
                bin[0] = val;
                lastSetBit = 0;
            }

            /**
             * Shorten the binary array by removing any excess bits
             * at the beginning (significant bits)
             */
            void fix() {
                // Set the last bit
                setLastBit();
                // Set the number of bits
                bits = utob(lastSetBit);
                // Initialize <binLen>
                binLen = lastSetBit + 1;
                // Allocate the new bits
                auto nBin = (uint32_t *) malloc(sizeof(uint32_t) * lastSetBit + 1);
                // Fill the new bits with values
                for (unsigned long long i = 0; i < lastSetBit + 1; i++) { nBin[i] = bin[i]; }
                // Set <bin> to the newly allocated bits
                bin = nBin;
            }

            /**
             * Return the value of the nth Bit
             * \param n Nth Bit
             * \return Value of the nth Bit (0 / 1, 2, 4, 8 ...)
             */
            [[nodiscard]] unsigned long long testBit(unsigned long long n) const {
                return bin[n / (sizeof(uint32_t) * 8)] & ((uint32_t) 1 << n % (sizeof(uint32_t) * 8));
            }

            /**
             * Force the nth Bit to be 'on'
             * \param n Nth Bit
             */
            void setBit(unsigned long long n) const {
                bin[n / (sizeof(uint32_t) * 8)] |= ((uint32_t) 1 << n % (sizeof(uint32_t) * 8));
            }

            /**
             * Force the nth Bit to be 'off'
             * \param n Nth Bit
             */
            void clearBit(unsigned long long n) const {
                bin[n / (sizeof(uint32_t) * 8)] &= ~((uint32_t) 1 << n % (sizeof(uint32_t) * 8));
            }

            /**
             * Create an exact copy of a <DynamicBitset> value
             * \return Exact copy of value
             */
            [[nodiscard]] DynamicBitset copy(unsigned long long n = 0) const {
                DynamicBitset res;

                if (n == 0) {
                    memcpy((void *) res.bin, (void *) bin, sizeof(uint32_t) * binLen);
                    // Set tracking values
                    res.binLen = binLen;
                    res.bits = bits;
                } else {
                    res.bin = (uint32_t *) calloc(DynamicBitset::btou(n), sizeof(uint32_t));
                    for (unsigned long long i = 0; i < binLen; i++) {
                        res.bin[i] = bin[i];
                    }
                    res.bits = n;
                    res.binLen = DynamicBitset::btou(n);
                }

                return res;
            }

            /**
             * Create an empty <DynamicBitset> instance with a specified number of bits
             * \param bits Number of bits to allocate
             * \return Empty instance of <DynamicBitset>
             */
            [[nodiscard]] static DynamicBitset empty(unsigned long long bits) {
                DynamicBitset res;

                // Initialise all values
                res.binLen = ((bits - 1) / (sizeof(uint32_t) * 8)) + 1;
                res.bits = bits;
                res.bin = (uint32_t *) calloc(res.binLen, (sizeof(uint32_t) * 8));

                for (unsigned long long i = 0; i < res.binLen; i++) {
                    res.bin[i] = 0;
                }

                res.lastSetBit = 0;

                return res;
            }

            /**
             * Access the nth Bit of the value
             * \param index The nth Bit to access
             * \return <Bit> type representing the Bit
             */
            [[nodiscard]] gpc::bit::Bit operator[](unsigned long long index) {
                return {&bin[index / (sizeof(uint32_t) * 8)], index % (sizeof(uint32_t) * 8), &lastSetBit,
                        DynamicBitset::btou(index)};
            }

            /**
             * Bitwise AND with another instance of <DynamicBitset>
             * \param other Other <DynamicBitset> value
             * \return Result of bitwise AND operation
             */
            [[nodiscard]] DynamicBitset operator&(DynamicBitset other) {
                setLastBit();
                other.setLastBit();

                // The longest value
                unsigned long long max = lastSetBit > other.lastSetBit ? lastSetBit + 1 : other.lastSetBit + 1;
                auto res = empty(max * (sizeof(uint32_t) * 8));

                // Loop over the longest value
                for (unsigned long long i = 0; i < max; i++) {
                    // Values representing the number at index <i> for <*this> and <other>
                    uint32_t aVal, bVal;
                    aVal = bVal = 0;

                    if (i < binLen) aVal = bin[i];
                    if (i < other.binLen) bVal = other.bin[i];

                    res.bin[i] = aVal & bVal;
                }

                return res;
            }

            /**
             * Bitwise OR with another instance of <DynamicBitset>
             * \param other Other <DynamicBitset> value
             * \return Result of bitwise OR operation
             */
            [[nodiscard]] DynamicBitset operator|(DynamicBitset other) {
                // See <operator&> for workings
                setLastBit();
                other.setLastBit();

                unsigned long long max = lastSetBit > other.lastSetBit ? lastSetBit + 1 : other.lastSetBit + 1;
                auto res = empty(max * (sizeof(uint32_t) * 8));

                for (unsigned long long i = 0; i < max; i++) {
                    uint32_t aVal, bVal;
                    aVal = bVal = 0;

                    if (i < binLen) aVal = bin[i];
                    if (i < other.binLen) bVal = other.bin[i];

                    res.bin[i] = aVal | bVal;
                }

                return res;
            }

            /**
             * Bitwise XOR with another <DynamicBitset> instance
             * \param other Other <DynamicBitset> value
             * \return The result of bitwise XOR operation
             */
            [[nodiscard]] DynamicBitset operator^(DynamicBitset other) {
                // See <operator&> for workings

                setLastBit();
                other.setLastBit();
                unsigned long long max = lastSetBit > other.lastSetBit ? lastSetBit + 1 : other.lastSetBit + 1;

                auto res = empty(max * (sizeof(uint32_t) * 8));

                for (unsigned long long i = 0; i < max; i++) {
                    uint32_t aVal, bVal;
                    aVal = bVal = 0;

                    if (i < binLen) aVal = bin[i];
                    if (i < other.binLen) bVal = other.bin[i];

                    res.bin[i] = aVal ^ bVal;
                }

                return res;
            }

            /**
             * Return the bitwise NOT of a <DynamicBitset> value
             * \return Result of bitwise NOT operation
             */
            [[nodiscard]] DynamicBitset operator~() const {
                auto res = DynamicBitset::empty(bits);

                for (unsigned long long i = 0; i < res.binLen; i++) {
                    res.bin[i] = ~bin[i];
                }

                return res;
            }

            /**
             * Compound assignment bitwise AND (See <operator&>)
             * \param other Value
             */
            void operator&=(DynamicBitset other) {
                *this = *this & other;
            }

            /**
             * Compound assignment bitwise OR (See <operator|>)
             * \param other Value
             */
            void operator|=(DynamicBitset other) {
                *this = *this | other;
            }

            /**
             * Compound assignment bitwise XOR (See <operator^>)
             * \param other Value
             */
            void operator^=(DynamicBitset other) {
                *this = *this ^ other;
            }

            /**
             * Left shift bits a certain distance (In this version of the
             * dynamicBitset library the result will expand to accommodate
             * storing all bits => 0101 << 4 == 0101 0000
             * \param dist Distance to shift bits to the left
             * \return Result of left shift bits
             */
            [[nodiscard]] DynamicBitset operator<<(unsigned long long dist) const {
                // Optimised function for shifting one to the left
                if (dist == 1) return _lShift1_();

                // Create empty array
                DynamicBitset res = DynamicBitset::empty(bits + dist);

                // Loop all bits and set the <i + 1>th Bit if <i>th Bit is set
                for (unsigned long long i = 0; i < bits; i++) {
                    if (testBit(i) != 0) res.setBit(i + dist);
                }

                return res;
            }

            /**
             * Right shift bits a certain distance
             * \param dist Distance to shift bits to the right
             * \return Result of right shift bits
             */
            [[nodiscard]] DynamicBitset operator>>(unsigned long long dist) const {
                DynamicBitset res = empty(bits);

                if (dist < bits) {
                    for (long long i = (long long) bits - 1; i >= 0; i--) {
                        if (i - dist >= 0) {
                            if (testBit(i) > 0) {
                                res.setBit(i - dist);
                            }
                        }
                    }
                }

                return res;
            }

            /**
             * Shift bits to the left by some distance
             * See < operator<<() >
             * @param other Distance to shift bits
             */
            void operator<<=(unsigned long long other) {
                *this = *this << other;
            }

            /**
             * Shift bits to the right by some distance
             * See < operator>>() >
             * @param other Distance to shift bits
             */
            void operator>>=(unsigned long long other) {
                *this = *this >> other;
            }

            /**
             * Create string representation of DynamicBitset object
             * @return <char *> array of the object
             */
            [[nodiscard]] explicit operator char *() {
                // Optimisations
                setLastBit();

                // Find the start
                unsigned long long start = utob(lastSetBit);
                // Allocate the memory for the result string
                char *res = (char *) malloc(sizeof(char) * (start + 1));
                unsigned long long index = 0;
                bool insert = false;

                // Loop all values starting from the end
                for (unsigned long long i = start + 1; i > 0; i--) {
                    // Insert the required value
                    if (testBit(i - 1) > 0) {
                        if (!insert) insert = true;
                        res[index++] = '1';
                    } else if (insert) res[index++] = '0';

                }

                // No values were added (i.e. value is 0) so add a zero for clarity
                if (index == 0) res[index++] = '0';
                // Add closing character to end
                res[index] = '\0';

                return res;
            }

            /**
             * Clear an entire DynamicBitset object and set all bits to 0
             */
            void clear() const {
                for (unsigned long long i = 0; i < lastSetBit; i++) {
                    bin[i] = 0;
                }
            }
        };

        /**
         * Stream a DynamicBitset object
         *
         * @param os String Stream reference
         * @param db DynamicBitset instance to stream
         * @return String Stream reference
         */
        std::ostream &operator<<(std::ostream &os, DynamicBitset &db) {
            os << (char *) db;
            return os;
        }

        /**
         * Stream a Bit object
         *
         * @param os String Stream reference
         * @param db Bit instance to stream
         * @return String Stream reference
         */
        DynamicBitset operator<<(std::ostream &os, gpc::bit::Bit &bc) {
            return ((char) bc == '1' ? DynamicBitset(1, 1) : DynamicBitset(1, 0));
        }

        /**
         * To String method for DynamicBitset class
         * @param val DynamicBitset value to convert to string
         * @return <char *> representation of value
         */
        char *to_string(gpc::dynamic_bitset::DynamicBitset val) {
            return (char *) val;
        }

#   undef lluBits
    }
}

#endif //GPC_TEMP_DYNAMICBITSET_HPP

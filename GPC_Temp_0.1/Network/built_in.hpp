//
// Created by penci on 25/05/2020.
//

#ifndef GPC_TEMP_BUILT_IN_HPP
#define GPC_TEMP_BUILT_IN_HPP

#include "../Math/math.hpp"
#include "../Array/array.hpp"
#include <cmath>

namespace gpc {
    namespace network {
        namespace activations {
            namespace imp {
                double alpha = 1;
            }

            void setAlpha(double n) {
                imp::alpha = n;
            }

            double binaryActivation(const double &x) {
                return x > 0 ? 1 : 0;
            }

            double binaryDerivative(const double &x) {
                return 0;
            }

            double linearActivation(const double &x) {
                return x;
            }

            double linearDerivative(const double &x) {
                return 1;
            }

            double sigmoidActivation(const double &x) {
                double ex = pow(gpc::math::E, x);
                return imp::alpha * (ex / (ex + 1));
            }

            double sigmoidDerivative(const double &x) {
                return imp::alpha * (x * (1 - x));
            }

            double tanhActivation(const double &x) {
                return imp::alpha * tanh(x);
            }

            double tanhDerivative(const double &x) {
                return imp::alpha * (1 / pow(cosh(x), 2));
            }

            double reluActivation(const double &x) {
                return imp::alpha * gpc::math::max(x, 0.0);
            }

            double reluDerivative(const double &x) {
                return x > 0 ? imp::alpha : 0;
            }

            double leakyReluActivation(const double &x) {
                if (x > 0) {
                    return x;
                } else {
                    return x * (1.0 / imp::alpha);
                }
            }

            double leakyReluDerivative(const double &x) {
                return x > 0 ? 1 : (1.0 / imp::alpha);
            }

            double eluActivation(const double &x) {
                if (x > 0) {
                    return x;
                } else {
                    return imp::alpha * (pow(gpc::math::E, x) - 1);
                }
            }

            double eluDerivative(const double &x) {
                if (x > 0) {
                    return 1;
                } else {
                    return imp::alpha * pow(gpc::math::E, x);
                }
            }

            double relu6Activation(const double &x) {
                return gpc::math::max(gpc::math::min(x, 6.0), 0.0);
            }

            double relu6Derivative(const double &x) {
                return x < 6 && x > 0 ? 1 : 0;
            }
        }
    }
}

namespace gpc::network::loss {
    gpc::array::Array<double> difference(gpc::array::Array<double> a, gpc::array::Array<double> b) {
        return a - b;
    }

    gpc::array::Array<double> quadratic(gpc::array::Array<double> a, gpc::array::Array<double> b) {
        auto temp = a - b;
        return temp * temp;
    }
}

#endif //GPC_TEMP_BUILT_IN_HPP

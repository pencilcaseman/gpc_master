//
// Created by penci on 25/05/2020.
//

#ifndef GPC_TEMP_NETWORK_HPP
#define GPC_TEMP_NETWORK_HPP

#include <ostream>
#include "../Array/array.hpp"
#include "../List/list.hpp"
#include "layer.hpp"
#include "built_in.hpp"

namespace gpc::network {
    class Network {
        gpc::array::Array<double> *layers;
        unsigned int layerCount = 0;
        gpc::array::Array<double> *bias;

        gpc::list::List<Layer> layersTemp = gpc::list::List<Layer>(0);
        gpc::list::List<double (*)(const double &)> layerActivations = gpc::list::List<double (*)(const double &)>(0);
        gpc::list::List<double (*)(const double &)> layerDerivatives = gpc::list::List<double (*)(const double &)>(0);

        LayerType *layerTypes;

        unsigned int inputCount;
        unsigned int outputCount;

        double learningRate;

        bool initialized = false;

        inline gpc::array::Array<double> _forwardFeed(gpc::array::Array<double> inputValues) {
            for (unsigned int i = 0; i < layerCount - 1; i++) {
                inputValues = layers[i].dot(inputValues);
                inputValues += bias[i];
                inputValues = inputValues.map(layerActivations[i]);
            }

            return inputValues;
        }

        void _backpropagateTrain(gpc::array::Array<double> inputs, gpc::array::Array<double> targets) {
            // The feed forward section of the algorithm
            auto layerResults = (gpc::array::Array<double> *) malloc(sizeof(gpc::array::Array<double>) * layerCount - 2);
            auto inputTemp = inputs.copy();
            for (unsigned long i = 0; i < layerCount; i++) {

            }
        }

    public:
        explicit Network(double lr = 0) {
            layers = (gpc::array::Array<double> *) malloc(sizeof(gpc::array::Array<double>));
            bias = (gpc::array::Array<double> *) malloc(sizeof(gpc::array::Array<double>));
            layerTypes = (LayerType *) malloc(sizeof(LayerType));

            layers[0] = gpc::array::Array<double>(gpc::array::Dimension() << 1 << 1);
            bias[0] = gpc::array::Array<double>(gpc::array::Dimension() << 1 << 1);
            layerTypes[0] = Deep;

            inputCount = 0;
            outputCount = 0;

            learningRate = lr == 0 ? 0.01 : lr;
        }

        ~Network() {
        	free(layers);
        	free(bias);
        	free(layerTypes);
        }

        void addLayer(Layer layer) {
            layersTemp.append(layer);

            if (layerCount > 0) {
                layerActivations.append(layer.getActivation());
                layerDerivatives.append(layer.getDerivative());
            }
            std::cout << layerActivations.length() << "\n";
            layerCount++;
        }

        void construct() {
            layers = (gpc::array::Array<double> *) malloc(sizeof(gpc::array::Array<double>) * layerCount);
            bias = (gpc::array::Array<double> *) malloc(sizeof(gpc::array::Array<double>) * layerCount);
            layerTypes = (LayerType *) malloc(sizeof(LayerType) * layerCount);
            inputCount = layersTemp[0].length();
            outputCount = layersTemp[layerCount - 1].length();

            for (unsigned int i = 1; i < layerCount; i++) {
                layers[i - 1] = gpc::array::Array<double>(
                        gpc::array::Dimension() << layersTemp[i].length() << layersTemp[i - 1].length());
                bias[i - 1] = gpc::array::Array<double>(
                        gpc::array::Dimension() << layersTemp[i].length() << 1);
                layerTypes[i - 1] = layersTemp[i - 1].type();
            }

            layerCount--;

            initialized = true;
        }

        inline void randomize(double min = -1, double max = 1) {
            for (unsigned int i = 0; i < layerCount - 1; i++) {
                layers[i].fillRandom(min, max);
                bias[i].fillRandom(min, max);
            }
        }

        inline gpc::array::Array<double> forwardFeed(gpc::list::List<double> data) {
            return forwardFeed(gpc::array::Array<double>::fromList(data).transpose());
        }

        inline gpc::array::Array<double> forwardFeed(double *data, unsigned long len) {
            return forwardFeed(gpc::array::Array<double>::fromArray(data, len).transpose());
        }

        inline gpc::array::Array<double> forwardFeed(gpc::array::Array<double> data) {
            gpc::array::Array<double> inputValues;
            if (data.shape()[1] == inputCount) { inputValues = data.transpose(); }
            else if (data.shape()[0] == inputCount) { inputValues = data.copy(); }
            else { gpc::warning::raiseError("Shape Error", "Invalid input array for network forward feed"); }

            return _forwardFeed(inputValues);
        }

        inline void backpropagateTrain(gpc::list::List<double> data1, gpc::list::List<double> data2) {
            backpropagateTrain(gpc::array::Array<double>::fromList(data1).transpose(), gpc::array::Array<double>::fromList(data2).transpose());
        }

        inline void backpropagateTrain(double *data1, double *data2, unsigned long len1, unsigned long len2) {
            backpropagateTrain(gpc::array::Array<double>::fromArray(data1, len1).transpose(), gpc::array::Array<double>::fromArray(data2, len2).transpose());
        }

        inline void backpropagateTrain(gpc::array::Array<double> data1, gpc::array::Array<double> data2) {
            gpc::array::Array<double> inputValues;
            gpc::array::Array<double> targetValues;

            if (data1.shape()[1] == inputCount) { inputValues = data1.transpose(); }
            else if (data1.shape()[0] == inputCount) { inputValues = data1.copy(); }
            else { gpc::warning::raiseError("Shape Error", "Invalid input array for network forward feed"); }

            if (data2.shape()[1] == outputCount) { targetValues = data2.transpose(); }
            else if (data2.shape()[0] == outputCount) { targetValues = data2.copy(); }
            else { gpc::warning::raiseError("Shape Error", "Invalid input array for network forward feed"); }

            //std::cout << inputValues << "\n" << targetValues << "\n\n";

            _backpropagateTrain(inputValues, targetValues);
        }

        std::string toString() {
            std::string res;

            for (unsigned long i = 0; i < layerCount - 1; i++) {
                res += layers[i].toString();
                res += "\n\n";
            }

            return res;
        }
    };
}

std::ostream &operator<<(std::ostream &os, gpc::network::Network val) {
    return os << val.toString();
}

#endif //GPC_TEMP_NETWORK_HPP

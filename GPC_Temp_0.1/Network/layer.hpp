//
// Created by penci on 25/05/2020.
//

#ifndef GPC_TEMP_LAYER_HPP
#define GPC_TEMP_LAYER_HPP

#include "../Array/array.hpp"
#include "built_in.hpp"

namespace gpc {
    namespace network {
        /**
         * Enumeration for different Layer types
         * For example:
         *      1. Deep (fully connected)
         *      2. Convolutional
         *      3. Sparse
         */
        enum LayerType {
            Deep = 1, Convolutional = 2, Sparse = 3
        };

        /**
         * A single layer for a segment in a Network object
         */
        class Layer {
            // Length of the layer
            unsigned long long layerSize;

            // The type for the layer
            LayerType layerType;

            double (*activation)(const double &);

            double (*derivative)(const double &);

        public:
            Layer() {
                layerSize = 1;
                layerType = Deep;
                activation = activations::sigmoidActivation;
                derivative = activations::sigmoidDerivative;
            }

            Layer(unsigned long long size, LayerType type,
                  double (*activationFunction)(const double &) = &activations::sigmoidActivation,
                  double (*activationDerivative)(const double &) = &activations::sigmoidDerivative) {
                layerSize = size;
                layerType = type;
                activation = activationFunction;
                derivative = activationDerivative;
            }

            int setActivation(double (*_activation)(const double &), double (*_derivative)(const double &)) {
                activation = _activation;
                derivative = _derivative;
                return 0;
            }

            [[nodiscard]] unsigned long long length() const {
                return layerSize;
            }

            [[nodiscard]] LayerType type() {
                return layerType;
            }

            auto getActivation() {
                return activation;
            }

            auto getDerivative() {
                return derivative;
            }
        };
    }
}

#endif //GPC_TEMP_LAYER_HPP

//
// Created by penci on 19/04/2020.
//

#ifndef GPC_TEMP_BASIC_HPP
#define GPC_TEMP_BASIC_HPP

#include <iostream>
#include <string>
#include <algorithm>
#include <chrono>
#include <iomanip>
#include <thread>

namespace gpc {
    namespace basic {
        /**
         * Remove all characters of a certain type from the left
         * side of a string
         *
         * @param s String to remove characters from
         * @param w Character to remove
         * @return Result string
         */
        std::string ltrim(const std::string &s, const std::string &w) {
            size_t start = s.find_first_not_of(w);
            return (start == std::string::npos) ? "" : s.substr(start);
        }

        /**
         * Remove all characters of a certain type from the right
         * side of a string
         *
         * @param s String to remove characters from
         * @param w Character to remove
         * @return Result string
         */
        std::string rtrim(const std::string &s, const std::string &w) {
            size_t end = s.find_last_not_of(w);
            return (end == std::string::npos) ? "" : s.substr(0, end + 1);
        }

        /**
         * Remove all characters of a certain type from the left
         * and right sides of a string
         *
         * @param s String to remove characters from
         * @param w Character to remove
         * @return Result string
         */
        std::string trim(const std::string &s, const std::string &w) {
            return rtrim(ltrim(s, w), w);
        }

        /**
         * Return the current time since the epoch in nanoseconds
         * @return Nanoseconds since the epoch
         */
        unsigned long long time() {
            return std::chrono::system_clock::now().time_since_epoch().count();
        }

        /**
         * Class to get the default value for any type
         */
        class default_t {
        public:
            template<typename T>
            operator T() const { return T(); }
        };

        /**
         * Allows you to set any variable to its default
         *
         * Example:
         *      int x = defaultType;
         */
        default_t const defaultType = default_t();

        /**
         * Set the decimal precision for std cout
         * @param n Number of decimal places to print values to
         */
        void setCoutPrec(unsigned long long n) {
            std::cout << std::fixed;
            std::cout << std::setprecision(n);
        }

        unsigned long long threadCount() {
            return std::thread::hardware_concurrency();
        }
    }
}

#endif //GPC_TEMP_BASIC_HPP

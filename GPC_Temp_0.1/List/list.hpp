//
// Created by penci on 25/04/2020.
//

#ifndef GPC_TEMP_LIST_HPP
#define GPC_TEMP_LIST_HPP

#include "../Math/math.hpp"
#include "../Warnings/raise.hpp"
#include "../Basic/basic.hpp"
#include "../Conversions/types.hpp"
#include "../Conversions/type_name.hpp"
#include <cstring>

namespace gpc {
    namespace list {
        /**
         * GPC List type
         *
         * Used to contain a variable number of values of any type
         *
         * @tparam listType The datatype of the List
         */
        template<typename listType>
        class List {
        	bool isValid_ = true;

        private:
            // Length of the list
            unsigned long long listLength;
            // Array slots allocated (allocate in multiples of 16)
            unsigned long long allocated;
            listType *data;

            /*
             * 0: +
             * 1: -
             * 2: *
             * 3: /
             * 4: %
             */
            /**
             * A function to do an operation on two lists of type <listType>
             *
             * The <op> parameter is an integer from 0 to 4, where each value
             * in that range represents a corresponding operation:
             *      0: +
             *      1: -
             *      2: *
             *      3: /
             *      4: %
             *
             * @param a First list
             * @param b Second list
             * @param op Operation
             * @return Resulting list
             */
            List<listType> math(List<listType> a, List<listType> b, int op) {
                if (a.listLength != b.listLength) {
                    gpc::warning::raiseError("Operator Error", "Cannot add two lists of different sizes");
                } else {
                    auto res = a.copy();
                    for (unsigned long long i = 0; i < listLength; i++) {
                        if (op == 0) {
                            res[i] = a[i] + b[i];
                        } else if (op == 1) {
                            res[i] = a[i] - b[i];
                        } else if (op == 2) {
                            res[i] = a[i] * b[i];
                        } else if (op == 3) {
                            res[i] = a[i] / b[i];
                        } else if (op == 4) {
                            res[i] = a[i] % b[i];
                        }
                    }

                    return res;
                }
            }

        public:
            /**
             * Create a list given a number of elements and a pointer to an array of those elements
             * @param initialLength Number of values to create the list with
             * @param _data The data to fill the list with
             */
            List(unsigned long long initialLength, listType *_data) {
                listLength = initialLength;
                allocated = gpc::math::roundUp(initialLength, (unsigned long long) 16);
                data = (listType *) malloc(sizeof(listType) * allocated);
                for (unsigned long long i = 0; i < allocated; i++) {
                    if (i < initialLength) {
                        data[i] = _data[i];
                    } else {
                        data[i] = gpc::basic::defaultType;
                    }
                }
            }


            ~List() {
            	std::cout << "Freeing the data...\n";
            	delete data;
            }

            void forceOverrideValidity(bool valid) {
            	isValid_ = valid;
            }

            bool isValid() {
            	return isValid_;
            }

            /**
             * Create a list given an initial length to allocate
             *
             * The size of the list can be changed, though it is faster
             * if the size is predefined and does not change
             *
             * @param initialLength Number of elements to allocate memory for
             */
            List(unsigned long long initialLength = 0) {
                listLength = initialLength;
                allocated = gpc::math::roundUp(initialLength, (unsigned long long) 16);
                data = (listType *) malloc(sizeof(listType) * allocated);
                for (unsigned long long i = 0; i < allocated; i++) {
                    data[i] = listType();
                }
            }

            /**
             * Change the length of a list
             *
             * If the new size is greater than the existing size, the
             * uninitialized values will be filled with the default
             * value for the list type, however if the new size is
             * smaller than the original size, the values will be removed
             * from the list
             *
             * @param n The number of elements to resize the list to
             */
            void resize(unsigned long long n) {
                // listLength = n;
                allocated = gpc::math::roundUp(n, (unsigned long long) 16);

                if (n > listLength) {
                    data = (listType *) realloc(data, sizeof(listType) * allocated);
                } else {
                    auto tempData = (listType *) malloc(sizeof(listType) * allocated);
                    for (unsigned long long i = 0; i < n; i++) {
                        tempData[i] = data[i];
                    }
                }
            }

            /**
             * Create an exact copy of a list
             * @return A list object with the exact same data as the array being copied
             */
            List<listType> copy() {
                auto res = List<listType>(listLength);
                memcpy((void *) res.data, data, sizeof(listType) * allocated);
                return res;
            }

            /**
             * Return the result of adding a list to another list element-wise
             *
             * For example:
             *      [1, 2, 3] + [4, 5, 6] = [5, 7, 9]
             *
             * @param other The other list to add
             * @return The result of element-wise addition of the two lists
             */
            List<listType> operator+(List<listType> other) {
                return math(*this, other, 0);
            }

            /**
             * Return the result of subtracting a list from another list element-wise
             *
             * For example:
             *       [4, 5, 6] - [1, 2, 3] = [3, 3, 3]
             *
             * @param other The other list to subtract with
             * @return The result of element-wise subtraction of the two lists
             */
            List<listType> operator-(List<listType> other) {
                return math(*this, other, 1);
            }

            /**
             * Return the result of multiplying a list by another list element-wise
             *
             * For example:
             *       [1, 2, 3] * [4, 5, 6] = [4, 10, 18]
             *
             * @param other The other list to multiply by
             * @return The result of element-wise subtraction of the two lists
             */
            List<listType> operator*(List<listType> other) {
                return math(*this, other, 2);
            }

            /**
             * Return the result of dividing a list by another list element-wise
             *
             * For example:
             *       [8, 12, 18] / [2, 4, 6] = [4, 3, 3]
             *
             * @param other The other list to divide by
             * @return The result of element-wise division of the two lists
             */
            List<listType> operator/(List<listType> other) {
                return math(*this, other, 3);
            }

            /**
            * Return the result of applying the modulus operator on
            * a list by another list element-wise
            *
            * For example:
            *       [2, 4, 6] % [1, 3, 4] = [0, 1, 2]
            *
            * @param other The other list to use as the lhs of the modulus operator
            * @return The result of the element-wise modulus operation of the two lists
            */
            List<listType> operator%(List<listType> other) {
                return math(*this, other, 4);
            }

            /**
             * Return the value at a given index in a list
             *
             * Entering a negative value will start the indexing
             * from the end of the list
             *
             * For Example:
             *      x = [1, 2, 3]
             *      x[1] == 2
             *      x[-1] == 3
             *
             * @param index The index of the list to access
             * @return The value of the list at that index
             */
            listType &operator[](unsigned long long index) {
                // Work backwards or forwards
                if (index < listLength) {
                    return data[index];
                } else if (-listLength <= index) {
                    return data[index - (-listLength)];
                } else {
                    gpc::warning::raiseError("List Index Out of Range",
                                             "Unable to access element " + std::to_string(index) +
                                             " in list of length " +
                                             std::to_string(listLength));
                    exit(1);
                }
            }

            /**
             * Test if one list is identical to another
             *
             * CAUTION: This is a slow function that needs to be
             *          improved -- if speed is important I advise
             *          that you create a better function or remove
             *          checks all together if possible
             *
             * @param other The other list to check equality with
             * @return True or false as to whether the lists are identical
             */
            bool operator==(List<listType> other) {
                if (listLength == other.listLength) {
                    for (unsigned long long i = 0; i < listLength; i++) {
                        if (data[i] != other.data[i]) {
                            return false;
                        }
                    }

                    return true;
                } else {
                    return false;
                }
            }

            /**
             * See if two lists are not equal
             *
             * See < operator==() >
             *
             * @param other The other list to check inequality with
             * @return True or false depending on whether the lists are equal
             */
            bool operator!=(List<listType> other) {
                return !(operator==(other));
            }

            /**
             * Return a new list from a given point in an existing one
             *
             * For example:
             *      [1, 2, 3, 4].from(2) == [3, 4]
             *
             * @param n List index to start copying from
             * @return A list of values representing a section of a list
             */
            List<listType> from(unsigned long long n) {
                if (n >= listLength) {
                    gpc::warning::raiseError("List Index Out of Range",
                                             "Unable to access element " + std::to_string(n) + " in list of length " +
                                             std::to_string(listLength));
                }

                auto res = List<listType>(listLength - n);
                for (unsigned long long i = n; i < listLength; i++) {
                    res[i - n] = data[i];
                }
                return res;
            }

            /**
             * Add a value on to the end of an existing list, altering
             * its size if necessary to accommodate the added value
             *
             * CAUTION: The appended value must be of the same type as
             *          the parent list
             */
            void append(listType value) {
                if (listLength < allocated) {
                    data[listLength++] = value;
                } else {
                    resize(listLength + 1);
                    data[listLength++] = value;
                }
            }

            /**
             * Get the length of a list object
             * @return The length of the list
             */
            unsigned long long length() {
                return listLength;
            }

            /**
             * Convert a list into a string, including commas and
             * brackets where necessary
             *
             * @return Std string representation of value
             */
            std::string toString() {
                std::string res = "[";

                for (unsigned long long i = 0; i < listLength; i++) {
                    res += gpc::conversion::type::toString(data[i]);

                    if (i + 1 < listLength) {
                        res += ", ";
                    }
                }

                res += "]";
                return res;
            }

            /**
             * Convert list to < char * >
             *
             * See < toString() >
             *
             * @return < char * > representation of list
             */
            explicit operator char *() {
                return &toString()[0];
            }
        };

        /**
         * Stream a list
         *
         * For example:
         *      std::cout << x << std::endl;
         *      // Where x is a list object
         *
         * @tparam t List type for the list
         * @param os Ostream reference
         * @param val The list object to stream
         * @return Ostream reference
         */
        template<typename t>
        std::ostream &operator<<(std::ostream &os, List<t> val) {
            return os << val.toString();
        }
    }
}

#endif //GPC_TEMP_LIST_HPP

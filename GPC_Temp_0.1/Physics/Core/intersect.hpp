//
// Created by penci on 08/06/2020.
//

#ifndef GPC_TEMP_INTERSECT_HPP
#define GPC_TEMP_INTERSECT_HPP

// Ported from js-intersect at https://github.com/vrd/js-intersect/blob/gh-pages/solution.js

#include "../../Vector/vector.hpp"
#include "../../List/list.hpp"
#include <cmath>

namespace gpc {
	namespace physics {
		typedef struct {
		public:
			double x = 0, y = 0, t = -1;
		} Point;

		typedef struct {
			Point p1;
			Point p2;
		} Edge;

		typedef struct {
			std::string loc;
			double t = 0;
			double theta = 0;
		} Classification;

		typedef struct {
			double x = 0, y = 0, theta = -1;
			unsigned int edge = 0;
		} PolygonThing;

		typedef struct {
			double min = 0, max = 0;
		} MinMax;

		typedef struct {
			MinMax x;
			MinMax y;
		} Size;

		typedef struct {
			Point p1;
			Point p2;
		} Line;

		Classification classifyPoint(Point p, Edge edge);

		gpc::list::List<Point> findEdgeIntersection(Edge edge1, Edge edge2);

		gpc::list::List<Point> sortPoints(gpc::list::List<Point> points);

		gpc::list::List<Edge> getEdges(gpc::list::List<Point> fig);

		bool pointExists(Point p, gpc::list::List<Point> points);

		bool equalEdges(Edge edge1, Edge edge2);

		gpc::list::List<Point> getMidpoints(gpc::list::List<Edge> edges) {
			auto midpoints = gpc::list::List<Point>();
			double x, y;
			for (unsigned int i = 0; i < edges.length(); i++) {
				x = (edges[i].p1.x + edges[i].p2.x) / 2;
				y = (edges[i].p1.y + edges[i].p2.y) / 2;
				midpoints.append({x, y});
			}
			return midpoints;
		}

		bool findPointInsidePolygon(Point point, gpc::list::List<Point> polygon) {
			int cross = 0;
			auto edges = getEdges(polygon);
			Classification classify;
			Point org, dest;
			for (unsigned int i = 0; i < edges.length(); i++) {
				org = edges[i].p1;
				dest = edges[i].p2;

				classify = classifyPoint(point, {org, dest});
				if ((
							(classify.loc == "RIGHT") &&
							(org.y < point.y) &&
							(dest.y >= point.y)
					) ||
					(
							(classify.loc == "LEFT") &&
							(org.y >= point.y) &&
							(dest.y < point.y)
					)
						) {
					cross++;
				}
				if (classify.loc == "BETWEEN") return false;
			}
			return cross % 2 != 0;
		}

		Size getSize(gpc::list::List<Point> polygon) {
			Size size = {{
								 polygon[0].x,
								 polygon[0].x
						 },
						 {
								 polygon[0].y,
								 polygon[0].y
						 }
			};
			for (unsigned int i = 1; i < polygon.length(); i++) {
				if (polygon[i].x < size.x.min) size.x.min = polygon[i].x;
				if (polygon[i].x > size.x.max) size.x.max = polygon[i].x;
				if (polygon[i].y < size.y.min) size.y.min = polygon[i].y;
				if (polygon[i].y > size.y.max) size.y.max = polygon[i].y;
			}
			return size;
		}

		Point getPointInsidePolygon(gpc::list::List<Point> polygon) {
			Point point;
			Size size = getSize(polygon);
			gpc::list::List<Edge> edges = getEdges(polygon);
			#ifdef PI
			double y = size.y.min + (size.y.max - size.y.min) / PI;
			#else
			double y = size.y.min + (size.y.max - size.y.min) / math::PI;
			#endif
			double dy = (size.y.max - size.y.min) / 13;
			Edge line;
			gpc::list::List<Point> points;
			auto interPoints = gpc::list::List<Point>();
			bool pointsOK = false;
			while (!pointsOK) {
				line = {{size.x.min - 1, y},
						{size.x.max + 1, y}};
				//find intersections with all polygon edges
				for (unsigned int i = 0; i < edges.length(); i++) {
					points = findEdgeIntersection(line, edges[i]);
					//if edge doesn't lie inside line
					if (points.isValid() && points.length() == 1) {
						interPoints.append(points[0]);
					}
				}
				interPoints = sortPoints(interPoints);
				//find two correct interpoints
				for (unsigned int i = 0; i < interPoints.length() - 1; i++) {
					if (interPoints[i].t != interPoints[i + 1].t) {
						//enable exit from loop and calculate point coordinates
						pointsOK = true;
						point = {((interPoints[i].x + interPoints[i + 1].x) / 2), y};
					}
				}
				//all points are incorrect, need to change line parameters
				y = y + dy;
				if (((y > size.y.max) || (y < size.y.min)) && !pointsOK) {
					pointsOK = true;
				}
			}
			return point;
		}

		double polygonArea(gpc::list::List<Point> p) {
			auto len = p.length();
			double s = 0;
			for (unsigned int i = 0; i < len; i++) {
				s += fabs((p[i % len].x * p[(i + 1) % len].y) - (p[i % len].y *
																 p[(i + 1) % len].x));
			}
			return s / 2;
		}

		gpc::list::List<gpc::list::List<Point>>
		removeSmallPolygons(gpc::list::List<gpc::list::List<Point>> polygons, double minSize) {
			auto big = gpc::list::List<gpc::list::List<Point>>();
			for (unsigned int i = 0; i < polygons.length(); i++) {
				if (polygonArea(polygons[i]) >= minSize) {
					big.append(polygons[i]);
				}
			}
			return big;
		}

		gpc::list::List<gpc::list::List<Point>>
		filterPolygons(gpc::list::List<gpc::list::List<Point>> polygons, gpc::list::List<Point> fig1,
					   gpc::list::List<Point> fig2, const std::string &mode) {
			auto filtered = gpc::list::List<gpc::list::List<Point>>();
			bool c1, c2;
			Point point;
			auto bigPolygons = removeSmallPolygons(polygons, 0.0001);

			for (unsigned int i = 0; i < bigPolygons.length(); i++) {
				point = getPointInsidePolygon(bigPolygons[i]);
				c1 = findPointInsidePolygon(point, fig1);
				c2 = findPointInsidePolygon(point, fig2);
				if (
						((mode == "intersect") && c1 && c2) || //intersection
						((mode == "cut1") && c1 && !c2) ||     //fig1 - fig2
						((mode == "cut2") && !c1 && c2) ||     //fig2 - fig2
						((mode == "sum") && (c1 || c2))) {     //fig1 + fig2
					filtered.append(bigPolygons[i]);
				}
			}

			return filtered;
		}

		bool polygonExists(gpc::list::List<Point> polygon, gpc::list::List<gpc::list::List<Point>> polygons) {
			//if array is empty element doesn't exist in it
			if (polygons.length() == 0) return false;
			//check every polygon in array
			for (unsigned int i = 0; i < polygons.length(); i++) {
				//if lengths are not same go to next element
				if (polygon.length() != polygons[i].length()) continue;
					//if length are same need to check
				else {
					//if all the points are same
					for (unsigned int j = 0; j < polygon.length(); j++) {
						//if point is not found break forloop and go to next element
						if (!pointExists(polygon[j], polygons[i])) break;
							//if point found
						else {
							//and it is last point in polygon we found polygon in array!
							if (j == polygon.length() - 1) return true;
						}
					}
				}
			}
			return false;
		}

		gpc::list::List<gpc::list::List<Point>> polygonate(gpc::list::List<Edge> edges) {
			auto polygons = gpc::list::List<gpc::list::List<Point>>();

			auto polygon = gpc::list::List<Point>();
			bool polygonDefined = true;

			auto len = edges.length();
			auto midpoints = getMidpoints(edges);

			//start from every edge and create non-self-intersecting polygons
			for (unsigned long i = 0; i < len - 2; i++) {
				Point org = {edges[i].p1.x, edges[i].p1.y};
				Point dest = {edges[i].p2.x, edges[i].p2.y};
				unsigned int currentEdge = i;

				PolygonThing point;
				bool pointDefined = false;

				Point p;
				bool pDefined = false;

				unsigned int direction;
				bool stop;
				//while we haven't come to the starting edge again
				for (direction = 0; direction < 2; direction++) {
					polygon = gpc::list::List<Point>();
					stop = false;
					while ((polygon.length() == 0) || (!stop)) {
						//add point to polygon
						polygon.append({org.x, org.y});
						polygonDefined = true;
						pointDefined = false;
						//look for edge connected with end of current edge
						for (unsigned int j = 0; j < len; j++) {
							pDefined = false;
							//except itself
							if (!equalEdges(edges[j], edges[currentEdge])) {
								//if some edge is connected to current edge in one endpoint
								if ((edges[j].p1.x == dest.x) && (edges[j].p1.y == dest.y)) {
									p = edges[j].p2;
									pDefined = true;
								}

								if ((edges[j].p2.x == dest.x) && (edges[j].p2.y == dest.y)) {
									p = edges[j].p1;
									pDefined = true;
								}
								//compare it with last found connected edge for minimum angle between itself and current edge
								if (pDefined) {
									auto classify = classifyPoint(p, {org, dest});
									//if this edge has smaller theta then last found edge update data of next edge of polygon
									if (!pointDefined ||
										((classify.theta < point.theta) && (direction == 0)) ||
										((classify.theta > point.theta) && (direction == 1))) {
										point = {p.x, p.y, classify.theta, j};
										pointDefined = true;
									}
								}
							}
						}
						//change current edge to next edge
						org.x = dest.x;
						org.y = dest.y;
						dest.x = point.x;
						dest.y = point.y;
						currentEdge = point.edge;
						//if we reach start edge

						if ((org.x == edges[i].p1.x) &&
							(org.y == edges[i].p1.y) &&
							(dest.x == edges[i].p2.x) &&
							(dest.y == edges[i].p2.y)) {
							stop = true;
							//check polygon for correctness
							/*for (var k = 0; k < allPoints.length; k++) {
							  //if some point is inside polygon it is incorrect
							  if ((!pointExists(allPoints[k], polygon)) && (findPointInsidePolygon(allPoints[k], polygon))) {
								polygon = false;
							  }
							}*/
							for (unsigned int k = 0; k < midpoints.length(); k++) {
								//if some midpoint is inside polygon (edge inside polygon) it is incorrect
								if (findPointInsidePolygon(midpoints[k], polygon)) {
									polygonDefined = false;
								}
							}
						}
					}

					//add created polygon if it is correct and was not found before
					if (polygonDefined && !polygonExists(polygon, polygons)) {
						polygons.append(polygon);
					}
				}
			}
			//console.log("polygonate: " + JSON.stringify(polygons));
			return polygons;
		}

		bool equalEdges(Edge edge1, Edge edge2) {
			return ((edge1.p1.x == edge2.p1.x) &&
					(edge1.p1.y == edge2.p1.y) &&
					(edge1.p2.x == edge2.p2.x) &&
					(edge1.p2.y == edge2.p2.y)) || (
						   (edge1.p1.x == edge2.p2.x) &&
						   (edge1.p1.y == edge2.p2.y) &&
						   (edge1.p2.x == edge2.p1.x) &&
						   (edge1.p2.y == edge2.p1.y));
		}

		bool pointExists(Point p, gpc::list::List<Point> points) {
			if (points.length() == 0) { return false; }

			for (unsigned int i = 0; i < points.length(); i++) {
				if ((p.x == points[i].x) && (p.y == points[i].y)) { return true; }
			}
			return false;
		}

		bool edgeExists(Edge e, gpc::list::List<Edge> edges) {
			if (edges.length() == 0) { return false; }
			for (unsigned int i = 0; i < edges.length(); i++) {
				if (equalEdges(e, edges[i])) return true;
			}
			return false;
		}

		double polarAngle(Edge edge) {
			auto dx = edge.p2.x - edge.p1.x;
			auto dy = edge.p2.y - edge.p1.y;
			if ((dx == 0) && (dy == 0)) { return false; }
			if (dx == 0) { return ((dy > 0) ? 90 : 270); }
			if (dy == 0) { return ((dx > 0) ? 0 : 180); }
			#ifdef PI
			auto theta = atan(dy / dx) * 360 / (2 * PI);
			#else
			auto theta = atan(dy / dx) * 360 / (2 * math::PI);
			#endif
			if (dx > 0) return ((dy >= 0) ? theta : theta + 360);
			else return (theta + 180);
		}

		Classification classifyPoint(Point p, Edge edge) {
			auto ax = edge.p2.x - edge.p1.x;
			auto ay = edge.p2.y - edge.p1.y;
			auto bx = p.x - edge.p1.x;
			auto by = p.y - edge.p1.y;
			auto sa = ax * by - bx * ay;

			if ((p.x == edge.p1.x) && (p.y == edge.p1.y)) {
				return Classification{"ORIGIN", 0, 0};
			}

			if ((p.x == edge.p2.x) && (p.y == edge.p2.y)) {
				return Classification{"DESTINATION", 1, 0};
			}

			auto theta = fmod((polarAngle({edge.p2, edge.p1}) - polarAngle({{edge.p2.x, edge.p2.y},
																			{p.x,       p.y}})), 360);

			if (theta < 0) { theta += 360; }
			if (sa < -0.0000000001) { return Classification{"LEFT", 0, theta}; }
			if (sa > 0.0000000001) { return Classification{"RIGHT", 0, theta}; }
			if (((ax * bx) < 0) || ((ay * by) < 0)) { return Classification{"BEHIND", 0, 0}; }
			if ((sqrt(ax * ax + ay * ay)) < (sqrt(bx * bx + by * by))) { return Classification{"BEYOND", 0, 180}; }

			double t;
			if (ax != 0) t = bx / ax;
			else t = by / ay;

			return Classification{"BETWEEN", t, 0};
		}

		gpc::list::List<Point> findEdgeIntersection(Edge edge1, Edge edge2) {
			auto x1 = edge1.p1.x;
			auto x2 = edge1.p2.x;
			auto x3 = edge2.p1.x;
			auto x4 = edge2.p2.x;
			auto y1 = edge1.p1.y;
			auto y2 = edge1.p2.y;
			auto y3 = edge2.p1.y;
			auto y4 = edge2.p2.y;
			auto nom1 = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3);
			auto nom2 = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3);
			auto denom = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
			auto t1 = nom1 / denom;
			auto t2 = nom2 / denom;
			auto interPoints = gpc::list::List<Point>(0);

			// 1. Lines are parallel or edges don't intersect
			if (((denom == 0) && (nom1 != 0)) || (t1 <= 0) || (t1 >= 1) || (t2 < 0) || (t2 > 1)) { return interPoints; }

				// 2. Lines are collinear
			else if ((nom1 == 0) && (denom == 0)) {
				for (unsigned int i = 0; i < 2; i++) {
					auto classify = classifyPoint(i == 0 ? edge2.p1 : edge2.p2, edge1);
					// Find position of this endpoints relatively to edge1
					if (classify.loc == "ORIGIN" || classify.loc == "DESTINATION") {
						interPoints.append(
								{i == 0 ? edge2.p1.x : edge2.p2.x, i == 0 ? edge2.p1.y : edge2.p2.y, classify.t});
					} else if (classify.loc == "BETWEEN") {
						auto x = (x1 + classify.t * (x2 - x1));
						auto y = (y1 + classify.t * (y2 - y1));
						interPoints.append({x, y, classify.t});
					}
				}

				return interPoints;
			}

				// 3. Edges intersect
			else {
				for (unsigned int i = 0; i < 2; i++) {
					auto classify = classifyPoint(i == 0 ? edge2.p1 : edge2.p2, edge1);
					if (classify.loc == "ORIGIN" || classify.loc == "DESTINATION") {
						interPoints.append(
								{i == 0 ? edge2.p1.x : edge2.p2.x, i == 0 ? edge2.p1.y : edge2.p2.y, classify.t});
					}
				}

				if (interPoints.length() > 0) { return interPoints; }

				auto x = x1 + t1 * (x2 - x1);
				auto y = y1 + t1 * (y2 - y1);
				interPoints.append({x, y, t1});
				return interPoints;
			}

			return interPoints;
		}

		gpc::list::List<Edge> getEdges(gpc::list::List<Point> fig) {
			auto len = fig.length();
			auto edges = gpc::list::List<Edge>(len);
			for (unsigned int i = 0; i < len; i++) {
				edges[i] = Edge{{fig[(i % len)].x,       fig[(i % len)].y},
								{fig[((i + 1) % len)].x, fig[((i + 1) % len)].y}};
			}

			return edges;
		}

		gpc::list::List<Point> sortPoints(gpc::list::List<Point> points) {
			// Bubble sort -- FIX THIS
			auto p = points.copy();

			unsigned int swaps = 99999999;
			while (swaps != 0) {
				swaps = 0;
				for (unsigned int i = 0; i < p.length() - 1; i++) {
					if (p[i].t > p[i + 1].t) {
						auto temp = p[i + 1];
						p[i + 1] = p[i];
						p[i] = temp;

						swaps++;
					}
				}
			}

			return p;
		}

		void addNewPoints(gpc::list::List<Point> *newPoints, gpc::list::List<Point> *points) {
			if (newPoints->length() > 0) {
				for (unsigned int k = 0; k < newPoints->length(); k++) {
					if (!pointExists(newPoints->operator[](k), *points)) {
						points->append(newPoints->operator[](k));
					}
				}
			}
		}

		gpc::list::List<Edge> edgify(gpc::list::List<Point> fig1, gpc::list::List<Point> fig2) {
			auto fig1Edges = getEdges(fig1);
			auto fig2Edges = getEdges(fig2);
			auto primEdges = gpc::list::List<Edge>(fig1Edges.length() + fig2Edges.length());

			for (unsigned int i = 0; i < fig1Edges.length(); i++) {
				primEdges[i] = fig1Edges[i];
			}

			for (unsigned int i = 0; i < fig2Edges.length(); i++) {
				primEdges[i + fig1Edges.length()] = fig2Edges[i];
			}

			auto secEdges = gpc::list::List<Edge>(0);

			for (unsigned int i = 0; i < primEdges.length(); i++) {
				auto points = gpc::list::List<Point>(0);
				for (unsigned int j = 0; j < primEdges.length(); j++) {
					if (i != j) {
						auto interPoints = findEdgeIntersection(primEdges[i], primEdges[j]);
						addNewPoints(&interPoints, &points);
					}
				}

				auto startPoint = primEdges[i].p1;

				startPoint.t = 0;
				auto endPoint = primEdges[i].p2;

				endPoint.t = 1;

				auto tempPoints = gpc::list::List<Point>(2);
				tempPoints[0] = startPoint;
				tempPoints[1] = endPoint;
				addNewPoints(&tempPoints, &points);

				points = sortPoints(points);

				for (unsigned int k = 0; k < points.length() - 1; k++) {
					auto edge = Edge{{points[k].x,     points[k].y},
									 {points[k + 1].x, points[k + 1].y}};

					if (!edgeExists(edge, secEdges)) {
						secEdges.append(edge);
					}
				}
			}

			return secEdges;
		}

		bool checkPolygons(gpc::list::List<Point> fig1, gpc::list::List<Point> fig2) {
			if (fig1.length() < 3) { return false; }
			return fig2.length() >= 3;
		}

		double distance(Point p1, Point p2) {
			auto dx = fabs(p1.x - p2.x);
			auto dy = fabs(p1.y - p2.y);
			return sqrt(dx * dx + dy * dy);
		}

		gpc::list::List<Point>
		alignPolygon(gpc::list::List<Point> polygon, gpc::list::List<Point> points) {
			for (unsigned int i = 0; i < polygon.length(); i++) {
				for (unsigned int j = 0; j < points.length(); j++) {
					if (distance(polygon[i], points[j]) < 0.00000001) {
						polygon[i] = points[j];
					}
				}
			}

			return polygon;
		}

		gpc::list::List<gpc::list::List<Point>>
		intersect(gpc::list::List<Point> fig1, gpc::list::List<Point> fig2) {
			auto fig2a = alignPolygon(fig2, fig1);
			if (!checkPolygons(fig1, fig2a)) {
				return {0};
			}

			auto edges = edgify(fig1, fig2a);
			auto polygons = polygonate(edges);
			auto filteredPolygons = filterPolygons(polygons, fig1, fig2a, "intersect");

			return filteredPolygons;
		}

		gpc::list::List<gpc::list::List<gpc::vector::Vec2>>
		intersect(gpc::list::List<gpc::vector::Vec2> fig1, gpc::list::List<gpc::vector::Vec2> fig2) {
			auto fig1Points = gpc::list::List<Point>();
			auto fig2Points = gpc::list::List<Point>();

			for (unsigned int i = 0; i < fig1.length(); i++) { fig1Points.append({fig1[i].x, fig1[i].y}); }
			for (unsigned int i = 0; i < fig2.length(); i++) { fig2Points.append({fig2[i].x, fig2[i].y}); }

			auto pointIntersection = intersect(fig1Points, fig2Points);

			auto res = gpc::list::List<gpc::list::List<gpc::vector::Vec2>>();
			for (unsigned int i = 0; i < pointIntersection.length(); i++) {
				res.append(gpc::list::List<gpc::vector::Vec2>());
				for (unsigned int j = 0; j < pointIntersection[i].length(); j++) {
					res[i].append(gpc::vector::Vec2(pointIntersection[i][j].x, pointIntersection[i][j].y));
				}
			}

			return res;
		}

		gpc::list::List<gpc::list::List<gpc::vector::Vec2>> // gpc::vector::Vec2 **
		intersect(gpc::vector::Vec2 *fig1, unsigned int fig1VertexCount, gpc::vector::Vec2 *fig2,
				  unsigned long long fig2VertexCount) {
			auto fig1Points = gpc::list::List<Point>();
			auto fig2Points = gpc::list::List<Point>();

			for (unsigned int i = 0; i < fig1VertexCount; i++) { fig1Points.append({fig1[i].x, fig1[i].y}); }
			for (unsigned int i = 0; i < fig2VertexCount; i++) { fig2Points.append({fig2[i].x, fig2[i].y}); }

			auto pointIntersection = intersect(fig1Points, fig2Points);

			// auto res = gpc::list::List<gpc::list::List<gpc::vector::Vec2>>();
			// auto res = (gpc::vector::Vec2 **) malloc(sizeof(gpc::vector::Vec2 *) * pointIntersection.length());

			// for (unsigned int i = 0; i < pointIntersection.length(); i++) {
			// 	res[i] = (gpc::vector::Vec2 *) malloc(sizeof(gpc::vector::Vec2) * pointIntersection[i].length());
			// 	for (unsigned int j = 0; j < pointIntersection[i].length(); j++) {
			// 		res[i][j] = gpc::vector::Vec2(pointIntersection[i][j].x, pointIntersection[i][j].y);
			// 	}
			// }

			auto res = gpc::list::List<gpc::list::List<gpc::vector::Vec2>>();
			for (unsigned int i = 0; i < pointIntersection.length(); i++) {
				res.append(gpc::list::List<gpc::vector::Vec2>());
				for (unsigned int j = 0; j < pointIntersection[i].length(); j++) {
					res[i].append(gpc::vector::Vec2(pointIntersection[i][j].x, pointIntersection[i][j].y));
				}
			}

			return res;
		}
	}
}

#endif //GPC_TEMP_INTERSECT_HPP

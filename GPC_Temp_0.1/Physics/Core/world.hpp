//
// Created by penci on 04/06/2020.
//

#ifndef GPC_TEMP_WORLD_HPP
#define GPC_TEMP_WORLD_HPP

#include "object.hpp"
#include "../../Vector/vector.hpp"
#include "../../Basic/basic.hpp"

namespace gpc {
    namespace physics {
        // A container for a physics simulation
        // A world will contain all of the objects inside the simulation, and
        // objects will not collide between worlds
        class World {
        private:
            unsigned long allocatedObjects;
            Object *objects;
            unsigned long objectCount;

        public:
            gpc::vector::Vec2 gravity;

            explicit World(int count = 1) {
                allocatedObjects = count;
                objects = (Object *) malloc(sizeof(Object) * count);
                objectCount = 0;

                gravity = gpc::vector::Vec2(0, 0.1);
            }

            /*
            ~World() {
            	free(objects);
            }
             */

            Object &operator[](unsigned int index) {
                if (index < allocatedObjects) {
                    return objects[index];
                } else {
                    gpc::warning::raiseError("IndexError", "List index out of range");
                }
            }

            void addObject(Object object) {
                if (objectCount < allocatedObjects) {
                    objects[objectCount] = object;
                    objectCount++;
                } else {
                    gpc::warning::raiseError("PhysicsError", "Cannot add more physics bodies to simulation. Please allocate more space");
                }
            }
        };
    }
}

#endif //GPC_TEMP_WORLD_HPP

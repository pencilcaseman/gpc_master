//
// Created by penci on 08/06/2020.
//

#ifndef GPC_TEMP_COLLISION_HPP
#define GPC_TEMP_COLLISION_HPP

#include "../../Vector/vector.hpp"

namespace gpc {
    namespace physics {
        gpc::vector::Vec2 edgeDirection(gpc::vector::Vec2 a, gpc::vector::Vec2 b) {
            return gpc::vector::Vec2(b.x - a.x, b.y - a.y);
        }

        gpc::vector::Vec2 orthogonal(gpc::vector::Vec2 v) {
            return gpc::vector::Vec2(v.y, -v.x);
        }

        gpc::vector::Vec2 *verticesToEdges(gpc::vector::Vec2 *vertices, unsigned int vertexCount) {
            auto res = (gpc::vector::Vec2 *) malloc(sizeof(gpc::vector::Vec2) * vertexCount);
            for (unsigned int i = 0; i < vertexCount; i++) {
                res[i] = edgeDirection(vertices[i], vertices[(i + 1) % vertexCount]);
            }
            return res;
        }

        gpc::vector::Vec2 project(gpc::vector::Vec2 *vertices, unsigned int vertexCount, gpc::vector::Vec2 axis) {
            // auto dots = (double *) malloc(sizeof(double) * vertexCount);
            double min = 999999999;
            double max = -999999999;
            for (unsigned int i = 0; i < vertexCount; i++) {
                auto dot = vertices[i].dot(axis);
                if (dot < min) { min = dot; }
                if (dot > max) { max = dot; }
            }

            return gpc::vector::Vec2(min, max);
        }

        bool contains(double n, gpc::vector::Vec2 range) {
            auto a = range.x;
            auto b = range.y;
            if (b < a) {
                a = range.y;
                b = range.x;
            }
            return (n >= a) && (n <= b);
        }

        bool overlap(gpc::vector::Vec2 a, gpc::vector::Vec2 b) {
            if (contains(a.x, b)) { return true; }
            if (contains(a.y, b)) { return true; }
            if (contains(b.x, a)) { return true; }
            return contains(b.y, a);
        }

        bool colliding(gpc::vector::Vec2 *a, unsigned int aVertCount, gpc::vector::Vec2 *b, unsigned int bVertCount) {
            auto edgesA = verticesToEdges(a, aVertCount);
            auto edgesB = verticesToEdges(b, bVertCount);

            auto edges = (gpc::vector::Vec2 *) malloc(sizeof(gpc::vector::Vec2) * (aVertCount + bVertCount));
            for (unsigned int i = 0; i < aVertCount; i++) { edges[i] = edgesA[i]; }
            for (unsigned int i = aVertCount; i < aVertCount + bVertCount; i++) { edges[i] = edgesB[i - aVertCount]; }

            auto axes = (gpc::vector::Vec2 *) malloc(sizeof(gpc::vector::Vec2) * (aVertCount + bVertCount));
            for (unsigned int i = 0; i < aVertCount + bVertCount; i++) { axes[i] = orthogonal(edges[i]).normalize(); }

            for (unsigned int i = 0; i < aVertCount + bVertCount; i++) {
                auto projectionA = project(a, aVertCount, axes[i]);
                auto projectionB = project(b, bVertCount, axes[i]);
                if (overlap(projectionA, projectionB)) {
                    continue;
                } else {
                    return false;
                }
            }
            return true;
        }
    }
}

#endif //GPC_TEMP_COLLISION_HPP

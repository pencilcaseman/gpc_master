//
// Created by penci on 07/06/2020.
//

#ifndef GPC_TEMP_AABB_HPP
#define GPC_TEMP_AABB_HPP

#include "../../Vector/vector.hpp"

namespace gpc {
    namespace physics {
        namespace bounds {
            typedef struct {
                gpc::vector::Vec2 min;
                gpc::vector::Vec2 max;
            } AABB;

            bool AABBvsAABB(AABB a, AABB b) {
                return (a.max.x > b.min.x && a.min.x < b.max.x) && (a.max.y > b.min.y && a.min.y < b.max.y);
            }
        }
    }
}

#endif //GPC_TEMP_AABB_HPP

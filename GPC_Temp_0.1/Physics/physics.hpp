//
// Created by penci on 04/06/2020.
//

#ifndef GPC_TEMP_PHYSICS_HPP
#define GPC_TEMP_PHYSICS_HPP

#include "Core/object.hpp"
#include "Core/world.hpp"
#include "Core/intersect.hpp"

namespace gpc {
    namespace physics {
        namespace settings {
            double mToP = 100;
        }

        double metersToPixels(double m) {
            return m * settings::mToP;
        }

        double pixelsToMeters(double p) {
            return p / settings::mToP;
        }
    }
}

#endif //GPC_TEMP_PHYSICS_HPP

//
// Created by penci on 25/04/2020.
//

#ifndef GPC_TEMP_DIMENSION_HPP
#define GPC_TEMP_DIMENSION_HPP

#include "../List/list.hpp"

namespace gpc {
    namespace array {
        /**
         * A class to specify the dimensions to create
         * an array from
         */
        class Dimension {
        public:
            /**
             * The dimensions list for an array
             */
            gpc::list::List<unsigned long long> dimensions;

            /**
             * Create a new Dimension type from a pre-existing list
             * @param list A list containing the dimensions for an array
             */
            explicit Dimension(const gpc::list::List<unsigned long long>& list) {
                dimensions = list;
            }

            /*
            ~Dimension() {
            	dimensions.~List();
            }
             */

            /**
             * Create a new Dimension instance
             */
            explicit Dimension() {
                dimensions = gpc::list::List<unsigned long long>(0);
            }

            /**
             * Add a new dimension to an existing Dimension instance
             *
             * For example: Dimension() << 2 << 3
             * gives a 2x3 matrix
             *
             * @param other Dimension to add
             * @return Reference to Dimension object
             */
            Dimension operator<<(unsigned long long other) {
                dimensions.append(other);

                return *this;
            }
        };
    }
}

#endif //GPC_TEMP_DIMENSION_HPP

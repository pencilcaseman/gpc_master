//
// Created by penci on 20/04/2020.
//

#ifndef GPC_TEMP_ARRAY_HPP
#define GPC_TEMP_ARRAY_HPP

#include "../Warnings/raise.hpp"
#include "../List/list.hpp"
#include "../Math/random.hpp"
#include "dimension.hpp"

namespace gpc {
    namespace array {
        /**
         * GPC recursive Array type. It currently
         * supports 2D arrays and larger
         *
         * @tparam arrayType The data type of the Array
         */
        template<typename arrayType = double>
        class Array {
        private:
            // List representing the shape of the Array
            gpc::list::List<unsigned long long> _shape;

            // Array containing and sub arrays (if array is not a Value Array)
            Array<arrayType> *arrayData;

            // If the array is a Value Array this will contain the value data
            arrayType *valueData;

            // Whether or not this Array is a Value Array
            bool isValue = false;

            // List containing the data required to format an Array correctly when printing
            // [ digitsBefore       ,         digitsAfter       ,     leftBefore      ,      right After ]
            gpc::list::List<unsigned long long> printData = gpc::list::List<unsigned long long>(4);

            /**
             * Get the number of digits before and after the
             * decimal point in a string
             *
             * Example:
             *      "123.4567" -> [3, 4]
             *
             * @param val Data value in string form
             * @return GPC List containing the digits before and after, in that order
             */
            inline static gpc::list::List<unsigned long long> _decimalLengths(std::string val) {
                unsigned long long before = 0;
                unsigned long long after = 0;

                if (val.find('.') != std::string::npos) {
                    while (val[before++] != '.') {}
                    while (val[val.length() - after++ - 1] != '.') {}
                } else {
                    before = val.length() + 1;
                    after = 1;
                }

                unsigned long long res[2] = {before - 1, after - 1};
                return {2, (unsigned long long *) res};
            }

            /**
             * Return a string formatting a Matrix correctly - used for
             * <toString()> function
             *
             * Example:
             *      ⌈ 1 2 3 ⌉ --> [[1, 2, 3]
             *      ⌊ 4 5 6 ⌋      [4, 5, 6]]
             *
             * @param matrix 2D Array to format
             * @param initialSpaces Number of spaces before first double bracket
             * @param spaces Number of spaces before the remaining brackets
             * @param lengthData List containing printing data
             * @return
             */
            inline static std::string
            _printMatrix(Array<arrayType> matrix, unsigned long long initialSpaces, unsigned long long spaces,
                         gpc::list::List<unsigned long long> lengthData) {

                // Rows and columns of the matrix
                unsigned long long rows = matrix.shape()[0];
                unsigned long long cols = matrix.arrayData[0].shape()[0];

                // Result string
                std::string res;

                // Add the initial spaces to the result
                for (unsigned long long i = 0; i < initialSpaces; i++) { res += " "; }

                // First bracket and loop all rows
                res += "[";
                for (unsigned long long i = 0; i < rows; i++) {
                    // Add main body spaces and bracket
                    if (i > 0) for (unsigned long long j = 0; j < spaces + 1; j++) { res += " "; }
                    res += "[";

                    // Loop all columns
                    for (unsigned long long j = 0; j < cols; j++) {
                        // Get digits before and after decimal point
                        auto toAdd = gpc::conversion::type::toString(matrix.arrayData[i].valueData[j]);
                        auto _len = _decimalLengths(toAdd);
                        auto before = _len[0];
                        auto after = _len[1];

                        // Add required spaces before value
                        if (j == 0) { for (unsigned long long k = 0; k < lengthData[2] - before; k++) { res += " "; }}
                        else { for (unsigned long long k = 0; k < lengthData[0] - before; k++) { res += " "; }}

                        // Add value
                        res += gpc::conversion::type::toString(matrix.arrayData[i].valueData[j]);


                        // Add required spaces after value
                        if (j + 1 == cols) {
                            for (unsigned long long k = 0; k < lengthData[3] - after; k++) { res += " "; }
                        }
                        else { for (unsigned long long k = 0; k < lengthData[1] - after; k++) { res += " "; }}

                        // Add comma if necessary
                        if (j + 1 < cols) res += ", ";
                    }
                    // Add final bracket
                    res += "]";

                    // Add new line if needed
                    if (i + 1 < rows) res += "\n";
                }

                // Return result with final bracket
                return res + "]";
            }

            /**
             * Convert an Array to a string, formatting it as necessary
             *
             * Example:
             *      For 2D Array see <_printMatrix>
             *      3D Array:
             *          2x3x4 -> [[[ 1,   2,  3,  4]
             *                     [ 4,   5,  6,  7]
             *                     [ 8,   9, 10, 11]]
             *                    [[12,  13, 14, 15]
             *                     [16,  17, 18, 19]
             *                     [20,  21, 22, 23]]]
             *
             * @param depth Number of Arrays below the current one
             * @param initialSpaces Number of spaces to add before first set of brackets
             * @param index Index in parent Array
             * @param lengthData Data containing the printData
             * @return std string of formatted Array
             */
            inline std::string
            _toString(int depth = 0, unsigned long long initialSpaces = 0, unsigned long long index = 0,
                      gpc::list::List<unsigned long long> lengthData = gpc::list::List<unsigned long long>(
                              4)) {
                // For 2D Array
                if (_shape.length() == 2) {
                    return _printMatrix(*this, 0, 0, _getSizes(&printData));
                } else if (_shape.length() == 1) {
                    std::string res = "[";
                    auto pd = _getSizes(&printData);
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        auto val = gpc::conversion::type::toString(valueData[i]);

                        auto _len = _decimalLengths(val);
                        auto before = _len[0];
                        auto after = _len[1];

                        if (i > 0) { for (unsigned long long j = 0; j < pd[0] - before; j++) { res += " "; }}

                        res += val;

                        for (unsigned long long j = 0; j < pd[1] - after; j++) { res += " "; }
                        if (i + 1 < _shape[0]) { res += ", "; }
                    }

                    return res + "]";
                }

                // Length data is not initialized, so initialize it
                if (lengthData[0] == 0) { lengthData = _getSizes(&printData); }

                // Initialize result string with spaces and bracket
                std::string res;
                for (unsigned long long j = 0;
                     j < initialSpaces + 1 - (index == 0 ? depth + 1 : 1); j++) { res += " "; }
                res += "[";

                // Loop current size
                for (unsigned long long i = 0; i < _shape[0]; i++) {
                    // Not the level above a 2D Array, so recurse deeper
                    if (_shape.length() > 3) {
                        // Recurse deeper with updated data
                        res += arrayData[i]._toString(depth + 1, initialSpaces + 1, i, lengthData) +
                               (i + 1 == _shape[0] ? "" : "\n\n");
                    } else {
                        // Stored arrays are 2D, so use <_printMatrix> and add them to the result string
                        std::string add;
                        for (unsigned long long j = 0; j < depth + 1; j++) { add += " "; }
                        res += Array<arrayType>::_printMatrix(arrayData[i], 0, depth + 1, lengthData) +
                               (i + 1 == _shape[0] ? "" : "\n\n" + add);
                    }
                }

                // Return result and add closing bracket
                return res + "]";
            }

            /**
             * Return the <printData> required for formatting an array correctly.
             *
             * It uses a recursive algorithm to search through every value in
             * the array and find the required data to print correctly
             *
             * @param data Reference to the <printData> required to format correctly
             * @return The printData containing the longest number segments
             */
            inline gpc::list::List<unsigned long long> _getSizes(gpc::list::List<unsigned long long> *data) {
                for (unsigned long long i = 0; i < _shape[0]; i++) {
                    if (isValue) {
                        auto lengths = Array<arrayType>::_decimalLengths(gpc::conversion::type::toString(valueData[i]));

                        data->operator[](0) = gpc::math::max(lengths[0], data->operator[](0));
                        data->operator[](1) = gpc::math::max(lengths[1], data->operator[](1));

                        if (i == 0) data->operator[](2) = gpc::math::max(lengths[0], data->operator[](2));
                        if (i + 1 == _shape[0]) data->operator[](3) = gpc::math::max(lengths[1], data->operator[](3));
                    } else {
                        arrayData[i]._getSizes(data);
                    }
                }

                unsigned long long res[4] = {data->operator[](0),
                                             data->operator[](1),
                                             data->operator[](2),
                                             data->operator[](3)};
                return {4, (unsigned long long *) res};
            }

            /**
             * Apply a function to every value in the array
             *
             * @param func The function to apply to every value
             * @return The result of applying the function
             */
            inline Array<arrayType> _map(arrayType (*func)(const arrayType &)) {
                auto res = copy();
                if (isValue) {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        res.valueData[i] = func(res.valueData[i]);
                    }
                } else {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        res.arrayData[i] = res.arrayData[i]._map(func);
                    }
                }

                return res;
            }

            /**
             * Return the result of adding two Arrays of the same size
             * Adds values element-wise. Does not change either Array,
             * simply returns the result of the addition.
             *
             * CAUTION: Arrays must be of the same size and shape for
             *          this to work, otherwise an error will be
             *          thrown
             *
             * @param other The other Array to add
             * @return The result of adding the two Arrays
             */
            inline Array<arrayType> _add(Array<arrayType> other, Array<arrayType> res) {
                if (_shape[0] == other._shape[0]) {
                    if (isValue) {
                        for (unsigned long long i = 0; i < _shape[0]; i++) {
                            res.valueData[i] += other.valueData[i];
                        }
                    } else {
                        for (unsigned long long i = 0; i < _shape[0]; i++) {
                            res.arrayData[i] = res.arrayData[i]._add(other.arrayData[i], res.arrayData[i]);
                        }
                    }
                } else {
                    gpc::warning::raiseError("Array Shape Error", "Cannot add two arrays of different sizes");
                }

                return res;
            }

            /**
             * See <operator+()>
             *
             * Add a single value to every item in an Array object
             *
             * @param other The value to add
             * @param res Result array
             * @return Result array
             */
            inline Array<arrayType> _add(arrayType other, Array<arrayType> res) {
                if (isValue) {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        res.valueData[i] += other;
                    }
                } else {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        res.arrayData[i] = res.arrayData[i]._add(other, res.arrayData[i]);
                    }
                }

                return res;
            }

            /**
             * Return the result of subtracting two Arrays of the same
             * size. Subtracts values element-wise. Does not change either
             * Array, simply returns the result of the subtraction.
             *
             * CAUTION: Arrays must be of the same size and shape for
             *          this to work, otherwise an error will be
             *          thrown
             *
             * @param other The other Array to subtract
             * @return The result of subtracting the two Arrays
             */
            inline Array<arrayType> _sub(Array<arrayType> other, Array<arrayType> res) {
                if (_shape[0] == other._shape[0]) {
                    if (isValue) {
                        for (unsigned long long i = 0; i < _shape[0]; i++) {
                            res.valueData[i] -= other.valueData[i];
                        }
                    } else {
                        for (unsigned long long i = 0; i < _shape[0]; i++) {
                            res.arrayData[i] = res.arrayData[i]._sub(other.arrayData[i], res.arrayData[i]);
                        }
                    }
                } else {
                    gpc::warning::raiseError("Array Shape Error", "Cannot subtract two arrays of different sizes");
                }

                return res;
            }

            /**
             * See <operator-()>
             *
             * Subtract a single value from every item in an Array object
             *
             * @param other The value to subtract
             * @param res Result array
             * @return Result array
             */
            inline Array<arrayType> _sub(arrayType other, Array<arrayType> res) {
                if (isValue) {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        res.valueData[i] -= other;
                    }
                } else {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        res.arrayData[i] = res.arrayData[i]._sub(other, res.arrayData[i]);
                    }
                }

                return res;
            }

            /**
             * Return the result of multiplying two Arrays of the same
             * size. Multiplies values element-wise. Does not change either
             * Array, simply returns the result of the multiplication.
             *
             * CAUTION: Arrays must be of the same size and shape for
             *          this to work, otherwise an error will be
             *          thrown
             *
             * @param other The other Array to multiply
             * @return The result of multiplying the two Arrays
             */
            inline Array<arrayType> _mul(Array<arrayType> other, Array<arrayType> res) {
                if (_shape[0] == other._shape[0]) {
                    if (isValue) {
                        for (unsigned long long i = 0; i < _shape[0]; i++) {
                            res.valueData[i] *= other.valueData[i];
                        }
                    } else {
                        for (unsigned long long i = 0; i < _shape[0]; i++) {
                            res.arrayData[i] = res.arrayData[i]._mul(other.arrayData[i], res.arrayData[i]);
                        }
                    }
                } else {
                    gpc::warning::raiseError("Array Shape Error", "Cannot add two arrays of different sizes");
                }

                return res;
            }

            /**
             * See <operator*()>
             *
             * Multiply every value in an Array object by a single value
             *
             * @param other The value to multiply by
             * @param res Result array
             * @return Result array
             */
            inline Array<arrayType> _mul(arrayType other, Array<arrayType> res) {
                if (isValue) {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        res.valueData[i] *= other;
                    }
                } else {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        res.arrayData[i] = res.arrayData[i]._mul(other, res.arrayData[i]);
                    }
                }

                return res;
            }

            /**
             * Return the result of dividing two Arrays of the same
             * size. Divides values element-wise. Does not change either
             * Array, simply returns the result of the division.
             *
             * CAUTION: Arrays must be of the same size and shape for
             *          this to work, otherwise an error will be
             *          thrown
             *
             * @param other The other Array to divide
             * @return The result of dividing the two Arrays
             */
            inline Array<arrayType> _div(Array<arrayType> other, Array<arrayType> res) {
                if (_shape[0] == other._shape[0]) {
                    if (isValue) {
                        for (unsigned long long i = 0; i < _shape[0]; i++) {
                            res.valueData[i] /= other.valueData[i];
                        }
                    } else {
                        for (unsigned long long i = 0; i < _shape[0]; i++) {
                            res.arrayData[i] = res.arrayData[i]._div(other.arrayData[i], res.arrayData[i]);
                        }
                    }
                } else {
                    gpc::warning::raiseError("Array Shape Error", "Cannot add two arrays of different sizes");
                }

                return res;
            }

            /**
             * See <operator/()>
             *
             * Divide every value in an Array object by a single value
             *
             * @param other The value to divide by
             * @param res Result array
             * @return Result array
             */
            inline Array<arrayType> _div(arrayType other, Array<arrayType> res) {
                if (isValue) {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        res.valueData[i] /= other;
                    }
                } else {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        res.arrayData[i] = res.arrayData[i]._div(other, res.arrayData[i]);
                    }
                }

                return res;
            }

            /**
             * Return the result of applying the modulus operator to
             * two Arrays of the same size. Applies the modulus operator
             * element-wise. Does not change either Array, simply
             * returns the result of the modulus operation.
             *
             * CAUTION: Arrays must be of the same size and shape for
             *          this to work, otherwise an error will be
             *          thrown
             *
             * @param other The other Array to use for the modulus operation
             * @return The result of applying the modulus to the two Arrays
             */
            inline Array<arrayType> _mod(Array<arrayType> other, Array<arrayType> res) {
                if (_shape[0] == other._shape[0]) {
                    if (isValue) {
                        for (unsigned long long i = 0; i < _shape[0]; i++) {
                            res.valueData[i] %= other.valueData[i];
                        }
                    } else {
                        for (unsigned long long i = 0; i < _shape[0]; i++) {
                            res.arrayData[i] = res.arrayData[i]._mod(other.arrayData[i], res.arrayData[i]);
                        }
                    }
                } else {
                    gpc::warning::raiseError("Array Shape Error", "Cannot add two arrays of different sizes");
                }

                return res;
            }

            /**
             * See <operator&()>
             *
             * Modulus every value in an Array by a single value
             *
             * @param other The value to use as the modulus RHS
             * @param res Result array
             * @return Result array
             */
            inline Array<arrayType> _mod(arrayType other, Array<arrayType> res) {
                if (isValue) {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        res.valueData[i] &= other;
                    }
                } else {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        res.arrayData[i] = res.arrayData[i]._mod(other, res.arrayData[i]);
                    }
                }

                return res;
            }

            /**
             * Recursively transpose an array object
             * @param result Pointer to the result array
             * @param self The current array to be processed
             */
            // Recursively transpose an entire Array object
            static inline void _transpose(Array<arrayType> &result, Array<arrayType> &self) {
                if (result._shape.length() == 2) {
                    auto cols = self._shape[1];
                    for (unsigned long long i = 0; i < self._shape[0]; i++) {
                        for (unsigned long long j = 0; j < cols; j++) {
                            result.arrayData[j].valueData[i] = self.arrayData[i].valueData[j];
                        }
                    }
                } else {
                    for (unsigned long long i = 0; i < result._shape[0]; i++) {
                        _transpose(result.arrayData[i], self.arrayData[i]);
                    }
                }
            }

            /**
             * Calculate the matrix product of two Array objects
             *
             * For example:
             *      [[1, 2, 3]  dot  [[1]   =  [[14]
             *       [4, 5, 6]]       [2]       [32]]
             *                        [3]]
             *
             * Function takes two arrays of values given in row-major order
             * as well as a result array. It also takes the rows and columns
             * of the first matrix, as well as the columns of the second matrix
             *
             * @param left List of values in row-major order for the first array
             * @param right List of values in row-major order for the second array
             * @param result List for values of the result array
             * @param M Rows of first
             * @param N Columns of first matrix and rows of first matrix
             * @param K Columns of second matrix
             */
            static inline void
            matMul(const arrayType *left, const arrayType *right, arrayType *result, int M, int N, int K) {
                int in = 0;
                for (int i = 0; i < M; ++i) {
                    int kn = 0;
                    for (int k = 0; k < N; ++k) {
                        arrayType aik = left[in * N + k];
                        for (int j = 0; j < K; ++j) {
                            // std::cout << "Dot Product Information: " << aik << " " << right[kn + j] << "\n";

                            result[in + j] += aik * right[kn + j];
                        }
                        kn += 1;
                    }
                    in += 1;
                }
            }

            /**
             * Convert Array object (matrix form) to an array of values in a square
             * format. The Array is converted into row-major order
             *
             * For example:
             *      [[1, 2, 3]   =>  [1, 2, 3, 4, 5, 6, 0, 0, 0]
             *       [4, 5, 6]]
             *
             *      [[1]
             *       [2]   =>  [1, 0, 0, 2, 0, 0, 3, 0, 0]
             *       [3]]
             *
             * @param squareSize Size of the square to create around the Array object
             * @return List representation of the Array object
             */
            arrayType *squareToFlat(unsigned long long squareSize) {
                auto resSqr = (arrayType *) calloc(squareSize * squareSize, sizeof(arrayType));
                unsigned long long q = 0;
                for (unsigned long long i = 0; i < _shape[0]; i++) {
                    for (unsigned long long j = 0; j < _shape[1]; j++) {
                        resSqr[q + j] = arrayData[i].get(j);
                    }
                    q += squareSize;
                }

                return resSqr;
            }

            /**
             * Convert Array object (matrix form) to an array of values in a flat
             * format. The Array is converted into row-major order
             *
             * For example:
             *      [[1, 2, 3]   =>  [1, 2, 3, 4, 5, 6]
             *       [4, 5, 6]]
             *
             *      [[1]
             *       [2]   =>  [1, 2, 3]
             *       [3]]
             *
             * @param squareSize Size of the square to create around the Array object
             * @return List representation of the Array object
             */
            arrayType *toFlat() {
                auto resSqr = (arrayType *) calloc(_shape[-2] * _shape[-1], sizeof(arrayType));
                unsigned long long q = 0;
                unsigned long long cols = _shape[1];
                for (unsigned long long i = 0; i < _shape[0]; i++) {
                    for (unsigned long long j = 0; j < cols; j++) {
                        resSqr[q + j] = arrayData[i].get(j);
                    }
                    q += cols;
                }

                return resSqr;
            }

            /**
             * Convert a flat, square list into a matrix Array given the
             * rows and colums for the result.
             *
             * For example:
             *      [1, 2, 3, 4, 5, 6, 0, 0, 0] with rows = 2  =>  [[1, 2, 3]
             *                                       cols = 3       [4, 5, 6]]
             *
             * @param data Data to create the Array from
             * @param squareSize Square size of the input data
             * @param rows Rows of result
             * @param cols Columns of result
             * @return Array object with correct size and values
             */
            static Array<arrayType>
            flatToMatrix(arrayType *data, unsigned long long squareSize, unsigned long long rows,
                         unsigned long long cols) {
                auto res = Array<arrayType>(Dimension() << rows << cols);
                unsigned long long q = 0;
                for (unsigned long long i = 0; i < rows; i++) {
                    for (unsigned long long j = 0; j < cols; j++) {
                        res.arrayData[i].get(j) = data[q + j];
                    }
                    q += squareSize;
                }

                return res;
            }

            /**
             * Convert a flat, square list into a matrix Array given the
             * rows and columns for the result.
             *
             * For example:
             *      [1, 2, 3, 4, 5, 6] with rows = 2  =>  [[1, 2, 3]
             *                              cols = 3       [4, 5, 6]]
             *
             * @param data Data to create the Array from
             * @param squareSize Square size of the input data
             * @param rows Rows of result
             * @param cols Columns of result
             * @return Array object with correct size and values
             */
            static Array<arrayType>
            flatToMatrix(arrayType *data, unsigned long long rows, unsigned long long cols) {
                auto res = Array<arrayType>(Dimension() << rows << cols);
                unsigned long long q = 0;
                for (unsigned long long i = 0; i < rows; i++) {
                    for (unsigned long long j = 0; j < cols; j++) {
                        res.arrayData[i].get(j) = data[q + j];
                    }
                    q += cols;
                }

                return res;
            }

            /**
             * Compute the dot product of two Array objects. This function will
             * calculate the product of all matrix sized Arrays, and will return
             * an Array object with the calculated matrices inside it
             *
             * @param a LHS Array object
             * @param b RHS Array object
             * @param res Result of the calculation
             * @param squareSize Square size for rows and columns of input matrices
             */
            static inline void
            _dot(Array<arrayType> *a, Array<arrayType> *b, Array<arrayType> *res, unsigned long long squareSize) {
                if (a->shape().length() > 2) {
                    for (unsigned long long i = 0; i < a->_shape[0]; i++) {
                        _dot(&a->arrayData[i], &b->arrayData[i], &res->arrayData[i], squareSize);
                    }
                } else {
                    auto resSqr = (arrayType *) calloc(squareSize * squareSize, sizeof(arrayType));
                    auto aSqr = a->squareToFlat(squareSize), bSqr = b->squareToFlat(squareSize);
                    matMul(aSqr, bSqr, resSqr, a->_shape[-2], a->_shape[-1], b->_shape[-1]);

                    auto resTmp = Array<arrayType>::flatToMatrix(resSqr, squareSize, a->_shape[-2], b->_shape[-1]);
                    for (unsigned long long i = 0; i < resTmp.shape()[0]; i++) {
                        for (unsigned long long j = 0; j < resTmp.shape()[1]; j++) {
                            res->arrayData[i].get(j) = resTmp.arrayData[i].get(j);
                        }
                    }
                }
            }

            static inline void
            _dot(Array<arrayType> *a, Array<arrayType> *b, Array<arrayType> *res) {
                if (a->shape().length() > 2) {
                    for (unsigned long long i = 0; i < a->_shape[0]; i++) {
                        _dot(&a->arrayData[i], &b->arrayData[i], &res->arrayData[i]);
                    }
                } else {
                    auto resFlat = (arrayType *) calloc(a->_shape[-2] * b->_shape[-1],
                                                        sizeof(arrayType)); // (arrayType *) calloc(squareSize * squareSize, sizeof(arrayType));
                    auto aFlat = a->toFlat(), bFlat = b->toFlat();
                    matMul(aFlat, bFlat, resFlat, a->_shape[-2], a->_shape[-1], b->_shape[-1]);

                    auto resTmp = Array<arrayType>::flatToMatrix(resFlat, a->_shape[-2], b->_shape[-1]);
                    for (unsigned long long i = 0; i < resTmp.shape()[0]; i++) {
                        for (unsigned long long j = 0; j < resTmp.shape()[1]; j++) {
                            res->arrayData[i].get(j) = resTmp.arrayData[i].get(j);
                        }
                    }
                }
            }

        public:

            /**
             * Create an Array initialized only with only 1 dimension and one zero value
             */
            Array() {
                valueData = (arrayType *) malloc(sizeof(arrayType));
                valueData[0] = 0;
                isValue = true;
                unsigned long long shapeData[1] = {1};
                _shape = gpc::list::List<unsigned long long>(1, shapeData);
            }

            /**
             * Create a new Array instance from a given Dimension
             *
             * Example:
             *      Array(Dimension() << 2 << 3) --> [[1, 2, 3]
             *                                        [4, 5, 6]]
             *
             * @param dims Dimension instance for Array size
             */
            explicit Array(Dimension dims, bool fill = true) {
                // Copy the shape
                _shape = dims.dimensions.copy();

                if (dims.dimensions.length() > 1) {
                    // List is not a Value Array so fill <arrayData> with more Arrays
                    arrayData = (Array<arrayType> *) malloc(sizeof(Array<arrayType>) * dims.dimensions[0]);

                    for (unsigned long long i = 0; i < dims.dimensions[0]; i++) {
                        arrayData[i] = Array<arrayType>(Dimension(dims.dimensions.from(1)));
                    }
                } else {
                    // List is a Value Array so fill <valueData> with the Array type
                    valueData = (arrayType *) malloc(sizeof(arrayType) * dims.dimensions[0]);
                    if (fill) {
                        for (unsigned long long i = 0; i < dims.dimensions[0]; i++) {
                            valueData[i] = 0;
                        }
                    }
                    isValue = true;
                }
            }


            ~Array() {
            	free(valueData);
            	free(arrayData);
            	_shape.~List();
            }


            static Array<arrayType> fromList(gpc::list::List<arrayType> list) {
                auto res = Array<arrayType>(Dimension() << list.length());
                for (unsigned long long i = 0; i < list.length(); i++) {
                    res.valueData[i] = list[i];
                }

                return res;
            }

            static Array<arrayType> fromArray(arrayType *data, unsigned long long len) {
                auto res = Array<arrayType>(Dimension() << len);
                memcpy((void *) res.valueData, (void *) data, sizeof(arrayType) * len);

                return res;
            }

            void fillRandom(arrayType min, arrayType max) {
                auto rand = gpc::random::Random(min, max);
                if (isValue) {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        valueData[i] = rand.next();
                    }
                } else {
                    for (unsigned long long i = 0; i < _shape[0]; i++) {
                        arrayData[i].fillRandom(min, max);
                    }
                }
            }

            Array<arrayType> randomize(arrayType min, arrayType max) {
                auto res = copy();
                res.fillRandom(min, max);
                return res;
            }

            /**
             * Access a sub-array within an Array object
             *
             * Passing in a negative number will give elements
             * starting from the end of the Array -- for example
             * giving [-1] will give the last element in the
             * Array
             *
             * Example:
             *      Calling [n] on a 2x3 Array will return a row
             *      of the Matrix
             *
             *      Calling [n] on a 2x3x4 Array will return a
             *      Matrix object of 2x3
             *
             * CAUTION: Will not return a value -- use <.get()>
             *          to access a value -- Calling [0][0] on
             *          a Matrix will raise an error.
             *
             * @param index Index of the sub-array to access
             * @return Sub-array of the parent Array
             */
            inline Array<arrayType> &operator[](unsigned long long index) {
                // Find the value to return
                if (index < _shape[0] && !isValue) {
                    // Value is positive and valid
                    return arrayData[index];
                } else if (-_shape[0] <= index && !isValue) {
                    // Value was negative so return value starting from end of Array
                    return arrayData[index - (-_shape[0])];
                }

                if (!isValue) {
                    // Raise an error as the index was out of range
                    gpc::warning::raiseError("List Index Out of Range",
                                             "Unable to get index " + std::to_string(index) +
                                             " of array with dimension " +
                                             std::to_string(_shape[0]));
                } else {
                    gpc::warning::raiseError("Type Error",
                                             "Cannot access value data using <[]> function -- must use <.get(n)> to access value elements in an Array object.");
                }
            }

            /**
             * Return a value at a given index in a Value Array
             *
             * Like the [] function, if a negative index is passed
             * it will start from the end of the Array and go
             * backwards
             *
             * CAUTION: Will not return an Array, and can only be
             *          used to return a value from an Array object
             * @param index The index of the value to return
             * @return The value of the Value Array at that index
             */
            inline arrayType &get(unsigned long long index) {
                // See <operator[]>
                if (index < _shape[0] && isValue) {
                    return valueData[index];
                } else if (-_shape[0] <= index && isValue) {
                    return valueData[index - (-_shape[0])];
                }

                gpc::warning::raiseError("List Index Out of Range",
                                         "Unable to get index " + std::to_string(index) + " of array with dimension " +
                                         std::to_string(_shape[0]));
            }

            /**
             * Get the shape of the array as a GPC List
             * @return GPC List containing the shape of the array
             */
            inline gpc::list::List<unsigned long long> shape() {
                return _shape.copy();
            }

            /**
             * Return an exact copy of an Array object
             * @return A copy of the Array
             */
            inline Array<arrayType> copy() {
                // Create the empty result array
                Array<arrayType> res;

                // If this Array is a not a Value Array, copy the <arrayData> , otherwise copy the <valueData>
                if (!isValue) {
                    // Allocate memory
                    res.arrayData = (Array<arrayType> *) malloc(sizeof(Array<arrayType>) * _shape[0]);
                    // Copy <arrayData>
                    memcpy((void *) res.arrayData, arrayData, sizeof(Array<arrayType>) * _shape[0]);
                } else {
                    // See above
                    res.valueData = (arrayType *) malloc(sizeof(arrayType) * _shape[0]);
                    memcpy((void *) res.valueData, valueData, sizeof(arrayType) * _shape[0]);
                }

                // Copy remaining data
                res.isValue = isValue;
                res._shape = _shape.copy();

                return res;
            }

            /**
             * Return the result of adding two Arrays of the same size
             * Adds values element-wise. Does not change either Array,
             * simply returns the result of the addition.
             *
             * CAUTION: Arrays must be of the same size and shape for
             *          this to work, otherwise an error will be
             *          thrown
             *
             * @param other The other Array to add
             * @return The result of adding the two Arrays
             */
            inline Array<arrayType> operator+(Array<arrayType> other) {
                return _add(other, copy());
            }

            /**
             * See <operator+(Array<arrayType> other)>
             *
             * Add a single value to every element in an array
             *
             * @param other Value to add
             * @return Result
             */
            inline Array<arrayType> operator+(arrayType other) {
                return _add(other, copy());
            }

            /**
             * See <operator+()>
             *
             * @param other RHS of addition
             */
            void operator+=(Array<arrayType> other) {
                _add(other, *this);
            }

            /**
             * See <operator+()>
             *
             * Add a single value to every element in an array
             *
             * @param other Value to add
             */
            inline Array<arrayType> operator+=(arrayType other) {
                return _add(other, *this);
            }

            /**
             * Return the result of subtracting two Arrays of the same
             * size. Subtracts values element-wise. Does not change either
             * Array, simply returns the result of the subtraction.
             *
             * CAUTION: Arrays must be of the same size and shape for
             *          this to work, otherwise an error will be
             *          thrown
             *
             * @param other The other Array to subtract
             * @return The result of subtracting the two Arrays
             */
            inline Array<arrayType> operator-(Array<arrayType> other) {
                return _sub(other, copy());
            }

            /**
             * See <operator-(Array<arrayType> other)>
             *
             * Subtract a single value from every element in an array
             *
             * @param other Value to subtract
             * @return Result
             */
            inline Array<arrayType> operator-(arrayType other) {
                return _sub(other, copy());
            }

            /**
             * See <operator-()>
             *
             * @param other RHS of subtraction
             */
            void operator-=(Array<arrayType> other) {
                _sub(other, *this);
            }

            /**
             * See <operator-()>
             *
             * @param other RHS of subtraction
             */
            void operator-=(arrayType other) {
                _sub(other, *this);
            }

            /**
             * Return the result of multiplying two Arrays of the same
             * size. Multiplies values element-wise. Does not change either
             * Array, simply returns the result of the multiplication.
             *
             * CAUTION: Arrays must be of the same size and shape for
             *          this to work, otherwise an error will be
             *          thrown
             *
             * @param other The other Array to multiply
             * @return The result of multiplying the two Arrays
             */
            inline Array<arrayType> operator*(Array<arrayType> other) {
                return _mul(other, copy());
            }

            /**
             * Multiply every element in an Array object by a
             * single value
             *
             * @param other Value to multiply by
             * @return Result of multiplication
             */
            inline Array<arrayType> operator*(arrayType other) {
                return _mul(other, copy());
            }

            /**
             * See <operator*()>
             *
             * @param other RHS of multiplication
             */
            void operator*=(Array<arrayType> other) {
                _mul(other, *this);
            }

            /**
             * See <operator*()>
             *
             * @param other RHS of multiplication
             */
            void operator*=(arrayType other) {
                _mul(other, *this);
            }

            /**
             * Return the result of dividing two Arrays of the same
             * size. Divides values element-wise. Does not change either
             * Array, simply returns the result of the division.
             *
             * CAUTION: Arrays must be of the same size and shape for
             *          this to work, otherwise an error will be
             *          thrown
             *
             * @param other The other Array to divide
             * @return The result of dividing the two Arrays
             */
            inline Array<arrayType> operator/(Array<arrayType> other) {
                return _div(other, copy());
            }

            /**
             * Divide every value in an Array object by a single
             * value
             *
             * @param other Value to divide by
             * @return Result of division
             */
            inline Array<arrayType> operator/(arrayType other) {
                return _div(other, copy());
            }

            /**
             * See <operator/()>
             *
             * @param other RHS of division
             */
            void operator/=(Array<arrayType> other) {
                _div(other, *this);
            }

            void operator/=(arrayType other) {
                _div(other, *this);
            }

            /**
             * Return the result of applying the modulus operator to
             * two Arrays of the same size. Applies the modulus operator
             * element-wise. Does not change either Array, simply
             * returns the result of the modulus operation.
             *
             * CAUTION: Arrays must be of the same size and shape for
             *          this to work, otherwise an error will be
             *          thrown
             *
             * @param other The other Array to use for the modulus operation
             * @return The result of applying the modulus to the two Arrays
             */
            inline Array<arrayType> operator%(Array<arrayType> other) {
                return _mod(other, copy());
            }

            /**
             * Modulus ever value in an Array object by a single
             * value
             *
             * @param other The value to use as the RHS of hte modulus operation
             * @return Result
             */
            inline Array<arrayType> operator%(arrayType other) {
                return _mod(other, copy());
            }

            /**
             * See <operator%()>
             *
             * @param other RHS of modulus
             */
            void operator%=(Array<arrayType> other) {
                _mod(other, *this);
            }

            /**
             * See <operator%()>
             *
             * @param other RHS of modulus
             */
            void operator%=(arrayType other) {
                _mod(other, *this);
            }

            /**
             * Transpose an array
             *
             * CAUTION: This function will only transpose the final
             *          two layers of an array
             *
             *          For example:
             *                 5x4x3 transposed = 5x3x4
             *
             * @return The transposed array
             */
            inline Array<arrayType> transpose() {
                if (_shape.length() > 1) {
                    auto resDims = _shape.copy();
                    auto temp = resDims[-2];
                    resDims[-2] = resDims[-1];
                    resDims[-1] = temp;

                    auto res = Array<arrayType>(Dimension(resDims), false);
                    _transpose(res, *this);
                    return res;
                } else {
                    auto res = Array<arrayType>(Dimension() << _shape[0] << 1, false);
                    auto tmp = Array<arrayType>(Dimension() << 1 << _shape[0]);
                    memcpy((void *) tmp.arrayData[0].valueData, (void *) valueData, sizeof(arrayType) * _shape[0]);
                    _transpose(res, tmp);
                    return res;
                }
            }

            /**
             * Calculate the dot product of two Array objects by
             * computing the dot product of all matrices contained
             * within the Array
             *
             * @param other The other Array object to use as the RHS for the calculation
             * @return Array object as the result of the calculation
             */
            inline Array<arrayType> dot(Array<arrayType> other) {
                if (other.shape().length() == 1 && _shape.length() == 1) {
                    if (other.shape()[0] == _shape[0]) {
                        auto res = Array<arrayType>(Dimension() << 1);
                        for (unsigned long long i = 0; i < _shape[0]; i++) {
                            res.get(i) += valueData[i] * other.valueData[i];
                        }

                        return res;
                    } else {
                        gpc::warning::raiseError("Shape Error",
                                                 "Arrays must be of the same size to compute the dot product\n");
                    }
                } else {
                    if (_shape[-1] != other._shape[-2]) {
                        gpc::warning::raiseError("Shape Error",
                                                 "LHS Array must have same columns as RHS Array has rows");
                    }

                    auto resDim = _shape.copy();
                    resDim[-2] = _shape[-2];
                    resDim[-1] = other._shape[-1];
                    Array<arrayType> res = Array<arrayType>(Dimension(resDim));

                    _dot(this, &other, &res);

                    return res;
                }
            }

            /**
             * Apply a function to every value in an Array object
             *
             * @param func The function to apply
             * @return Result of applying the function
             */
            inline Array<arrayType> map(arrayType (*func)(const arrayType &)) {
                return _map(func);
            }

            /**
             * Return a string representation of the Array,
             * formatting it appropriately.
             *
             * Formatting includes:
             *      > Inserting brackets
             *      > Aligning all decimal points
             *
             * @return String representation of the Array
             */
            inline std::string toString() {
                return _toString();
            }
        };
    }
}

/**
 * Ostream function to allow < std::cout << val > where
 * <val> is an Array object
 *
 * @tparam t Typename for the Array object
 * @param os Ostream reference
 * @param val Array object to print
 * @return Ostream reference with Array object printed
 */
template<typename t>
std::ostream &operator<<(std::ostream &os, gpc::array::Array<t> val) {
    return os << val.toString();
}

#endif //GPC_TEMP_ARRAY_HPP

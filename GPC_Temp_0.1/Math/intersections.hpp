//
// Created by penci on 07/06/2020.
//

#ifndef GPC_TEMP_INTERSECTIONS_HPP
#define GPC_TEMP_INTERSECTIONS_HPP

#include "../Vector/vector.hpp"

namespace gpc {
    namespace math {
        gpc::vector::Vec2
        checkIntersection(gpc::vector::Vec2 p1, gpc::vector::Vec2 p2, gpc::vector::Vec2 p3, gpc::vector::Vec2 p4) {
            double m1 = (p2.y - p1.y) / (p2.x - p1.x);
            double m2 = (p4.y - p3.y) / (p4.x - p3.x);

            double x = -(m2 * p3.x + p3.y + m1 * p1.x - p1.y) / (m1 - m2);
            double y = m1 * x - m1 * p1.x + p1.y;

            if ((x > min(p1.x, p2.x)) && (x < max(p1.x, p2.x)) && (y > min(p1.y, p2.y)) && (y < max(p1.y, p2.y))
                && (x > min(p3.x, p4.x)) && (x < max(p3.x, p4.x)) && (y > min(p3.y, p4.y)) && (y < max(p3.y, p4.y))) {
                return gpc::vector::Vec2(x, y);
            } else {
                return gpc::vector::Vec2(nullptr);
            }
        }

        gpc::vector::Vec2 checkIntersectionUnbounded(gpc::vector::Vec2 p1, gpc::vector::Vec2 p2, gpc::vector::Vec2 p3,
                                                     gpc::vector::Vec2 p4) {
            double m1 = (p2.y - p1.y) / (p2.x - p1.x);
            double m2 = (p4.y - p3.y) / (p4.x - p3.x);

            double x = -(m2 * p3.x + p3.y + m1 * p1.x - p1.y) / (m1 - m2);
            double y = m1 * x - m1 * p1.x + p1.y;

            return gpc::vector::Vec2(x, y);
        }
    }
}
#endif //GPC_TEMP_INTERSECTIONS_HPP

//
// Created by penci on 20/04/2020.
//

#ifndef GPC_TEMP_RANDOM_HPP
#define GPC_TEMP_RANDOM_HPP

#include "../Basic/basic.hpp"

namespace gpc {
    namespace random {
        /**
         * A class to generate random values within a given range
         */
        class Random {
        private:
            // Values required for random number generation
            uint64_t x = 0, w = 0, s = 0;
            // The minimum and maximum values for output values and the seed
            double min, max, seed;

        public:
            /**
             * Create a new random object with a range from 0 to 1
             */
            Random() {
                min = 0;
                max = 1;
                seed = (double) gpc::basic::time();
                s = seed;
            }

            /**
             * Create a new random object given a range of output values and
             * a potential seed
             * @param _min The minimum output value
             * @param _max The maximum output value
             * @param _seed The seed for the number generation
             */
            Random(double _min, double _max, double _seed = (double) gpc::basic::time()) {
                min = _min;
                max = _max;
                seed = _seed;
                s = seed;
                randomize();
            }

            /**
             * Randomize a random object
             *
             * This is required because the first few calls
             * to the < next() > function will return similar
             * values, to mitigate this problem this function
             * simply generates a few numbers to increase the
             * "randomness" of the following ones
             *
             * @param n Number of numbers to generate
             */
            void randomize(long n = 10) {
                for (long i = 0; i < n; i++) {
                    next();
                }
            }

            /**
             * Get the seed of a random object
             * @return The seed of the object
             */
            [[nodiscard]] double getSeed() const {
                return seed;
            }

            /**
             * Get a random value from the random object
             * @return A random value in the specified range of the object
             */
            double next() {
                // Calculate the value
                x *= x;
                x += (w += s);
                x = (x >> (unsigned long long) 32) | (x << (unsigned long long) 32);

                // Here x is the random number but it is an integer between 0 and UINT64_MAX
                // so map it between the minimum and maximum values for the object
                return gpc::math::map((double) x, (double) 0, (double) UINT64_MAX, (double) min, (double) max);
            }
        };
    }
}

#endif //GPC_TEMP_RANDOM_HPP

//
// Created by penci on 18/04/2020.
//

#ifndef GPC_TEMP_MATH_HPP
#define GPC_TEMP_MATH_HPP

#include <cmath>

namespace gpc {
    namespace math {
#ifndef PI
        const double PI = 3.141592653589793238462643383279502884197169399375105820974944592307816406286;
#endif
#ifndef TWO_PI
        const double TWO_PI = 6.283185307179586476925286766559005768394338798750211641949889184615632812572;
#endif
#ifndef HALF_PI
        const double HALF_PI = 1.570796326794896619231321691639751442098584699687552910487472296153908203143;
#endif
#ifndef E
        const double E = 2.718281828459045235360287471352662497757247093699959574966967627724076630353;
#endif

        /**
         * Return the smallest of two values of the same type
         *
         * @tparam t The type of the values
         * @param a The first value
         * @param b The second value
         * @return The smallest of the two values
         */
        template<typename t>
        t min(t a, t b) {
            return a <= b ? a : b;
        }

        /**
         * Return the largest of two values of the same type
         *
         * @tparam t The type of the values
         * @param a The first value
         * @param b The second value
         * @return The largest of the two values
         */
        template<typename t>
        t max(t a, t b) {
            return a >= b ? a : b;
        }

        /**
         * Map a value from one range to another
         *
         * For example:
         *      map(5, 0, 10, 0, 1) == 0.5
         *
         * @tparam t Datatype for the values
         * @param n The value to map
         * @param start1 The lower bound for the starting range
         * @param stop1 The upper bound for the starting range
         * @param start2 The lower bound for the result range
         * @param stop2 The upper bound for the result range
         * @return
         */
        template<typename t = double>
        t map(t n, t start1, t stop1, t start2, t stop2) {
            return start2 + (stop2 - start2) * ((n - start1) / (stop1 - start1));
        }

        /**
         * Round a value up to the nearest multiple of another
         *
         * For example:
         *      roundUp(10, 8) == 16
         *
         * @tparam t The type of the value being rounded
         * @param numToRound The value to round up
         * @param multiple The multiple to round to
         * @return The rounded value
         */
        template<typename t>
        t roundUp(t numToRound, t multiple) {
            // Multiple is zero so return value
            if (multiple == 0)
                return numToRound;

            // Calculate the remainder
            t remainder = numToRound % multiple;

            // Remainder is zero so return the value
            if (remainder == 0)
                return numToRound;

            // Return the value added to the multiple, minus the remainder
            // E.g:
            //      num         =       10
            //      mul         =       8
            //      rem         =       2
            //      num + mul   =       18
            //      18 - rem    =       16
            return numToRound + multiple - remainder;
        }

        double dist(double x1, double y1, double x2, double y2) {
            auto p1 = x2 + x1;
            auto p2 = y2 + y1;
            return sqrt(p1 * p1 + p2 * p2);
        }
    }
}

#endif //GPC_TEMP_MATH_HPP

//
// Created by penci on 06/06/2020.
//

#ifndef GPC_TEMP_MATRIX_HPP
#define GPC_TEMP_MATRIX_HPP

#include <cmath>
#include "../Vector/vector.hpp"

namespace gpc {
	namespace matrix {
		class Matrix2x2 {
			double m00, m01, m10, m11;

		public:
			Matrix2x2(double a = 0, double b = 0, double c = 0, double d = 0) {
				m00 = a;
				m01 = b;
				m10 = c;
				m11 = d;
			}

			//Create from angle in radians
			void set(real radians) {
				real c = cos(radians);
				real s = sin(radians);

				m00 = c;
				m01 = -s;
				m10 = s;
				m11 = c;
			}

			Matrix2x2 transpose() {
				return Matrix2x2(m00, m10, m01, m11);
			}

			gpc::vector::Vec2 operator*(gpc::vector::Vec2 rhs) {
				return gpc::vector::Vec2(m00 * rhs.x + m01 * rhs.y, m10 * rhs.x + m11 * rhs.y);
			}

			Matrix2x2 operator*(Matrix2x2 rhs) {
				return Matrix2x2(
						m00 * rhs.m00 + m01 * rhs.m10, m00 * rhs.m01 + m01 * rhs.m11,
						m10 * rhs.m00 + m11 * rhs.m10, m10 * rhs.m01 + m11 * rhs.m11);
			}

			Matrix2x2 copy() {
				return Matrix2x2(m00, m01, m10, m11);
			}
		};
	}
}

#endif //GPC_TEMP_MATRIX_HPP

//
// Created by penci on 22/03/2020.
//

#ifndef GPC_COLORS_HPP
#define GPC_COLORS_HPP

namespace gpc {
    namespace color {
        /**
         * Class containing many ANSI escape sequence colour codes,
         * as well as an RGB function to create custom colours
         */
        class Color {
        public:
            // Ansi colour codes
            std::string RESET = "\x1b[0m";
            std::string BOLD = "\x1b[1m";
            std::string ITALIC = "\x1b[3m";
            std::string UNDERLINE = "\x1b[4m";
            std::string STRIKETHROUGH = "\x1b[9m";
            std::string THICK_UNDERLINE = "\x1b[21m";
            std::string WHITE = "\x1b[30m";
            std::string RED = "\x1b[31m";
            std::string GREEN = "\x1b[32m";
            std::string YELLOW = "\x1b[33m";
            std::string BLUE = "\x1b[34m";
            std::string PURPLE = "\x1b[35m";
            std::string TURQUOISE = "\x1b[36m";
            std::string GREY = "\x1b[37m";
            std::string LIGHT_GREY = "\x1b[38m";
            std::string WHITE_BACKGROUND = "\x1b[40m";
            std::string RED_BACKGROUND = "\x1b[41m";
            std::string GREEN_BACKGROUND = "\x1b[42m";
            std::string YELLOW_BACKGROUND = "\x1b[43m";
            std::string BLUE_BACKGROUND = "\x1b[44m";
            std::string PURPLE_BACKGROUND = "\x1b[45m";
            std::string TURQUOISE_BACKGROUND = "\x1b[46m";
            std::string GREY_BACKGROUND = "\x1b[47m";
            std::string BORDER = "\x1b[51m";
            std::string BRIGHT_RED = "\x1b[91m";
            std::string BRIGHT_GREEN = "\x1b[92m";
            std::string BRIGHT_YELLOW = "\x1b[93m";
            std::string BRIGHT_BLUE = "\x1b[94m";
            std::string BRIGHT_PURPLE = "\x1b[95m";
            std::string BRIGHT_TURQUOISE = "\x1b[96m";
            std::string BRIGHT_BLACK = "\x1b[97m";
            std::string BRIGHT_GREY = "\x1b[98m";
            std::string BRIGHT_RED_BACKGROUND = "\x1b[101m";
            std::string BRIGHT_GREEN_BACKGROUND = "\x1b[102m";
            std::string BRIGHT_YELLOW_BACKGROUND = "\x1b[103m";
            std::string BRIGHT_BLUE_BACKGROUND = "\x1b[104m";
            std::string BRIGHT_PURPLE_BACKGROUND = "\x1b[105m";
            std::string BRIGHT_TURQUOISE_BACKGROUND = "\x1b[106m";
            std::string BRIGHT_BLACK_BACKGROUND = "\x1b[107m";
            std::string BRIGHT_GREY_BACKGROUND = "\x1b[100m";

            /**
             * Default constructor for Color class
             */
            explicit Color() = default;

            /**
             * Create an RGB color code as a string for streaming
             * @param r Red (0-255)
             * @param g Green (0-255)
             * @param b Blue (0-255)
             * @return Escape sequence with RGB colour value
             */
            std::string RGBCol(int r, int g, int b) {
                std::string res = "\x1b[38;2;";
                res += std::to_string(r) + ";";
                res += std::to_string(g) + ";";
                res += std::to_string(b) + "m";
                return res;
            }
        };
    }
}

#endif //GPC_COLORS_HPP

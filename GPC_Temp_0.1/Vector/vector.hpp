//
// Created by penci on 04/06/2020.
//

#ifndef GPC_TEMP_VECTOR_HPP
#define GPC_TEMP_VECTOR_HPP

#include <cmath>
#include "../List/list.hpp"

namespace gpc {
    namespace vector {
        /**
         * A vector with two dimensions, an x component and a y component
         */
        class Vec2 {
            bool isValidX_;
            bool isValidY_;

        public:
            double x;
            double y;

            explicit Vec2(double xx = 0, double yy = 0) {
                x = xx;
                y = yy;

                isValidX_ = true;
                isValidY_ = true;
            }

            explicit Vec2(void *) {
                x = 0;
                y = 0;
                isValidX_ = false;
                isValidY_ = false;
            }

            [[nodiscard]] double max() const { return x > y ? x : y; }

            [[nodiscard]] double min() const { return x < y ? x : y; }

            [[nodiscard]] double mag() const { return sqrt(x * x + y * y); }

            [[nodiscard]] double magSquared() const { return x * x + y * y; }

            gpc::vector::Vec2 normalize() const {
                double m = max();
                // x /= m;
                // y /= m;
                return Vec2(x / m, y / m);
            }

            [[nodiscard]] bool isValidX() const { return isValidX_; }

            [[nodiscard]] bool isValidY() const { return isValidY_; }

            [[nodiscard]] bool isValid() const { return isValidX_ && isValidY_; }

            void makeValidX() { isValidX_ = true; }

            void makeValidY() { isValidY_ = true; }

            void makeValid() { isValidX_ = true; isValidY_ = true; }

            static Vec2 fromAngle(double theta = 0) { return Vec2(sin(theta), cos(theta)); }

            [[nodiscard]] double toAngle(double x1 = 0, double y1 = 0) const { return atan2(x - x1, y - y1); }

            [[nodiscard]] double dot(Vec2 other) const { return x * other.x + y * other.y; }

            [[nodiscard]] double dot(double other) const { return x * other + y * -other; }

            Vec2 operator+(Vec2 other) const { return Vec2(x + other.x, y + other.y); }

            Vec2 operator+(double other) const { return Vec2(x + other, y + other); }

            Vec2 operator-(Vec2 other) const { return Vec2(x + other.x, y + other.y); }

            Vec2 operator-(double other) const { return Vec2(x - other, y - other); }

            Vec2 operator*(Vec2 other) const { return Vec2(x * other.x, y * other.y); }

            Vec2 operator*(double other) const { return Vec2(x * other, y * other); }

            Vec2 operator/(Vec2 other) const { return Vec2(x / other.x, y / other.y); }

            Vec2 operator/(double other) const { return Vec2(x / other, y / other); }

            Vec2 &operator+=(Vec2 other) {
                x += other.x;
                y += other.y;

                return *this;
            }

            Vec2 &operator+=(double other) {
                x += other;
                y += other;

                return *this;
            }

            Vec2 &operator-=(Vec2 other) {
                x -= other.x;
                y -= other.y;

                return *this;
            }

            Vec2 &operator-=(double other) {
                x -= other;
                y -= other;

                return *this;
            }

            Vec2 &operator*=(Vec2 other) {
                x *= other.x;
                y *= other.y;

                return *this;
            }

            Vec2 &operator*=(double other) {
                x *= other;
                y *= other;

                return *this;
            }

            Vec2 &operator/=(Vec2 other) {
                x /= other.x;
                y /= other.y;

                return *this;
            }

            Vec2 &operator/=(double other) {
                x /= other;
                y /= other;

                return *this;
            }

            [[nodiscard]] std::string toString() const {
                return std::string("(x: ") + std::to_string(x) + ", y: " + std::to_string(y) + ")";
            }

            Vec2 copy() const {
                return Vec2(x, y);
            }
        };

        Vec2 operator/(double x, Vec2 other) { return Vec2(x / other.x, x / other.y); }

        std::ostream &operator<<(std::ostream &os, Vec2 val) {
            return os << val.toString();
        }


        /**
         * A vector with three dimensions, an x component, a y component
         * and a z component
         */
        class Vec3 {
        public:
            double x;
            double y;
            double z;

            explicit Vec3(double xx = 0, double yy = 0, double zz = 0) {
                x = xx;
                y = yy;
                z = zz;
            }

            [[nodiscard]] double max() const { return x > y ? x > z ? x : z : y > z ? y : z; }

            [[nodiscard]] double min() const { return x < y ? x < z ? x : z : y < z ? y : z; }

            [[nodiscard]] double mag() const { return sqrt(x * x + y * y + z * z); }

            [[nodiscard]] double magSquared() const { return x * x + y * y + z * z; }

            void normalize() {
                double m = max();
                x /= m;
                y /= m;
                z /= m;
            }

            static Vec3 fromAngle(double theta = 0, double phi = 0) {
                return Vec3(cos(theta) * cos(phi), sin(theta) * cos(phi), sin(phi));
            }

            [[nodiscard]] gpc::list::List<double> toAngle(double x1 = 0, double y1 = 0, double z1 = 0) const {
                double theta = atan((y - y1) / (x - x1));
                double phi = atan((z - z1) / sqrt(((y - y1) * (y - y1)) / ((x - x1) * (x - x1))));
                auto res = gpc::list::List<double>(2);
                res[0] = theta;
                res[1] = phi;

                return res;
            }

            [[nodiscard]] double dot(Vec3 other) const {
                return x * other.x + y * other.y + z * other.z;
            }

            Vec3 operator+(Vec3 other) const { return Vec3(x + other.x, y + other.y, z + other.z); }

            Vec3 operator+(double other) const { return Vec3(x + other, y + other, z + other); }

            Vec3 operator-(Vec3 other) const { return Vec3(x + other.x, y + other.y, z - other.z); }

            Vec3 operator-(double other) const { return Vec3(x - other, y - other, z - other); }

            Vec3 operator*(Vec3 other) const { return Vec3(x * other.x, y * other.y, z * other.z); }

            Vec3 operator*(double other) const { return Vec3(x * other, y * other, z * other); }

            Vec3 operator/(Vec3 other) const { return Vec3(x / other.x, y / other.y, z / other.z); }

            Vec3 operator/(double other) const { return Vec3(x / other, y / other, z / other); }

            Vec3 &operator+=(Vec3 other) {
                x += other.x;
                y += other.y;
                z += other.z;

                return *this;
            }

            Vec3 &operator+=(double other) {
                x += other;
                y += other;
                z += other;

                return *this;
            }

            Vec3 &operator-=(Vec3 other) {
                x -= other.x;
                y -= other.y;
                z -= other.z;

                return *this;
            }

            Vec3 &operator-=(double other) {
                x -= other;
                y -= other;
                z -= other;

                return *this;
            }

            Vec3 &operator*=(Vec3 other) {
                x *= other.x;
                y *= other.y;
                z *= other.z;

                return *this;
            }

            Vec3 &operator*=(double other) {
                x *= other;
                y *= other;
                z *= other;

                return *this;
            }

            Vec3 &operator/=(Vec3 other) {
                x /= other.x;
                y /= other.y;
                z /= other.z;

                return *this;
            }

            Vec3 &operator/=(double other) {
                x /= other;
                y /= other;
                z /= other;

                return *this;
            }

            [[nodiscard]] std::string toString() const {
                return std::string("(x: ") + std::to_string(x) + ", y: " + std::to_string(y) + ", z: " +
                       std::to_string(z) +
                       ")";
            }
        };

        std::ostream &operator<<(std::ostream &os, Vec3 val) {
            return os << val.toString();
        }


        /**
         * A vector with four dimensions, an x component, a y component,
         * a z component and a w component
         */
        class Vec4 {
        public:
            double x;
            double y;
            double z;
            double w;

            explicit Vec4(double xx = 0, double yy = 0, double zz = 0, double ww = 0) {
                x = xx;
                y = yy;
                z = zz;
                w = ww;
            }

            [[nodiscard]] double max() const {
                return x > y ? x > z ? x > w ? x : w : z > w ? z : w : y > z ? y > w ? y : w : z > w ? z : w;
            }

            [[nodiscard]] double min() const {
                return x < y ? x < z ? x < w ? x : w : z < w ? z : w : y < z ? y < w ? y : w : z < w ? z : w;
            }

            [[nodiscard]] double mag() const { return sqrt(x * x + y * y + z * z + w * w); }

            [[nodiscard]] double magSquared() const { return x * x + y * y + z * z + w * w; }

            void normalize() {
                double m = max();
                x /= m;
                y /= m;
                z /= m;
                w /= m;
            }

            static Vec4 fromAngle(double theta = 0, double phi = 0) {
                return Vec4(cos(theta) * cos(phi), sin(theta) * cos(phi), sin(phi), 0);
            }

            [[nodiscard]] gpc::list::List<double> toAngle(double x1 = 0, double y1 = 0, double z1 = 0) const {
                double theta = atan((y - y1) / (x - x1));
                double phi = atan((z - z1) / sqrt(((y - y1) * (y - y1)) / ((x - x1) * (x - x1))));
                auto res = gpc::list::List<double>(3);
                res[0] = theta;
                res[1] = phi;
                res[2] = 0;

                return res;
            }

            [[nodiscard]] double dot(Vec4 other) const {
                return x * other.x + y * other.y + z * other.z + w * other.w;
            }

            Vec4 operator+(Vec4 other) const { return Vec4(x + other.x, y + other.y, z + other.z, w + other.w); }

            Vec4 operator+(double other) const { return Vec4(x + other, y + other, z + other, w + other); }

            Vec4 operator-(Vec4 other) const { return Vec4(x + other.x, y + other.y, z - other.z, w - other.w); }

            Vec4 operator-(double other) const { return Vec4(x - other, y - other, z - other, w - other); }

            Vec4 operator*(Vec4 other) const { return Vec4(x * other.x, y * other.y, z * other.z, w * other.w); }

            Vec4 operator*(double other) const { return Vec4(x * other, y * other, z * other, w * other); }

            Vec4 operator/(Vec4 other) const { return Vec4(x / other.x, y / other.y, z / other.z, w / other.w); }

            Vec4 operator/(double other) const { return Vec4(x / other, y / other, z / other, w / other); }

            Vec4 &operator+=(Vec4 other) {
                x += other.x;
                y += other.y;
                z += other.z;
                w += other.w;

                return *this;
            }

            Vec4 &operator+=(double other) {
                x += other;
                y += other;
                z += other;
                w += other;

                return *this;
            }

            Vec4 &operator-=(Vec4 other) {
                x -= other.x;
                y -= other.y;
                z -= other.z;
                w -= other.w;

                return *this;
            }

            Vec4 &operator-=(double other) {
                x -= other;
                y -= other;
                z -= other;
                w -= other;

                return *this;
            }

            Vec4 &operator*=(Vec4 other) {
                x *= other.x;
                y *= other.y;
                z *= other.z;
                w *= other.w;

                return *this;
            }

            Vec4 &operator*=(double other) {
                x *= other;
                y *= other;
                z *= other;
                w *= other;

                return *this;
            }

            Vec4 &operator/=(Vec4 other) {
                x /= other.x;
                y /= other.y;
                z /= other.z;
                w /= other.w;

                return *this;
            }

            Vec4 &operator/=(double other) {
                x /= other;
                y /= other;
                z /= other;
                w /= other;

                return *this;
            }

            [[nodiscard]] std::string toString() const {
                return std::string("(x: ") + std::to_string(x) + ", y: " + std::to_string(y) + ", z: " +
                       std::to_string(z) +
                       ", w: " + std::to_string(w) + ")";
            }
        };

        double dist(gpc::vector::Vec2 a, gpc::vector::Vec2 b) {
            auto p = b - a;
            return sqrt(p.x * p.x + p.y * p.y);
        }

        std::ostream &operator<<(std::ostream &os, Vec4 val) {
            return os << val.toString();
        }
    }
}

#endif //GPC_TEMP_VECTOR_HPP

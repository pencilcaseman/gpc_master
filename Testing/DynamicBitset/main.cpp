#include <iostream>

/*
 * Binary values stored as lists of <unsigned long long *>
 *
 * Eg: 1001 1010 => |1010|, |1001|
 *
 * Values stored in reverse order to increase speed and usability
 *
 * #################################################################################
 * TO DO
 *
 * Optimise operations by making use of first and last set bits
 * #################################################################################
 *
 */


#define lluSize (sizeof(unsigned long long) * (size_t) 8)

unsigned long long roundUp(unsigned long long numToRound, unsigned long long multiple) {
    if (multiple == 0)
        return numToRound;

    unsigned long long remainder = numToRound % multiple;
    if (remainder == 0)
        return numToRound;

    return numToRound + multiple - remainder;
}

class _BinContainer {
private:
    unsigned long long *binVal;
    unsigned long long n;

public:
    _BinContainer(unsigned long long *val, unsigned long long index) {
        binVal = val;
        n = index;
    }

    _BinContainer &operator=(unsigned long long val) {
        if (val > 0) {
            *binVal |= (unsigned long long) ((unsigned long long) 1 << n);
        } else {
            *binVal &= (unsigned long long) ~((unsigned long long) 1 << n);
        }
        return *this;
    }

    explicit operator char() {
        return (*binVal & (unsigned long long) ((unsigned long long) 1 << n)) > 0 ? '1' : '0';
    }

    explicit operator int() {
        return (*binVal & (unsigned long long) ((unsigned long long) 1 << n)) > 0 ? 1 : 0;
    }

    explicit operator unsigned long long() {
        return (unsigned long long) (int) *this;
    }
};

class DynamicBitset : public std::error_code {
public:
    unsigned long long binLen;
    unsigned long long *bin;
    unsigned long long bits;

    explicit DynamicBitset() {
        binLen = 1; // (len - 1) / lluSize + 1; // (n - 1) // 64 + 1
        bin = (unsigned long long *) malloc(lluSize);
        bits = lluSize;
        bin[0] = 0;
    }

    explicit DynamicBitset(unsigned long long val) {
        binLen = 1; // (n - 1) // 64 + 1
        bin = (unsigned long long *) malloc( sizeof(unsigned long long) * (size_t) binLen);
        bits = lluSize;
        bin[0] = val;
    }

    explicit DynamicBitset(unsigned long long len, unsigned long long val) {
        binLen = (len - 1) / lluSize + 1; // (n - 1) // 64 + 1
        bin = (unsigned long long *) malloc( sizeof(unsigned long long) * (size_t) binLen);
        bits = len;

        for (unsigned long long i = 1; i < binLen; i++) {
            bin[i] = 0;
        }

        bin[0] = val;
    }

    unsigned long long testBit(unsigned long long n) {
        return (bin[n / (lluSize)]) & ((unsigned long long) 1 << n % (lluSize));
    }

    DynamicBitset copy() {
        DynamicBitset res;
        res.bin = (unsigned long long *) malloc(sizeof(unsigned long long) * (size_t) binLen);

        for (unsigned long long i = 0; i < binLen; i++) {
            res.bin[i] = bin[i];
        }

        res.binLen = binLen;
        res.bits = bits;

        return res;
    }

    static DynamicBitset empty(unsigned long long bits) {
        DynamicBitset res;
        res.binLen = (bits - 1) / lluSize + 1;
        res.bits = bits;
        res.bin = (unsigned long long *) malloc(sizeof(unsigned long long) * (size_t) res.binLen);

        for (unsigned long long i = 0; i < res.binLen; i++) {
            res.bin[i] = 0;
        }

        return res;
    }

    _BinContainer operator[](unsigned long long index) {
        return _BinContainer(&bin[index / (lluSize)], index % (lluSize));
    }

    DynamicBitset operator&(DynamicBitset other) {
        unsigned long long max = binLen > other.binLen ? binLen : other.binLen;
        unsigned long long min = binLen < other.binLen ? binLen : other.binLen;
        auto res = empty(max);

        for (unsigned long long i = 0; i < min; i++) {
            std::cout << "Doing an and! ############################################################################\n";
            res.bin[i] = bin[i] & other.bin[i];
        }

        return res;
    }

    DynamicBitset operator|(DynamicBitset other) {
        unsigned long long max = binLen > other.binLen ? binLen : other.binLen;
        unsigned long long min = binLen < other.binLen ? binLen : other.binLen;
        auto res = copy();

        for (unsigned long long i = 0; i < min; i++) {
            res.bin[i] = bin[i] | other.bin[i];
        }

        return res;
    }

    DynamicBitset operator^(DynamicBitset other) {
        unsigned long long max = binLen > other.binLen ? binLen : other.binLen;
        unsigned long long min = binLen < other.binLen ? binLen : other.binLen;
        auto res = copy();

        for (unsigned long long i = 0; i < min; i++) {
            res.bin[i] = bin[i] ^ other.bin[i];
        }

        return res;
    }

    DynamicBitset operator~() {
        auto res = DynamicBitset::empty(bits); // copy();

        for (unsigned long long i = 0; i < res.binLen; i++) {
            res.bin[i] = ~bin[i];
        }

        return res;
    }

    void operator&=(DynamicBitset other) {
        *this = *this & other;
    }

    void operator|=(DynamicBitset other) {
        *this = *this | other;
    }

    void operator^=(DynamicBitset other) {
        *this = *this ^ other;
    }

    DynamicBitset operator<<(unsigned long long dist) {
        DynamicBitset res = DynamicBitset::empty(bits + dist);

        for (unsigned long long i = 0; i < bits; i++) {
            if (i < bits) {
                if (testBit(i) > 0) {
                    res[i + dist] = 1;
                }
            }
        }

        return res;
    }

    DynamicBitset operator>>(unsigned long long dist) {
        DynamicBitset res = empty(bits);

        if (dist < bits) {
            for (long long i = (long long) bits - 1; i >= 0; i--) {
                if (i - dist >= 0) {
                    if (testBit(i) > 0) {
                        res[i - dist] = 1;
                    }
                }
            }
        }

        return res;
    }

    explicit operator char *() {
        char *res = (char *) malloc(sizeof(char) * (size_t) bits + 1);
        unsigned long long index = 0;
        bool insert = false;
        for (unsigned long long i = bits; i > 0; i--) {
            char val = ((bin[(i - 1) / (lluSize)]) &
                        ((unsigned long long) ((unsigned long long) 1
                                << ((i - 1) % (lluSize))))) >
                       0 ? '1' : '0';

            if (!insert) {
                if (val == '1') {
                    insert = true;
                    res[index++] = val;
                }
            } else {
                res[index++] = val;
            }
        }

        if (index == 0) {
            res[index++] = '0';
        }
        res[index] = '\0';

        return res;
    }
};

std::ostream &operator<<(std::ostream &os, DynamicBitset &db) {
    os << (char *) db;
    return os;
}

std::ostream &operator<<(std::ostream &os, _BinContainer &bc) {
    os << (char) bc;
    return os;
}

int main() {
    std::cout << "Hello, World!" << std::endl;

    auto value = DynamicBitset(1000000000, 9223372036854775808);
    std::cout << "Some important information: " << value.binLen << "\n";
    //std::cout << "Printing the value: " << value << "\n";

    auto bitwiseAnd = value & DynamicBitset(10);
    //std::cout << "Testing operator (&): " << bitwiseAnd << "\n";
    std::cout << "Some important information: " << bitwiseAnd.bin[0] << "\n";

    std::cout << "Complete\n";

    auto bitwiseOr = value | DynamicBitset(10);
    std::cout << "Or values: \n";
    //std::cout << value << "\n";
    //std::cout << DynamicBitset(10) << "\n";
    //std::cout << "Testing operator (|): " << bitwiseOr << "\n";

    std::cout << "Complete\n";

    auto bitwiseXOR = value ^ DynamicBitset(10);
    //std::cout << "Testing operator (^): " << (char *) bitwiseXOR << "\n";

    std::cout << "Complete\n";

    auto bitwiseNot = ~value;
    //std::cout << "Testing operator (~): " << bitwiseNot << "\n";

    std::cout << "Complete\n";

    bitwiseAnd &= DynamicBitset(2);
    //std::cout << "Testing operator (&=): " << bitwiseAnd << "\n";

    std::cout << "Complete\n";

    bitwiseOr |= DynamicBitset(2);
    //std::cout << "Testing operator (|=): " << bitwiseOr << "\n";

    std::cout << "Complete\n";

    bitwiseXOR ^= DynamicBitset(2);
    //std::cout << "Testing operator (^=): " << bitwiseXOR << "\n";

    //std::cout << "Testing index ((^) [n]): " << (char) bitwiseXOR[4] << "\n";

    std::cout << "Complete\n";

    bitwiseXOR[4] = 0;
    //std::cout << "Testing index after setting it ((^) [n]): " << (char) bitwiseXOR[3] << "\n";

    std::cout << "Complete\n";

    //std::cout << "Testing value after assignment ((^) [n]): " << bitwiseXOR << "\n";

    // Bit shifts
    auto shift = DynamicBitset(10);
    //std::cout << "Value: " << shift << "\n";

    auto res = shift << 1;
    //std::cout << "Result: " << res << "\n";

    res = shift >> 1;
    //std::cout << "Result: " << res << "\n";
    //std::cout << "Complete... Lets just hope it is correct..." << "\n";

    std::cout << "Complete...\n";

    return 0;
}

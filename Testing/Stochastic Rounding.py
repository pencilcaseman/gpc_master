from random import *
from math import *

print("Stochastic Rounding")

def round(n):
    rndUp = abs(n - floor(n))

    if (uniform(0, 1) < rndUp):
        return ceil(n)
    else:
        return floor(n)

loops = 10000
av = 0
for i in range(loops):
    n = 0.03
    total = 0
    for i in range(1000):
        total += round(n)

    # print("Total:", total)
    av += total / loops

print(av)

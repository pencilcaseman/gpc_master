def mul(a, b, base): # Operands containing rightmost digits at index 1
    p = len(a)
    q = len(b)
    
    product = [0 for i in range(p + q)] # Allocate space for result
    for b_i in range(1, q): # for all digits in b
        carry = 0
        for a_i in range(1, p): # for all digits in a
            product[a_i + b_i - 1] += carry + a[a_i] * b[b_i]
            carry = product[a_i + b_i - 1] // base
            product[a_i + b_i - 1] = product[a_i + b_i - 1] % base
        product[b_i + p] = carry # last digit comes from final carry
    return product

def karatsuba(x,y):
    """Function to multiply 2 numbers in a more efficient manner than the grade school algorithm"""
    if len(str(x)) == 1 or len(str(y)) == 1:
        return x*y
    else:
        n = max(len(str(x)),len(str(y)))
        nby2 = int(n / 2)
		
        a = int(x // 10**(nby2))
        b = int(x % 10**(nby2))
        c = int(y // 10**(nby2))
        d = int(y % 10**(nby2))
		
        ac = karatsuba(a,c)
        bd = karatsuba(b,d)
        ad_plus_bc = karatsuba(a+b,c+d) - ac - bd
        
        # this little trick, writing n as 2*nby2 takes care of both even and odd n
        prod = ac * 10**(2*nby2) + (ad_plus_bc * 10**nby2) + bd

    return prod


print("Multiplication Testing")
a = [0, 0, 2]
b = [0, 0, 1]

print(mul(a, b, 10))
print(karatsuba(int(99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999),
                int(99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999)))

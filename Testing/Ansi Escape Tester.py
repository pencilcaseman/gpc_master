print("Ansi escape sequence testing...")

def printEscape(esc):
    print("\tstd::cout << \"{}\" << \"Escape code \" << \"{}\" << \"\\033[0m\" << std::endl;".format(esc, esc[5:]))

for i in range(200):
    printEscape("\\033[{}m".format(i))
          

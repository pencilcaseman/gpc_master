#include <iostream>
#include <tuple>
#include <cstring>

class Test {
public:
    int x;

    Test(int y) {
        x = y;
    }
};

int main() {
    std::cout << "Hello, World!" << std::endl;

    return 0;
}

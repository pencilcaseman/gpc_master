"""

Begin
   initially push some special character say # into the stack
   for each character ch from infix expression, do
      if ch is alphanumeric character, then
         add ch to postfix expression
      else if ch = opening parenthesis (, then
         push ( into stack
      else if ch = ^, then            //exponential operator of higher precedence
         push ^ into the stack
      else if ch = closing parenthesis ), then
         while stack is not empty and stack top ≠ (,
            do pop and add item from stack to postfix expression
         done

         pop ( also from the stack
      else
         while stack is not empty AND precedence of ch <= precedence of stack top element, do
            pop and add into postfix expression
         done

         push the newly coming character.
   done

   while the stack contains some remaining characters, do
      pop and add to the postfix expression
   done
   return postfix
End

"""


# https://scriptasylum.com/tutorials/infix_postfix/algorithms/postfix-evaluation/index.html

import math


class Term:
    def __init__(self, val):
        self.value = val

    def __str__(self):
        return str(self.value)

    def __add__(self, other):
        if isinstance(other, Term):
            return Term(self.value + other.value)
        else:
            return Term(self.value + float(other))

    def __sub__(self, other):
        if isinstance(other, Term):
            return Term(self.value - other.value)
        else:
            return Term(self.value - float(other))

    def __mul__(self, other):
        if isinstance(other, Term):
            return Term(self.value * other.value)
        else:
            return Term(self.value * float(other))

    def __truediv__(self, other):
        if isinstance(other, Term):
            return Term(self.value / other.value)
        else:
            return Term(self.value / float(other))

    def __pow__(self, other):
        if isinstance(other, Term):
            return Term(self.value ** other.value)
        else:
            return Term(self.value ** float(other))


numbers = [str(i) for i in range(10)]
letters = [chr(i) for i in range(65, 65 + 26)]
for i in range(97, 97 + 26):
    letters.append(chr(i))


def precede(ch):
    if ch == '+' or ch == '-':
        return 1
    elif ch == '*' or ch == '/':
        return 2
    elif ch == '^':
        return 3
    elif ch in ["sin", "cos", "tan", "sinh", "cosh", "tanh"]:
        return 4
    else:
        return 0


def isNumb(string):
    for char in string:
        if char not in numbers and char != ".":
            return False
    return True


def expressionToInfix(expression):
    segments = [""]
    index = 0
    trig = -1
    for char in expression:
        if char != " ":
            if index + 3 < len(expression) and expression[index] in ["s", "c", "t"] and expression[index + 1] in ["i", "o", "a"] and expression[index + 1] in ["n", "s"]:
                trig = 0
            elif trig != -1:
                segments[-1] += char
                trig += 1

                if trig + index >= 3:
                    trig = -1
            else:
                if char in numbers or char in letters or char == ".":
                    segments[-1] += char
                elif char in ["(", ")"]:
                    segments.append(char)
                    segments.append("")
                else:
                    segments.append(char)
                    segments.append("")

        index += 1

    res = []
    negative = False
    for i in range(len(segments)):
        if segments[i] != "":
            if segments[i] == "-" and res[-1] in ["+", "-", "*", "/", "^"]:
                # res.append(Term(float(segments[i] + segments[i + 1])))
                negative = True
            else:
                if isNumb(segments[i]):
                    if negative:
                        segments[i] = "-" + segments[i]

                    res.append(Term(float(segments[i])))
                else:
                    if negative:
                        segments[i] = "-" + segments[i]

                    res.append(segments[i])

                negative = False

    return res


def infixToPostfix(expression):
    stack = []
    postfix = []

    for ch in expression:
        if ch in numbers or ch in letters or isinstance(ch, Term):
            postfix.append(ch)
        elif ch == "(":
            stack.append("(")
        elif ch == "^":
            stack.append("^")
        elif ch == ")":
            while len(stack) > 0 and stack[-1] != "(":
                postfix.append(stack.pop())

            stack.pop()
        else:
            while len(stack) > 0 and precede(ch) <= precede(stack[-1]):
                postfix.append(stack.pop())

            stack.append(ch)

    while len(stack) > 0:
        postfix.append(stack.pop())

    return postfix


def postfixEval(postFix):
    stack = []

    for ch in postFix:
        if ch in numbers or ch in letters or isinstance(ch, Term):
            stack.append(ch)
        else:
            b = stack.pop()
            try:
                a = stack.pop()
            except IndexError:
                pass
            if ch == "+":
                stack.append(a + b)
            elif ch == "-":
                stack.append(a - b)
            if ch == "*":
                stack.append(a * b)
            elif ch == "/":
                stack.append(a / b)
            elif ch == "^":
                stack.append(a ** b)
            elif ch == "sin":
                stack.append(Term(math.sin(b.value)))
            elif ch == "cos":
                stack.append(Term(math.cos(b.value)))
            elif ch == "tan":
                stack.append(Term(math.tan(b.value)))

    return stack[0]


string = "((sin(0.5) / 2^0.5) / cos(tan(2^0.5)) * 3.14159265358979323846) / (1/10^5)"
print("Partitioned Expression       :", *expressionToInfix(string))
print("Postfix form of expression   :", *infixToPostfix(expressionToInfix(string)))
print("Evaluated answer             :", postfixEval(infixToPostfix(expressionToInfix(string))))


class Variable:
    def __init__(self, coefficient, varName, exp):
        self.coef = coefficient
        self.varName = varName
        self.exp = exp


class Constant:
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)


def add(a, b):
    if isinstance(a, Constant) and isinstance(b, Constant):
        return Constant(a.value + b.value)
    elif isinstance(a, Constant) and isinstance(b, Variable):
        # if ()
        pass
    else:
        return Term(self.value + float(other))

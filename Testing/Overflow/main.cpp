#include <iostream>
#include <cstring>

uint32_t *addLongs(uint32_t *a, uint32_t *b, int aLen, int bLen) {
    uint32_t resLen = aLen > bLen ? aLen + 1 : bLen + 1;
    auto *res = (uint32_t *) malloc(sizeof(uint32_t) * resLen);
    uint32_t carry = 0;
    for (long long i = (long long) resLen - 1; i >= 0; i--) {
        uint64_t sum = (uint64_t) a[i] + (uint64_t) b[i] + (uint64_t) carry; // note the type!
        res[i] = (uint32_t) sum;
        carry = (uint32_t) (sum >> sizeof(uint32_t) * 8);
    }

    return res;
}

uint32_t *mulLongs(uint32_t *a, uint32_t *b, int aLen, int bLen) {

}

void printBin(const uint32_t *val, uint32_t len) {
    auto *res = (char *) malloc(sizeof(char) * (len + 1));
    res[len] = '\0';
    uint64_t index = len - 1;

    for (uint32_t i = 0; i < len; i++) {
        for (uint32_t j = 0; j < sizeof(uint32_t) * 8; j++) {
            std::cout << (((val[i] << j) & ((uint32_t) 1 << (uint32_t) 31)) > 0 ? 1 : 0);
        }
    }
    std::cout << "\n";
}

int main() {
	uint32_t max = UINT32_MAX;
	std::cout << "Maximum value: " << max << "\n";

	//struct container res = add(max, 1);

#   ifdef __SIZEOF_INT128__
	std::cout << "Got a thing: " << __SIZEOF_INT128__ << "\n";
#   endif

    __int128 val;

    auto *a = (uint32_t *) calloc(sizeof(uint32_t), 2);
    a[1] = max; // 1 << (uint32_t) 63;
    auto *b = (uint32_t *) calloc(sizeof(uint32_t), 2);
    b[1] = max;

	std::cout << "Results:\n";
	printBin(a, 2);
	printBin(b, 2);
	printBin(addLongs(a, b, 2, 2), 2);

	std::cout << "\n\n\n";

	auto *m = (uint32_t *) calloc(sizeof(uint32_t), 2);
	auto *n = (uint32_t *) calloc(sizeof(uint32_t), 2);
	m[1] = max;
	n[1] = 2;

	auto m64 = ((uint64_t) m[1]); // << 32u;
	auto n64 = ((uint64_t) n[1]); // << 32u;

	std::cout << "M: "; printBin(m, 2);
	std::cout << "N: "; printBin(n, 2);
    std::cout << "\n";
    std::cout << "M64: " << m64 << "\n";
    std::cout << "N64: " << n64 << "\n";
    std::cout << "N*M: " << (m64 * n64) << "\n";
    std::cout << "Result: " << ((uint32_t) (m64 * n64)) << "\n";
    std::cout << "Carry: " << ((uint32_t) ((m64 * n64) >> 32u)) << "\n";

    return 0;
}

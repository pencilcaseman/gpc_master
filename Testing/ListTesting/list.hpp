//
// Created by penci on 03/06/2020.
//

#ifndef LISTTESTING_LIST_HPP
#define LISTTESTING_LIST_HPP

class Container {
    std::string initialized = "NaN";

    int intVal = 0;
    float floatVal = 0;
    double doubleVal = 0;
    std::string strVal;

public:

    explicit Container(int x) {
        initialized = "int";
        intVal = x;
    }

    explicit Container(float x) {
        initialized = "float";
        floatVal = x;
    }

    explicit Container(double x) {
        initialized = "double";
        doubleVal = x;
    }

    Container &operator=(int x) {
        std::cout << "here?\n";
        intVal = x;
        initialized = "int";
        return *this;
    }

    Container &operator=(float x) {
        floatVal = x;
        initialized = "float";
        return *this;
    }

    Container &operator=(double x) {
        doubleVal = x;
        initialized = "double";
        return *this;
    }

    explicit operator int() const { return intVal; }

    explicit operator float() const { return floatVal; }

    explicit operator double() const { return doubleVal; }

    std::string toString() {
        std::string res;

        if (initialized == "int") {
            res += std::to_string(intVal);
        } else if (initialized == "float") {
            res += std::to_string(floatVal);
        } else if (initialized == "double") {
            res += std::to_string(doubleVal);
        } else {
            res += "NaN";
        }

        return res;
    }
};

class List {
    Container *data;
    int len;

public:

    explicit List(int length) {
        std::cout << "Here\n";

        data = (Container *) malloc(sizeof(Container) * length);

        for (int i = 0; i < length; i++) {
            std::cout << "Here123?\n";
            data[i] = Container(0);

            Container x = Container(0);
            x = 3.141592653589793;
            std::cout << x.toString() << "\n";
        }

        len = length;
    }

    Container &operator[](int index) {
        if (index < len) {
            return data[index];
        }
    }

    std::string toString() {
        std::string res = "[";

        for (int i = 0; i < len; i++) {
            std::cout << "here.\n";

            res += data[i].toString();

            if (i + 1 < len) {
                res += ", ";
            }
        }

        return res + "]";
    }
};

std::ostream &operator<<(std::ostream &os, List val) {
    return os << val.toString();
}

#endif //LISTTESTING_LIST_HPP

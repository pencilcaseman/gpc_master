#include <iostream>
#include "list.hpp"

int main() {
    List l = List(3);
    std::cout << "ABC?\n";
    l[0] = 1;
    l[1] = 2.5;
    l[2] = 4.2;
    std::cout << l[0].toString() << "\n";

    return 0;
}

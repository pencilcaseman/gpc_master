#include <iostream>
#include <climits>

struct container {
    unsigned long long sum, carry;
};

struct container add(unsigned long long a, unsigned long long b) {
    unsigned long long sum, carry;
    if (a + b < a) {
        // Overflow
        carry = 1;
        sum = a + b;
    } else {
        carry = 0;
        sum = a + b;
    }

    return container {sum, carry};
}

int main() {
	unsigned long long max = UUL_MAX;

	struct container res = add(10, 10);

	std::cout << "Results:\n";
	std::cout << "Sum 	: " << res.sum << "\n";
	std::cout << "Carry : " << res.carry << "\n";

    return 0;
}

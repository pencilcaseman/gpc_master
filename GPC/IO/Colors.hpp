//
// Created by penci on 22/03/2020.
//

#ifndef GPC_COLORS_HPP
#define GPC_COLORS_HPP

// Ansi colour codes
#define RESET                           "\033[0m"
#define BOLD                            "\033[1m"
#define ITALIC                          "\033[3m"
#define UNDERLINE                       "\033[4m"
#define UNDERLINE                       "\033[4m"
#define STRIKETHROUGH                   "\033[9m"
#define THICK_UNDERLINE                 "\033[21m"
#define WHITE                           "\033[30m"
#define RED                             "\033[31m"
#define GREEN                           "\033[32m"
#define YELLOW                          "\033[33m"
#define BLUE                            "\033[34m"
#define PURPLE                          "\033[35m"
#define TURQUOISE                       "\033[36m"
#define GREY                            "\033[37m"
#define LIGHT_GREY                      "\033[38m"
#define WHITE_BACKGROUND                "\033[40m"
#define RED_BACKGROUND                  "\033[41m"
#define GREEN_BACKGROUND                "\033[42m"
#define YELLOW_BACKGROUND               "\033[43m"
#define BLUE_BACKGROUND                 "\033[44m"
#define PURPLE_BACKGROUND               "\033[45m"
#define TURQUOISE_BACKGROUND            "\033[46m"
#define GREY_BACKGROUND                 "\033[47m"
#define BORDER                          "\033[51m"
#define BRIGHT_RED                      "\033[91m"
#define BRIGHT_GREEN                    "\033[92m"
#define BRIGHT_YELLOW                   "\033[93m"
#define BRIGHT_BLUE                     "\033[94m"
#define BRIGHT_PURPLE                   "\033[95m"
#define BRIGHT_TURQUOISE                "\033[96m"
#define BRIGHT_BLACK                    "\033[97m"
#define BRIGHT_GREY                     "\033[98m"
#define BRIGHT_RED_BACKGROUND           "\033[101m"
#define BRIGHT_GREEN_BACKGROUND         "\033[102m"
#define BRIGHT_YELLOW_BACKGROUND        "\033[103m"
#define BRIGHT_BLUE_BACKGROUND          "\033[104m"
#define BRIGHT_PURPLE_BACKGROUND        "\033[105m"
#define BRIGHT_TURQUOISE_BACKGROUND     "\033[106m"
#define BRIGHT_BLACK_BACKGROUND         "\033[107m"
#define BRIGHT_GREY_BACKGROUND          "\033[100m"

// Other escape codes

#endif //GPC_COLORS_HPP

//
// Created by penci on 22/03/2020.
//

#ifndef GPC_BUILTIN_HPP
#define GPC_BUILTIN_HPP

#include "../IO/Colors.hpp"

class BaseException {
private:
    unsigned long long line;
    char *file;
    char *function;
    char *message;

public:
    BaseException(unsigned long long _line, char *_file, char *_function, char *_message) {
        line = _line;
        file = _file;
        function = _function;
        message = _message;
    }

    int handle() {

        return 123;
    }
};

#endif //GPC_BUILTIN_HPP

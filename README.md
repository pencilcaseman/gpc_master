# GPC

## The Aim
The aim of this project is to provide a basic framework of functions, classes
and libraries to allow large projects to be made simpler, shorter and faster.

## Progress
The project is currently in its early stages, with many files still incomplete,
and many more files and libraries to be added. The code will be optimised and
made much faster, improving the speed of projects that utilize this set of
libraries.

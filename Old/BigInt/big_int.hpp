//
// Created by penci on 22/03/2020.
//

#ifndef GPC_TEMP_BIGINT_HPP
#define GPC_TEMP_BIGINT_HPP

// WORKS ONLY FOR POSITIVE VALUES
// SIGN CURRENTLY DOESN'T DO ANYTHING

#include "../DynamicBitset/dynamic_bitset.hpp"

using namespace gpc::dynamic_bitset;

/*
 * Big integer type for C++
 *
 * Allows indefinitely large numbers to be stored and operated on, with
 * all basic math operations, and more advanced operations to come
 */
class BigInt {
public:
    DynamicBitset bits;
    int sign;
    bool _usingNegative;

    BigInt() {
        bits = DynamicBitset(0);
        sign = 0;
        _usingNegative = false;
    }

    BigInt(int x) {
        bits = DynamicBitset((unsigned long) std::abs(x));
        sign = x < 0;
        _usingNegative = false;
    }

    BigInt(unsigned int x) {
        bits = DynamicBitset(x);
        sign = 0;
        _usingNegative = false;
    }

    BigInt(long x) {
        bits = DynamicBitset((unsigned long) std::abs(x));
        sign = x < 0;
        _usingNegative = false;
    }

    BigInt(unsigned long x) {
        bits = DynamicBitset(x);
        sign = 0;
        _usingNegative = false;
    };

    BigInt(long long x) {
        bits = DynamicBitset((unsigned long long) std::abs(x));
        sign = x < 0;
        _usingNegative = false;
    }

    BigInt(unsigned long long x) {
        bits = DynamicBitset(x);
        sign = 0;
        _usingNegative = false;
    }

    BigInt copy(unsigned long long n = 0) {
        BigInt res;
        res.bits = bits.copy(n);
        res.sign = sign;
        return res;
    }

    static BigInt zero(unsigned long long n) {
        BigInt res;
        res.bits = DynamicBitset::empty(n);
        res.sign = 0;
        return res;
    }

    BigInt negative() {
        auto res = copy();
        res.bits = ~res.bits;
        res._usingNegative = true;
        return res + BigInt(1);
    }

    bool operator>(BigInt other) {
        bits.setFirst_LastBit();
        other.bits.setFirst_LastBit();

        return bits.bin[bits.lastSetBit] > other.bits.bin[other.bits.lastSetBit];
    }

    bool operator<(BigInt other) {
        bits.setFirst_LastBit();
        other.bits.setFirst_LastBit();

        return bits.bin[bits.lastSetBit] < other.bits.bin[other.bits.lastSetBit];
    }

    bool operator>=(BigInt other) {
        bits.setFirst_LastBit();
        other.bits.setFirst_LastBit();

        return bits.bin[bits.lastSetBit] >= other.bits.bin[other.bits.lastSetBit];
    }

    bool operator<=(BigInt other) {
        bits.setFirst_LastBit();
        other.bits.setFirst_LastBit();

        return bits.bin[bits.lastSetBit] <= other.bits.bin[other.bits.lastSetBit];
    }

    bool operator==(BigInt other) {
        for (unsigned long long i = 0; i < (bits.binLen > other.bits.binLen ? bits.binLen : other.bits.binLen); i++) {
            uint32_t aVal, bVal;
            aVal = bVal = 0;

            if (i < bits.binLen) aVal = bits.bin[i];
            if (i < other.bits.binLen) bVal = other.bits.bin[i];

            if (aVal != bVal) {
                return false;
            }
        }

        return true;
    }

    BigInt operator+(BigInt other) {
        bits.setFirst_LastBit();
        unsigned long long nBits = bits.bits > other.bits.bits ? bits.bits + 1 : other.bits.bits + 1;
        BigInt res = BigInt::zero(nBits);

        uint64_t carry = 0;
        for (unsigned long long i = 0; i < res.bits.binLen; i++) {
            uint64_t aVal, bVal;
            aVal = bVal = 0;

            if (i < bits.binLen) aVal = (uint64_t) bits.bin[i];
            if (i < other.bits.binLen) bVal = (uint64_t) other.bits.bin[i];

            uint64_t sum = aVal + bVal + carry;
            carry = sum >> (sizeof(uint32_t) * 8);
            res.bits.bin[i] = (uint32_t) sum;
        }

        _usingNegative = false;
        res.bits.setFirst_LastBit();

        return res;
    }

    BigInt operator+(unsigned long long other) {
        return *this + BigInt(other);
    }

    BigInt operator-(BigInt other) {
        _usingNegative = true;
        return *this + other.negative();
    }

    BigInt operator-(unsigned long long other) {
        return *this - BigInt(other);
    }

    BigInt operator<<(unsigned long long other) {
        auto res = copy(bits.bits + other);
        res.bits <<= other;
        return res;
    }

    BigInt operator>>(unsigned long long other) {
        auto res = copy(bits.bits - other);
        res.bits >>= other;
        return res;
    }

    BigInt operator*(BigInt other) {
        auto res = BigInt::zero(bits.bits + other.bits.bits);
        auto tmp = other.copy(bits.bits + other.bits.bits);

        for (unsigned long long i = 0; i < bits.bits; i++) {
            if (bits.testBit(i) > 0) res = res + tmp;

            tmp = tmp << 1;
        }

        return res;
    }

    BigInt operator*(unsigned long long other) {
        return *this * BigInt(other);
    }

    // #################################################################
    // #################################################################
    // #################################################################
    // #################################################################
    // #################################################################
    // #################################################################
    // #################################################################
    // #################################################################
    // FIX THIS -- WORKS FOR SMALL NUMBERS BUT NOT FOR LARGER ONES
    // PROB TO DO WITH <add> VARIABLE NOT BEING LARGE ENOUGH (OR TOO SMALL)
    // #################################################################
    // #################################################################
    // #################################################################
    // #################################################################
    // #################################################################
    // #################################################################
    // #################################################################
    BigInt operator/(BigInt other) {
        BigInt quotient = BigInt(0);
        BigInt temp = BigInt(0);

        for (unsigned long long i = DynamicBitset::utob(bits.binLen); i > 0; i--) {
            // Use i - 1
            if (temp + (other << i) <= *this) {
                temp = temp + (other << i);
                quotient.bits |= (DynamicBitset(1) << i);
            }
        }

        return quotient;
    }

    BigInt pow(unsigned long long n) {
        auto res = copy();
        for (unsigned long long i = 0; i < n - 1; i++) {
            res = res * *this;

            //########################################################################################################
            //########################################################################################################
            //########################################################################################################
            //########################################################################################################
            // OPTIMISE!!!
            res.bits.fix();
            // std::cout << "Res: " << res.toBin() << "\n";
        }

        return res;
    }

    char *toBin() {
        return (char *) bits;
    }
};

#endif //GPC_TEMP_BIGINT_HPP

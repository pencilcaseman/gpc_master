alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUV"


def generateMacros(n):
    for i in range(n):
        print("#define FromVoid{}D {}".format(i + 1, "*" * (i + 1)))


def generateConversionTemplates(n):
    for i in range(n):
        print("template<class type>")
        print("type FromVoid{}D Array{}D(gpc::array::Array<type> val) {}".format(i + 1, i + 1, "{"))
        print("\treturn (type {}) val.data;".format("*" * (i + 1)))
        print("};\n")


def generateSubscripts(n):
    res = ""
    for i in range(n):
        res += "[" + alphabet[i] + "]"

    return res


def generateCreatorCode(n):    
    for i in range(n):
        if (i == 0):
            print("if (dimCount == {}) {}".format(i + 1, "{"))
        else:
            print(" else if (dimCount == {}) {}".format(i + 1, "{"))

        print("type FromVoid{}D temp = (type FromVoid{}D) malloc(sizeof(type {}) * dims[0]);".format(i + 1, i + 1, "" if i == 0 else " FromVoid{}D".format(i + 1)))
        for j in range(i + 1):
            if j < i:
                print("for (unsigned long long {} = 0; {} < dims[{}]; {}++) {}".format(alphabet[j], alphabet[j], j, alphabet[j], "{"))
                print("temp{} = (type FromVoid{}D) malloc(sizeof(type FromVoid{}D) * dims[{}]);".format(generateSubscripts(j + 1), (i) - j, (i) - j, j, j))
            else:
                print("for (unsigned long long {} = 0; {} < dims[{}]; {}++) {}".format(alphabet[j], alphabet[j], j, alphabet[j], "{"))
                print("temp{} = gpc::basic::defaultType;".format(generateSubscripts(j + 1)))
                
        for j in range(i + 1):
            print("}")
        print("return (void *) temp;")
        print("}", end="")

            
# generateMacros(32)
# generateConversionTemplates(32)
generateCreatorCode(8)

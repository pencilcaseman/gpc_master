//
// Created by penci on 23/04/2020.
//

#ifndef GPC_TEMP_ARRAYCREATOR_HPP
#define GPC_TEMP_ARRAYCREATOR_HPP

#include "DimMacros.hpp"
#include "../../Basic/basic.hpp"
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <cstring>

template<class type>
void *createEmptyVoid(const unsigned long long *dims, unsigned long long dimCount) {
    if (dimCount == 1) {
        type FromVoid1D temp = (type FromVoid1D) malloc(sizeof(type ) * dims[0]);
        for (unsigned long long a = 0; a < dims[0]; a++) {
            temp[a] = gpc::basic::defaultType;
        }
        return (void *) temp;
    } else if (dimCount == 2) {
        type FromVoid2D temp = (type FromVoid2D) malloc(sizeof(type  FromVoid2D) * dims[0]);
        for (unsigned long long a = 0; a < dims[0]; a++) {
            temp[a] = (type FromVoid1D) malloc(sizeof(type FromVoid1D) * dims[0]);
            for (unsigned long long b = 0; b < dims[1]; b++) {
                temp[a][b] = gpc::basic::defaultType;
            }
        }
        return (void *) temp;
    } else if (dimCount == 3) {
        type FromVoid3D temp = (type FromVoid3D) malloc(sizeof(type  FromVoid3D) * dims[0]);
        for (unsigned long long a = 0; a < dims[0]; a++) {
            temp[a] = (type FromVoid2D) malloc(sizeof(type FromVoid2D) * dims[0]);
            for (unsigned long long b = 0; b < dims[1]; b++) {
                temp[a][b] = (type FromVoid1D) malloc(sizeof(type FromVoid1D) * dims[1]);
                for (unsigned long long c = 0; c < dims[2]; c++) {
                    temp[a][b][c] = gpc::basic::defaultType;
                }
            }
        }
        return (void *) temp;
    } else if (dimCount == 4) {
        type FromVoid4D temp = (type FromVoid4D) malloc(sizeof(type  FromVoid4D) * dims[0]);
        for (unsigned long long a = 0; a < dims[0]; a++) {
            temp[a] = (type FromVoid3D) malloc(sizeof(type FromVoid3D) * dims[0]);
            for (unsigned long long b = 0; b < dims[1]; b++) {
                temp[a][b] = (type FromVoid2D) malloc(sizeof(type FromVoid2D) * dims[1]);
                for (unsigned long long c = 0; c < dims[2]; c++) {
                    temp[a][b][c] = (type FromVoid1D) malloc(sizeof(type FromVoid1D) * dims[2]);
                    for (unsigned long long d = 0; d < dims[3]; d++) {
                        temp[a][b][c][d] = gpc::basic::defaultType;
                    }
                }
            }
        }
        return (void *) temp;
    } else if (dimCount == 5) {
        type FromVoid5D temp = (type FromVoid5D) malloc(sizeof(type  FromVoid5D) * dims[0]);
        for (unsigned long long a = 0; a < dims[0]; a++) {
            temp[a] = (type FromVoid4D) malloc(sizeof(type FromVoid4D) * dims[0]);
            for (unsigned long long b = 0; b < dims[1]; b++) {
                temp[a][b] = (type FromVoid3D) malloc(sizeof(type FromVoid3D) * dims[1]);
                for (unsigned long long c = 0; c < dims[2]; c++) {
                    temp[a][b][c] = (type FromVoid2D) malloc(sizeof(type FromVoid2D) * dims[2]);
                    for (unsigned long long d = 0; d < dims[3]; d++) {
                        temp[a][b][c][d] = (type FromVoid1D) malloc(sizeof(type FromVoid1D) * dims[3]);
                        for (unsigned long long e = 0; e < dims[4]; e++) {
                            temp[a][b][c][d][e] = gpc::basic::defaultType;
                        }
                    }
                }
            }
        }
        return (void *) temp;
    } else if (dimCount == 6) {
        type FromVoid6D temp = (type FromVoid6D) malloc(sizeof(type  FromVoid6D) * dims[0]);
        for (unsigned long long a = 0; a < dims[0]; a++) {
            temp[a] = (type FromVoid5D) malloc(sizeof(type FromVoid5D) * dims[0]);
            for (unsigned long long b = 0; b < dims[1]; b++) {
                temp[a][b] = (type FromVoid4D) malloc(sizeof(type FromVoid4D) * dims[1]);
                for (unsigned long long c = 0; c < dims[2]; c++) {
                    temp[a][b][c] = (type FromVoid3D) malloc(sizeof(type FromVoid3D) * dims[2]);
                    for (unsigned long long d = 0; d < dims[3]; d++) {
                        temp[a][b][c][d] = (type FromVoid2D) malloc(sizeof(type FromVoid2D) * dims[3]);
                        for (unsigned long long e = 0; e < dims[4]; e++) {
                            temp[a][b][c][d][e] = (type FromVoid1D) malloc(sizeof(type FromVoid1D) * dims[4]);
                            for (unsigned long long f = 0; f < dims[5]; f++) {
                                temp[a][b][c][d][e][f] = gpc::basic::defaultType;
                            }
                        }
                    }
                }
            }
        }
        return (void *) temp;
    } else if (dimCount == 7) {
        type FromVoid7D temp = (type FromVoid7D) malloc(sizeof(type  FromVoid7D) * dims[0]);
        for (unsigned long long a = 0; a < dims[0]; a++) {
            temp[a] = (type FromVoid6D) malloc(sizeof(type FromVoid6D) * dims[0]);
            for (unsigned long long b = 0; b < dims[1]; b++) {
                temp[a][b] = (type FromVoid5D) malloc(sizeof(type FromVoid5D) * dims[1]);
                for (unsigned long long c = 0; c < dims[2]; c++) {
                    temp[a][b][c] = (type FromVoid4D) malloc(sizeof(type FromVoid4D) * dims[2]);
                    for (unsigned long long d = 0; d < dims[3]; d++) {
                        temp[a][b][c][d] = (type FromVoid3D) malloc(sizeof(type FromVoid3D) * dims[3]);
                        for (unsigned long long e = 0; e < dims[4]; e++) {
                            temp[a][b][c][d][e] = (type FromVoid2D) malloc(sizeof(type FromVoid2D) * dims[4]);
                            for (unsigned long long f = 0; f < dims[5]; f++) {
                                temp[a][b][c][d][e][f] = (type FromVoid1D) malloc(sizeof(type FromVoid1D) * dims[5]);
                                for (unsigned long long g = 0; g < dims[6]; g++) {
                                    temp[a][b][c][d][e][f][g] = gpc::basic::defaultType;
                                }
                            }
                        }
                    }
                }
            }
        }
        return (void *) temp;
    } else if (dimCount == 8) {
        type FromVoid8D temp = (type FromVoid8D) malloc(sizeof(type  FromVoid8D) * dims[0]);
        for (unsigned long long a = 0; a < dims[0]; a++) {
            temp[a] = (type FromVoid7D) malloc(sizeof(type FromVoid7D) * dims[0]);
            for (unsigned long long b = 0; b < dims[1]; b++) {
                temp[a][b] = (type FromVoid6D) malloc(sizeof(type FromVoid6D) * dims[1]);
                for (unsigned long long c = 0; c < dims[2]; c++) {
                    temp[a][b][c] = (type FromVoid5D) malloc(sizeof(type FromVoid5D) * dims[2]);
                    for (unsigned long long d = 0; d < dims[3]; d++) {
                        temp[a][b][c][d] = (type FromVoid4D) malloc(sizeof(type FromVoid4D) * dims[3]);
                        for (unsigned long long e = 0; e < dims[4]; e++) {
                            temp[a][b][c][d][e] = (type FromVoid3D) malloc(sizeof(type FromVoid3D) * dims[4]);
                            for (unsigned long long f = 0; f < dims[5]; f++) {
                                temp[a][b][c][d][e][f] = (type FromVoid2D) malloc(sizeof(type FromVoid2D) * dims[5]);
                                for (unsigned long long g = 0; g < dims[6]; g++) {
                                    temp[a][b][c][d][e][f][g] = (type FromVoid1D) malloc(sizeof(type FromVoid1D) * dims[6]);
                                    for (unsigned long long h = 0; h < dims[7]; h++) {
                                        temp[a][b][c][d][e][f][g][h] = gpc::basic::defaultType;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return (void *) temp;
    }
}

#endif //GPC_TEMP_ARRAYCREATOR_HPP

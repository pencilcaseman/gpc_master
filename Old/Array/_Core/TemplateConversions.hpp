//
// Created by penci on 23/04/2020.
//

#ifndef GPC_TEMP_TEMPLATECONVERSIONS_HPP
#define GPC_TEMP_TEMPLATECONVERSIONS_HPP

#include "DimMacros.hpp"
#include "../../Array/array.hpp"

namespace gpc::array {
    template<class type>
    type FromVoid1D Array1D(gpc::array::Array<type> val) {
        return (type *) val.data;
    };

    template<class type>
    type FromVoid2D Array2D(gpc::array::Array<type> val) {
        return (type **) val.data;
    };

    template<class type>
    type FromVoid3D Array3D(gpc::array::Array<type> val) {
        return (type ***) val.data;
    };

    template<class type>
    type FromVoid4D Array4D(gpc::array::Array<type> val) {
        return (type ****) val.data;
    };

    template<class type>
    type FromVoid5D Array5D(gpc::array::Array<type> val) {
        return (type *****) val.data;
    };

    template<class type>
    type FromVoid6D Array6D(gpc::array::Array<type> val) {
        return (type ******) val.data;
    };

    template<class type>
    type FromVoid7D Array7D(gpc::array::Array<type> val) {
        return (type *******) val.data;
    };

    template<class type>
    type FromVoid8D Array8D(gpc::array::Array<type> val) {
        return (type ********) val.data;
    };

    template<class type>
    type FromVoid9D Array9D(gpc::array::Array<type> val) {
        return (type *********) val.data;
    };

    template<class type>
    type FromVoid10D Array10D(gpc::array::Array<type> val) {
        return (type **********) val.data;
    };

    template<class type>
    type FromVoid11D Array11D(gpc::array::Array<type> val) {
        return (type ***********) val.data;
    };

    template<class type>
    type FromVoid12D Array12D(gpc::array::Array<type> val) {
        return (type ************) val.data;
    };

    template<class type>
    type FromVoid13D Array13D(gpc::array::Array<type> val) {
        return (type *************) val.data;
    };

    template<class type>
    type FromVoid14D Array14D(gpc::array::Array<type> val) {
        return (type **************) val.data;
    };

    template<class type>
    type FromVoid15D Array15D(gpc::array::Array<type> val) {
        return (type ***************) val.data;
    };

    template<class type>
    type FromVoid16D Array16D(gpc::array::Array<type> val) {
        return (type ****************) val.data;
    };

    template<class type>
    type FromVoid17D Array17D(gpc::array::Array<type> val) {
        return (type *****************) val.data;
    };

    template<class type>
    type FromVoid18D Array18D(gpc::array::Array<type> val) {
        return (type ******************) val.data;
    };

    template<class type>
    type FromVoid19D Array19D(gpc::array::Array<type> val) {
        return (type *******************) val.data;
    };

    template<class type>
    type FromVoid20D Array20D(gpc::array::Array<type> val) {
        return (type ********************) val.data;
    };

    template<class type>
    type FromVoid21D Array21D(gpc::array::Array<type> val) {
        return (type *********************) val.data;
    };

    template<class type>
    type FromVoid22D Array22D(gpc::array::Array<type> val) {
        return (type **********************) val.data;
    };

    template<class type>
    type FromVoid23D Array23D(gpc::array::Array<type> val) {
        return (type ***********************) val.data;
    };

    template<class type>
    type FromVoid24D Array24D(gpc::array::Array<type> val) {
        return (type ************************) val.data;
    };

    template<class type>
    type FromVoid25D Array25D(gpc::array::Array<type> val) {
        return (type *************************) val.data;
    };

    template<class type>
    type FromVoid26D Array26D(gpc::array::Array<type> val) {
        return (type **************************) val.data;
    };

    template<class type>
    type FromVoid27D Array27D(gpc::array::Array<type> val) {
        return (type ***************************) val.data;
    };

    template<class type>
    type FromVoid28D Array28D(gpc::array::Array<type> val) {
        return (type ****************************) val.data;
    };

    template<class type>
    type FromVoid29D Array29D(gpc::array::Array<type> val) {
        return (type *****************************) val.data;
    };

    template<class type>
    type FromVoid30D Array30D(gpc::array::Array<type> val) {
        return (type ******************************) val.data;
    };

    template<class type>
    type FromVoid31D Array31D(gpc::array::Array<type> val) {
        return (type *******************************) val.data;
    };

    template<class type>
    type FromVoid32D Array32D(gpc::array::Array<type> val) {
        return (type ********************************) val.data;
    };
}

#endif //GPC_TEMP_TEMPLATECONVERSIONS_HPP

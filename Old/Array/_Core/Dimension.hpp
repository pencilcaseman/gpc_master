//
// Created by penci on 25/04/2020.
//

#ifndef GPC_TEMP_DIMENSION_HPP
#define GPC_TEMP_DIMENSION_HPP

#include "../../List/list.hpp"

namespace gpc::array {
    class Dimension {
    public:
        gpc::list::List<unsigned long long> dimensions;
        unsigned long long index;

        explicit Dimension(unsigned long long n = 0) {
            dimensions = gpc::list::List<unsigned long long>(0);
            index = 0;
        }

        Dimension operator<<(unsigned long long other) {
            // dimensions[index++] = other;

            dimensions.append(other);

            return *this;
        }
    };
}

#endif //GPC_TEMP_DIMENSION_HPP

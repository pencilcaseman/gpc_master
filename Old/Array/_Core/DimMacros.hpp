//
// Created by penci on 23/04/2020.
//

#ifndef GPC_TEMP_DIMMACROS_HPP
#define GPC_TEMP_DIMMACROS_HPP

#define FromVoid1D *
#define FromVoid2D **
#define FromVoid3D ***
#define FromVoid4D ****
#define FromVoid5D *****
#define FromVoid6D ******
#define FromVoid7D *******
#define FromVoid8D ********
#define FromVoid9D *********
#define FromVoid10D **********
#define FromVoid11D ***********
#define FromVoid12D ************
#define FromVoid13D *************
#define FromVoid14D **************
#define FromVoid15D ***************
#define FromVoid16D ****************
#define FromVoid17D *****************
#define FromVoid18D ******************
#define FromVoid19D *******************
#define FromVoid20D ********************
#define FromVoid21D *********************
#define FromVoid22D **********************
#define FromVoid23D ***********************
#define FromVoid24D ************************
#define FromVoid25D *************************
#define FromVoid26D **************************
#define FromVoid27D ***************************
#define FromVoid28D ****************************
#define FromVoid29D *****************************
#define FromVoid30D ******************************
#define FromVoid31D *******************************
#define FromVoid32D ********************************

#endif //GPC_TEMP_DIMMACROS_HPP

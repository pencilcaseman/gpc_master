//
// Created by penci on 26/04/2020.
//

#ifndef GPC_TEMP_ARRAYTOSTRING_HPP
#define GPC_TEMP_ARRAYTOSTRING_HPP

#include "../array.hpp"

namespace gpc::array {
    template<typename t>
    std::string vecToString(void *data, unsigned long long *dimensions, std::string type) {
        auto tempData = (t FromVoid1D) data;

        if (type == "Vec2") {
            auto x = gpc::conversion::toString(tempData[0]);
            auto y = gpc::conversion::toString(tempData[1]);

            auto res = "Vec2(X: " + x + ", Y: " + y + ")";

            return res;
        } else if (type == "Vec3") {
            auto x = gpc::conversion::toString(tempData[0]);
            auto y = gpc::conversion::toString(tempData[1]);
            auto z = gpc::conversion::toString(tempData[2]);

            auto res = "Vec3(X: " + x + ", Y: " + y + ", Z: " + z + ")";

            return res;
        } else if (type == "Vec4") {
            auto x = gpc::conversion::toString(tempData[0]);
            auto y = gpc::conversion::toString(tempData[1]);
            auto z = gpc::conversion::toString(tempData[2]);
            auto w = gpc::conversion::toString(tempData[3]);

            auto res = "Vec4(X: " + x + ", Y: " + y + ", Z: " + z + ", W: " + w + ")";

            return res;
        } else {
            std::string res = "[";
            for (unsigned long long i = 0; i < dimensions[0]; i++) {
                res += gpc::conversion::toString(tempData[i]);
                if (i + 1 < dimensions[0]) {
                    res += ", ";
                }
            }

            res += "]";
            return res;
        }
    }

    template<typename t>
    unsigned long long countDigitsBeforeDecimal(t value) {
        std::string str = gpc::conversion::toString(value);
        unsigned long long digitsBeforeDecimal = 0;

        for (unsigned long long i = 0; i < str.length(); i++) {
            if (str[i] != '.') {
                digitsBeforeDecimal++;
            } else {
                return digitsBeforeDecimal;
            }
        }

        return digitsBeforeDecimal;
    }

    template<typename t>
    unsigned long long countDigitsAfterDecimal(t value) {
        std::string str = gpc::conversion::toString(value);
        unsigned long long digitsAfterDecimal = 0;

        for (unsigned long long i = str.length(); i > 0; i--) {
            if (str[i - 1] != '.') {
                digitsAfterDecimal++;
            } else {
                return digitsAfterDecimal;
            }
        }

        return 0;
    }

    std::string tabsAndSpaces(unsigned long long tabs, unsigned long long spaces) {
        std::string res = "";
        for (unsigned long long i = 0; i < tabs; i++) {
            res += "\t";
        }

        for (unsigned long long i = 0; i < spaces; i++) {
            res += " ";
        }

        return res;
    }

    template<typename t>
    std::string _matrixToString(t **matrix, unsigned long long rows, unsigned long long cols,
                                unsigned long long digitsBeforeDecimal, unsigned long long digitsAfterDecimal,
                                unsigned long long beforeLeftColumn, unsigned long long afterRightColumn,
                                unsigned long long tabs, bool format, long long _fix) {
        if (format) {
            unsigned long long fix = _fix - (_fix % 2);
            unsigned long long colWidth = cols;
            unsigned long long rowHeight = rows;
            if (fix > cols) colWidth = fix / 2;
            if (fix > rows) rowHeight = fix / 2;

            std::string res = tabsAndSpaces(tabs, 0);
            res += "[";

            for (unsigned long long i = 0; i < rows; i++) {
                if (i == 0) res += "[";
                else res += tabsAndSpaces(tabs, 1) + "[";

                for (unsigned long long j = 0; j < cols; j++) {
                    std::string value = gpc::conversion::toString(matrix[i][j]);
                    auto before = countDigitsBeforeDecimal(value);
                    auto after = countDigitsAfterDecimal(value);

                    if (j == 0) {
                        res += tabsAndSpaces(0, beforeLeftColumn - before) + gpc::conversion::toString(matrix[i][j]) +
                               tabsAndSpaces(0, digitsAfterDecimal - after);
                    } else if (j + 1 == cols) {
                        res += tabsAndSpaces(0, digitsBeforeDecimal - before) +
                               gpc::conversion::toString(matrix[i][j]) +
                               tabsAndSpaces(0, afterRightColumn - after);
                    } else {
                        res += tabsAndSpaces(0, digitsBeforeDecimal - before) +
                               gpc::conversion::toString(matrix[i][j]) +
                               tabsAndSpaces(0, digitsAfterDecimal - after);
                    }

                    if (j + 1 < cols) res += ", ";
                }
                if (i + 1 < rows) res += "]\n";
                else res += "]]";
            }
            return res;
        } else {
            std::string res = "";
            for (unsigned long long i = 0; i < rows; i++) {
                for (unsigned long long j = 0; j < cols; j++) {
                    if (matrix[i][j] == 1) {
                        res += gpc::color::Color().RED_BACKGROUND;
                    } else if (matrix[i][j] == 0) {
                        res += gpc::color::Color().BLUE_BACKGROUND;
                    }
                    res += gpc::conversion::toString(matrix[i][j]);
                }
                res += gpc::color::Color().RESET;
                res += "\n";
            }

            return res;
        }
    }

    template<typename t>
    std::string matrixToString(void *data, unsigned long long *dimensions, unsigned long long dimCount, bool format, long long fix) {
        unsigned long long digitsBefore = 0, digitsAfter = 0, beforeLeftColumn = 0, afterRightColumn = 0;
        t **tempData = (t **) data;

        // Get the longest integer and decimal lengths
        for (unsigned long long i = 0; i < dimensions[0]; i++) {
            for (unsigned long long j = 0; j < dimensions[1]; j++) {
                auto before = countDigitsBeforeDecimal<t>(tempData[i][j]);
                auto after = countDigitsAfterDecimal<t>(tempData[i][j]);

                if (i == 0 && before > beforeLeftColumn) beforeLeftColumn = before;
                else if (i + 1 == dimensions[0] && after > afterRightColumn) afterRightColumn = after;
                else {
                    if (before > digitsBefore) digitsBefore = before;
                    if (after > digitsAfter) digitsAfter = after;
                }
            }
        }

        return _matrixToString<t>(tempData, dimensions[0], dimensions[1], digitsBefore, digitsAfter, beforeLeftColumn,
                                  afterRightColumn, 0, format, fix);
    }
}

#endif //GPC_TEMP_ARRAYTOSTRING_HPP

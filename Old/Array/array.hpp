//
// Created by penci on 20/04/2020.
//

#ifndef GPC_TEMP_ARRAY_HPP
#define GPC_TEMP_ARRAY_HPP

#include "../Conversions/types.hpp"
#include <cstdarg>
#include "_Core/DimMacros.hpp"
#include "_Core/ArrayCreator.hpp"
#include <ostream>
#include "../List/list.hpp"
#include "_Core/Dimension.hpp"
#include "_Core/ArrayToString.hpp"
#include <cstring>

namespace gpc::array {
    template<typename arrayTypeName>
    class Array {
    public:
        bool format = true;
        long long fix = 6;

        void *data;
        unsigned long long *shape;
        unsigned long long depth;
        std::string arrayType;

        explicit Array(arrayTypeName val = 0) {
            shape = (unsigned long long *) malloc(sizeof(unsigned long long) * 1);
            shape[0] = 1;
            depth = 1;
            arrayType = "Vector";
            auto *tempData = (arrayTypeName *) malloc(sizeof(arrayTypeName) * 2);
            tempData[0] = val;
            data = (void *) tempData;
        }

        static Array Vec2(arrayTypeName x, arrayTypeName y) {
            Array<arrayTypeName> res = Array();

            res.shape = (unsigned long long *) malloc(sizeof(unsigned long long) * 1);
            res.shape[0] = 2;
            res.depth = 1;
            res.arrayType = "Vec2";
            auto *tempData = (arrayTypeName *) malloc(sizeof(arrayTypeName) * 2);
            tempData[0] = x;
            tempData[1] = y;
            res.data = (void *) tempData;

            return res;
        }

        static Array Vec3(arrayTypeName x, arrayTypeName y, arrayTypeName z) {
            Array<arrayTypeName> res = Array();

            res.shape = (unsigned long long *) malloc(sizeof(unsigned long long) * 1);
            res.shape[0] = 3;
            res.depth = 1;
            res.arrayType = "Vec3";
            auto *tempData = (arrayTypeName *) malloc(sizeof(arrayTypeName) * 3);
            tempData[0] = x;
            tempData[1] = y;
            tempData[2] = z;
            res.data = (void *) tempData;

            return res;
        }

        static Array Vec4(arrayTypeName x, arrayTypeName y, arrayTypeName z, arrayTypeName w) {
            Array<arrayTypeName> res = Array();

            res.shape = (unsigned long long *) malloc(sizeof(unsigned long long) * 1);
            res.shape[0] = 4;
            res.depth = 1;
            res.arrayType = "Vec4";
            auto *tempData = (arrayTypeName *) malloc(sizeof(arrayTypeName) * 4);
            tempData[0] = x;
            tempData[1] = y;
            tempData[2] = z;
            tempData[3] = w;
            res.data = (void *) tempData;

            return res;
        }

        explicit Array(gpc::array::Dimension dimensions) {
            depth = dimensions.dimensions.listLength;
            shape = (unsigned long long *) malloc(sizeof(unsigned long long) * depth);
            memcpy((void *) shape, dimensions.dimensions.data, sizeof(unsigned long long) * depth);

            data = createEmptyVoid<arrayTypeName>(shape, dimensions.dimensions.listLength);
            if (depth == 1) {
                arrayType = "Vector";
            } else if (depth == 2) {
                arrayType = "Matrix";
            } else {
                arrayType = "Array";
            }
        }

        std::string toString() {
            if (arrayType == "Vec2" ||
                arrayType == "Vec3" ||
                arrayType == "Vec4" ||
                arrayType == "Vector") {
                return gpc::array::vecToString<arrayTypeName>(data, shape, arrayType);
            } else {
                return gpc::array::matrixToString<arrayTypeName>(data, shape, depth, format, fix);
            }
        }

        explicit operator char *() {
            return &toString()[0];
        }
    };

    template<typename t>
    std::string to_string(Array<t> val) {
        return std::string((char *) val);
    }

    template<typename t>
    std::ostream &operator<<(std::ostream & os, Array<t> val) {
        return os << val.toString();
    }
}

#include "_Core/TemplateConversions.hpp"

#endif //GPC_TEMP_ARRAY_HPP

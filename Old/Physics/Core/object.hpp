//
// Created by penci on 04/06/2020.
//

#ifndef GPC_TEMP_OBJECT_HPP
#define GPC_TEMP_OBJECT_HPP

#include "../../Vector/vector.hpp"
#include "../../Math/intersections.hpp"

namespace gpc {
    namespace physics {
        namespace components {
            class Tri {
            public:
                gpc::vector::Vec2 p1;
                gpc::vector::Vec2 p2;
                gpc::vector::Vec2 p3;
                double minAngle;
                double maxAngle;

                Tri(gpc::vector::Vec2 a, gpc::vector::Vec2 b, gpc::vector::Vec2 c, double min, double max) {
                    p1 = a;
                    p2 = b;
                    p3 = c;
                    minAngle = min;
                    maxAngle = max;
                }
            };
        }

        // An object in the physics simulation
        // Contains all of the required data to represent
        // a 2D object in a World object
        class Object {
            // Vertices of the shape
            gpc::vector::Vec2 *vertexAngles_;
            gpc::vector::Vec2 *vertexLocations_;
            unsigned int vertexCount_;
            gpc::vector::Vec2 centreMass_;
            components::Tri *tris_;

        public:
            gpc::vector::Vec2 position;
            double angle = 0;
            double mass;

            Object(gpc::vector::Vec2 *verts, unsigned int vertCount, double x, double y) {
                vertexAngles_ = (gpc::vector::Vec2 *) malloc(sizeof(gpc::vector::Vec2) * vertCount);
                tris_ = (components::Tri *) malloc(sizeof(gpc::vector::Vec2) * (vertexCount_ - 2));
                vertexLocations_ = (gpc::vector::Vec2 *) malloc(sizeof(gpc::vector::Vec2) * vertCount);
                memcpy((void *) vertexLocations_, (void *) verts, sizeof(gpc::vector::Vec2) * vertCount);
                vertexCount_ = vertCount;
                position = gpc::vector::Vec2(x, y);
                mass = 0;
                calculateMass();

                for (unsigned int i = 0; i < vertCount; i++) {
                    vertexAngles_[i].x = verts[i].toAngle(centreMass_.x, centreMass_.y);
                    vertexAngles_[i].y = verts[i].mag();
                }
            }

            gpc::vector::Vec2 operator[](unsigned int index) {
                if (index < vertexCount_) {
                    // return gpc::vector::Vec2::fromAngle(
                    //        vertexAngles_[index].toAngle(centreMass_.x, centreMass_.y) + angle) * vertexAngles_[index].mag() +
                    //        position;

                    return gpc::vector::Vec2::fromAngle(vertexAngles_[index].x + angle) * vertexAngles_[index].y +
                           position;

                    // return gpc::vector::Vec2(vertexAngles_[index].x, vertexAngles_[index].y);
                } else {
                    gpc::warning::raiseError("IndexError", "List index out of range");
                }
            }

            void calculateMass() {
                if (vertexCount_ < 3) {
                    gpc::warning::raiseError("PhysicsShapeError",
                                             "Cannot create a shape with fewer than three vertexAngles_");
                }

                // Pick a point
                unsigned int index = 0;
                // Create averaged vertexAngles_, their mass and the toal
                auto vecCount = vertexCount_ - 2; // (vertexCount_ + 2) / 3;
                auto vecs = (gpc::vector::Vec2 *) malloc(sizeof(gpc::vector::Vec2) * vertexCount_);
                auto areas = (double *) malloc(sizeof(double) * vertexCount_);
                double total = 0;

                gpc::vector::Vec2 v1, v2, v3;
                v1 = vertexLocations_[index];

                // Loop triangles
                for (unsigned int i = 1; i < vertexCount_ - 1; i++) {
                    v2 = vertexLocations_[i];
                    v3 = vertexLocations_[i + 1];

                    vecs[i - 1] = gpc::vector::Vec2((v1.x + v2.x + v3.x) / 3, (v1.y + v2.y + v3.y) / 3);
                    areas[i - 1] = fabs(v1.x * (v2.y - v3.y) + v2.x * (v3.y - v1.y) + v3.x * (v1.y - v2.y)) / 2;

                    total += areas[i - 1];
                }

                auto average = gpc::vector::Vec2(0, 0);
                for (unsigned int i = 0; i < vecCount; i++) {
                    average += vecs[i] * (areas[i] / total);
                }

                mass = total;
                centreMass_ = average / vecCount;

                // Calculate triangle angles (for edge calculations)
                for (unsigned int i = 0; i < vertexCount_ - 1; i++) {
                    double a1 = vertexLocations_[i].toAngle(centreMass_.x, centreMass_.y);
                    double a2 = vertexLocations_[i + 1].toAngle(centreMass_.x, centreMass_.y);

                    // tris_[i] = components::Tri(centreMass_, v2, v3, gpc::math::min(a1, a2), gpc::math::max(a1, a2));
                    tris_[i] = components::Tri(centreMass_, vertexLocations_[i], vertexLocations_[i + 1], a1, a2);
                }
            }

            gpc::vector::Vec2 centreOfMass() {
                // return centreMass_ + position;
                return gpc::vector::Vec2::fromAngle(centreMass_.toAngle() + angle) * centreMass_.mag() + position;
            }

            [[nodiscard]] double vertexCount() const {
                return vertexCount_;
            }

            [[nodiscard]] components::Tri *tris() const {
                return tris_;
            }

            gpc::vector::Vec2 findEdgeRelative(double theta) const {
                for (unsigned int i = 0; i < vertexCount_; i++) {
                    if (theta >= tris_[i].minAngle && theta <= tris_[i].maxAngle) {
                        std::cout << "Tri: " << i << "\n";

                        // Found correct triangle section
                        // P1 = centreMass
                        auto p2 = gpc::vector::Vec2::fromAngle(theta) + centreMass_;
                        auto p3 = tris_[i].p2;
                        auto p4 = tris_[i].p3;

                        return gpc::math::checkIntersectionUnbounded(centreMass_, p2, p3, p4);
                    }
                }
            }

            void applyImpulse(gpc::vector::Vec2 relativeLocation, gpc::vector::Vec2 force) {

            }
        };
    }
}

#endif //GPC_TEMP_OBJECT_HPP

/*
 Copyright 2020 Toby Davis

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef GPC_TEMP_0_2_GPCBASEWARN_HPP
#define GPC_TEMP_0_2_GPCBASEWARN_HPP

#include <cassert>

namespace gpc {
	namespace warn {
		typedef struct {
			unsigned int line;
			std::string error;
			std::string message;
			std::string file;
			std::string function;
		} ErrorMessage;

		class Error : std::exception {
		private:
			unsigned int line = 0;
			std::string errorType;
			std::string errorMessage;
			std::string file;
			std::string function;

		public:
			virtual ErrorMessage what() {
				return ErrorMessage {
						line,
						!errorType.empty() ? errorType : "UnknownError",
						!errorMessage.empty() ? errorMessage : "No error message was received",
						!file.empty() ? file : "Unknown file",
						!function.empty() ? function : "Unknown function"
				};
			}
		};
	}
}

#endif //GPC_TEMP_0_2_GPCBASEWARN_HPP
